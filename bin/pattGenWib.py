#!/usr/bin/env python


import click
import wib2g
import wib_pattgen
import fmt_wibfile
import fmt_fullmode
import fmt_dpr
import fmt_unpacker
import fmt_axi4sfile

CONTEXT_SETTINGS = dict(help_option_names=["-h", "--help"])


@click.command(context_settings=CONTEXT_SETTINGS)
@click.option(
    "--type",
    "type",
    type=click.Choice(wib_pattgen.patterns.keys(),),
    help="Pattern type",
    default=None,
)
@click.option("--npackets", "-n", help="N packets", type=int, default=64)
@click.option("--outfile", "-o", help="Output file name", default="wib_pattern")
@click.option("--wib/--no-wib", "wib", help="Write WIB file", default=True)
@click.option("--33b/--no-33b", "bus33", help="Write 33b file", default=True)
@click.option("--dpr/--no-dpr", "dpr", help="Write DPR file", default=True)
@click.option("--up/--no-up", "up", help="Write Unpacker file", default=True)
def cli(type, npackets, outfile, wib, bus33, dpr, up):

    if npackets!=64:
        print("Are you sure you don't want 64 packets?")

    wiblist = wib_pattgen.wibPattern(type, npackets)

    if wib:
        fmt_w = fmt_wibfile.WibFileFormatter()
        file_w = open(outfile + "_wib.txt", "w")
        file_w.write(fmt_w.writeWibList(wiblist))
        file_w.close()

    if bus33:
        fmt_fm = fmt_fullmode.FullModeWibFormatter()

        if outfile:
            file_fm = open(outfile + "_wib_33b.txt", "w")
            fmt_fm.writeList(file_fm, wiblist)
            file_fm.close()

    if dpr:
        fmt_d = fmt_dpr.DPRWibFormatter()

        # convert to file format (should this be in fmt_dpr) ?
        strlist = ["{0:#0{1}x}".format(x, 18) for x in fmt_d.dumpList(wiblist)]
        s = "\n".join(strlist)

        file_d = open(outfile + "_wib_dpr.txt", "w")
        file_d.write(s)
        file_d.close()

    if up:
        fmt_up = fmt_unpacker.UnpackerFormatter()
        fmt_axi4stpg = fmt_axi4sfile.Axi4sTPGFormatter()

        up_packets = fmt_up.formatAllChannels(wiblist)
        axi4sStream = fmt_axi4stpg._formatAxi4sToTPGInStream(up_packets)

        # Write out unpacked packets in axi4 stream format
        file1_axi4s = open(outfile + "_up_axi4s.1.txt", "w")
        file2_axi4s = open(outfile + "_up_axi4s.2.txt", "w")
        file3_axi4s = open(outfile + "_up_axi4s.3.txt", "w")
        file4_axi4s = open(outfile + "_up_axi4s.4.txt", "w")
        files = [file1_axi4s, file2_axi4s, file3_axi4s, file4_axi4s]
        fmt_axi4stpg._writeAxi4sUpStream(files, axi4sStream)
        for file in files:
            file.close()

if __name__ == "__main__":
    cli()
