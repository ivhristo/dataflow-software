#!/usr/bin/env python

import click
import sys

sys.path.insert(1, "../dataformats")

import wib2g

import philfilegen

import fmt_wibfile
import fmt_dpr
import fmt_unpacker
import fmt_philfile

philtypes = {"std": 1, "half": 2, "wire": 3, "2hits": 4, "rnd": 5}

CONTEXT_SETTINGS = dict(help_option_names=["-h", "--help"])


@click.command(context_settings=CONTEXT_SETTINGS)
@click.option(
    "--type",
    "type",
    type=click.Choice(["std", "half", "wire", "2hits", "rnd"]),
    help="Pattern type",
    default="std",
)
@click.option("--npackets", "-n", help="N packets", type=int, default=64)
@click.option("--outfile", "-o", help="Output file name", default="philfile")
@click.option("--wib/--no-wib", "wib", help="Write WIB file", default=False)
@click.option("--dpr/--no-dpr", "dpr", help="Write DPR file", default=False)
@click.option("--up/--no-up", "up", help="Write Unpacker file", default=False)
@click.option("--phil/--no-phil", "phil", help="Write Philfile", default=False)
@click.option(
    "--fm-ascii/--no-fm-ascii",
    "fm_ascii",
    help="Dump FULLMODE in ASCII format",
    default=False,
)
@click.option(
    "--verbose/--not-verbose",
    "verbose",
    help="Print out resulting packets",
    default=False,
)

##### PHIL FILE GENERATOR CLICC OPTIONS #####
@click.option(
    "--nhits",
    "-nh",
    help="Used by some patterntypess which add multiple hits to a given wire. For patterntypes 1-4 leave as 1",
    type=click.INT,
    default=1,
)
@click.option(
    "--standardpeakheight",
    "-sph",
    help="Standard max height for peaks in ADC counts. Default=50.",
    type=click.INT,
    default=50,
)
@click.option("--napas", "-na", help="number of APAs to use", type=click.INT, default=1)
@click.option(
    "--nticks", "-nt", help="number of ticks per channel", type=click.INT, default=4492
)
@click.option(
    "--ncolchans",
    "-nc",
    help="number of collection chans per APA",
    type=click.INT,
    default=960,
)
@click.option(
    "--nindchans",
    "-ni",
    help="number of induction chans per APA",
    type=click.INT,
    default=1600,
)
@click.option(
    "--colped", "-pc", help="pedestal for col channels", type=click.INT, default=500
)
@click.option(
    "--noiseon", "-no", help="1 is noise on, 0 if not", type=click.INT, default=1
)
@click.option(
    "--noisemean",
    "-nmean",
    help="mean for noise - should be 0",
    type=click.INT,
    default=0,
)
@click.option(
    "--noisesig",
    "-nsig",
    help="sigma for noise - 2.5 is approx",
    type=click.INT,
    default=2.5,
)
@click.option(
    "--noisesamples",
    "-nsamp",
    help="number of noise samples to draw from",
    type=click.INT,
    default=1000000,
)
@click.option(
    "--nevts", "-e", help="number of events to fake", type=click.INT, default=1
)
@click.option(
    "--standardtickdelay",
    "-std",
    help="Fixed number of ticks to delay all hit peak centres by. Default=0",
    type=click.INT,
    default=0,
)
@click.option(
    "--fixedlength", "-fl", help="fixed length of hit", type=click.INT, default=10
)
@click.option(
    "--gaussianoption",
    "-go",
    help="use gaussian shaped hits",
    type=click.INT,
    default=0,
)  # 0 is off
@click.option(
    "--gausswidth", "-gw", help="std dev of gaussian", type=click.FLOAT, default=5
)  # default is tick 5 ticks std dev
@click.option(
    "--fchan", "-fc", help="first wire to have a signal", type=int, default=1600
)
@click.option(
    "--patterntype",
    "-pt",
    help="Type of pattern to use in signal generated. From 1-5",
    type=int,
    default=1,
)
@click.option(
    "--packetlength",
    "-pl",
    help="Tick length of a packet. Default 64",
    type=int,
    default=64,
)
def cli(
    outfile,
    type,
    npackets,
    wib,
    dpr,
    up,
    phil,
    fm_ascii,
    verbose,
    nhits,
    standardpeakheight,
    napas,
    nticks,
    ncolchans,
    nindchans,
    colped,
    noiseon,
    noisemean,
    noisesig,
    noisesamples,
    nevts,
    standardtickdelay,
    fixedlength,
    gaussianoption,
    gausswidth,
    fchan,
    patterntype,
    packetlength,
):

    fmt_phil = fmt_philfile.PhilFileFormatter()
    fmt_d = fmt_dpr.DPRWibFormatter()
    fmt_wib = fmt_wibfile.WibFileFormatter()
    fmt_up = fmt_unpacker.UnpackerFormatter()

    philGen = philfilegen.fakePhilFile(
        philtypes[type],
        nhits,
        standardpeakheight,
        napas,
        nticks,
        ncolchans,
        nindchans,
        colped,
        noiseon,
        noisemean,
        noisesig,
        noisesamples,
        nevts,
        standardtickdelay,
        fixedlength,
        gaussianoption,
        gausswidth,
        fchan,
        patterntype,
        packetlength,
    )

    wiblist = fmt_phil._formatPhilToWib(philGen)

    # Write data out in a philfile
    if phil:
        print("\n##### PHIL FILE #####\n")
        philData = fmt_phil._formatWibToPhil(wiblist)
        if outfile:
            file_phil = open(outfile + "_phil.txt", "w")
            fmt_phil._writePhilFile(file_phil, philData)
            file_phil.close()

        # Print out Philfile (Pretty)
        if verbose:
            fmt_phil._dumpPhilFile(philData)

    if fm_ascii:
        print("\n##### FULLMODE STREAM #####\n")
        fmData = fmt_fm.dumpListASCII(wiblist)

        if outfile:
            file_fm = open(outfile + "_fm.txt", "w")
            fmt_fm.writeList(file_fm, data)
            file_fm.close()

        # Print out FULLMODE stream (ASCII)
        if verbose:
            print(fmData)

    # Print out WIB packets (pretty)
    if wib:
        print("\n##### WIB PACKETS #####\n")
        if outfile:
            file_wib = open(outfile + "_wib.txt", "w")
            file_wib.write(fmt_wib.writeWibList(wiblist))
            file_wib.close()

        if verbose:
            for packet in wiblist:
                print(packet)

    if dpr:
        print("\n##### DPR #####\n")
        if outfile:
            # convert to file format (should this be in fmt_dpr) ?
            strlist = ["{0:#0{1}x}".format(x, 18) for x in fmt_d.dumpList(wiblist)]
            s = "\n".join(strlist)

            file_d = open(outfile + "_dpr.txt", "w")
            file_d.write(s)
            file_d.close()

        if verbose:
            strlist = ["{0:#0{1}x}".format(x, 18) for x in fmt_d.dumpList(wiblist)]
            s = "\n".join(strlist)
            print(s)

    # Print out unpacked packets (pretty)
    if up:
        print("\n##### UNPACKED PACKETS #####\n")
        up_packets = fmt_up.formatAllChannels(wiblist)
        axi4sStream = fmt_axi4stpg._formatAxi4sToTPGInStream(up_packets)

        if outfile:

            # Write out unpacked packets in axi4 stream format
            file1_axi4s = open(outfile + "_axi4s_1.txt", "w")
            file2_axi4s = open(outfile + "_axi4s_2.txt", "w")
            file3_axi4s = open(outfile + "_axi4s_3.txt", "w")
            file4_axi4s = open(outfile + "_axi4s_4.txt", "w")
            files = [file1_axi4s, file2_axi4s, file3_axi4s, file4_axi4s]
            fmt_axi4stpg._writeAxi4sUpStream(files, axi4sStream)
            for file in files:
                file.close()

        if verbose:
            fmt_axi4stpg._dumpAxi4sUpStream(axi4sStream)


if __name__ == "__main__":
    cli()
