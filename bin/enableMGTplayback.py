#!/usr/bin/env python 
import setup
import dpr
import dr

device = 'SIMUDP'
hw = setup.connectionManager().getDevice(device)
hw.setTimeoutPeriod(10000)

#####################################

# reset MGTs ( in simulation this starts the WIB data)
print "Enabling simulated MGTs"
hw.getNode("mgt.ctrl.rst_all").write(0)
hw.getNode("mgt.ctrl.rst_all").write(1)
hw.dispatch()

# print "Reading from output buffers"
# readFromAllSinks(hw)
