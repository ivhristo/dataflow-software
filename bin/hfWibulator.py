#!/usr/bin/env python
from __future__ import print_function, absolute_import
from pkg_resources import parse_version


import click
import setup
import wibulator
import time
import uhal

from toolbox import dumpReg, dumpSubRegs, printRegTable


CONTEXT_SETTINGS = dict(help_option_names=['-h', '--help'])

def get_devices(ctx, args, incomplete):
    devs = setup.connectionManager(ctx.params['connection']).getDevices()
    return [k for k in devs if incomplete in k]

extra_autocompl = {'autocompletion': get_devices} if parse_version(click.__version__) >= parse_version('7.0') else {}

@click.command(context_settings=CONTEXT_SETTINGS)
@click.option('-c', '--connection', type=click.Path(exists=True), envvar='CONNECTION_FILE')
@click.argument('device', **extra_autocompl)
@click.option('-p', '--pattern', type=click.Path(exists=True), default=None)
@click.option('-f', '--fire', is_flag=True, default=None)
@click.option('-l/-s', '--loop/--single', default=None)
@click.option('-c','--capture', is_flag=True, default=None)
@click.option('-c','--cap-path', type=click.Path(exists=False), default='b33_dump.txt')
@click.option('-t','--cap-timeout', type=float, default=10)
@click.option('--drop-idles', is_flag=True, default=None)

def cli(connection, device, pattern, fire, loop, capture, cap_path, cap_timeout, drop_idles):

    hw = setup.connectionManager(connection).getDevice(str(device))
    hw.setTimeoutPeriod(1000000)

    wibtorNode = hw.getNode('wibtor')
    try:
        b33sinkNode = hw.getNode('b33sink')
    except:
        b33sinkNode = None

    if not pattern is None:
        
        p = wibulator.loadWIBPatternFromFile(pattern)

        print('Pattern file:', pattern)
        print('Size:', len(p))

        wibulator.writePattern(wibtorNode, p)

    if not loop is None:
        wibulator.loop(wibtorNode, loop)
        
    if not fire is None:
        wibulator.fire(wibtorNode)

    if capture:
        if b33sinkNode:
            print('Capturing b33sink data')
            b33sinkNode.getNode('csr.ctrl.en').write(0)
            b33sinkNode.getNode('csr.ctrl.clear').write(1)
            b33sinkNode.getNode('csr.ctrl.clear').write(0)
            if drop_idles:
                b33sinkNode.getNode('csr.pattern_kchar').write(0x1)
                b33sinkNode.getNode('csr.pattern_data').write(0xbc)
                b33sinkNode.getNode('csr.ctrl.filter').write(0x1)
            else:
                b33sinkNode.getNode('csr.pattern_kchar').write(0x0)
                b33sinkNode.getNode('csr.pattern_data').write(0x0)
                b33sinkNode.getNode('csr.ctrl.filter').write(0x0)
            b33sinkNode.getClient().dispatch()
            b33sinkNode.getNode('csr.ctrl.en').write(1)
            b33sinkNode.getClient().dispatch()

            sleep = 0.1
            max_iter = int(cap_timeout//sleep)
            for i in xrange(max_iter):
                full = b33sinkNode.getNode('csr.stat.full').read()
                b33sinkNode.getClient().dispatch()
                if full.value():
                    break
                if i == (max_iter - 1):
                    print('Capture Timeout!!!')
                time.sleep(sleep)

            b33sinkNode.getNode('csr.ctrl.en').write(0)
            count = b33sinkNode.getNode('buf.count').read()
            b33sinkNode.getClient().dispatch()


            print('Reading out %s frames' % count)
            d = b33sinkNode.getNode('buf.data').readBlock(2*int(count))
            b33sinkNode.getClient().dispatch()
            data = [ (d[2*i+1], d[2*i]) for i in xrange(len(d)/2) ]
            with open(cap_path, 'w') as lFile:
                for i, (k, d) in enumerate(data):
                    lFile.write('0x{1:08x} {0}\n'.format(k, d)) 
                    print ('%04d' % i, k, '0x%08x' % d)
        else:
            print('ERROR: capture skipped. B33 sink not found')

    time.sleep(1)

    print('WIBulator registers')
    regs = dumpSubRegs(wibtorNode)
    printRegTable(regs, False)

    if b33sinkNode:
        print('b33 sink regs')
        # regs = dumpSubRegs(b33sinkNode)
        regs = dumpSubRegs(b33sinkNode.getNode('csr'))
        # This contraption is to avoid reading the fifo
        regs.update(dumpReg(b33sinkNode.getNode('buf.count')))
        printRegTable(regs, False)

if __name__ == '__main__':
    cli(complete_var='_HFWIBULATOR_COMPLETE')