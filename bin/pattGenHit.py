#!/usr/bin/env python

import sys

sys.path.insert(1, "../dataformats")
import click
import hit
import hitpattgen
import fmt_hitfile
import fmt_hitfullmode


CONTEXT_SETTINGS = dict(help_option_names=["-h", "--help"])


@click.command(context_settings=CONTEXT_SETTINGS)
@click.option(
    "--type",
    "type",
    type=click.Choice(["rnd", "onehit", "nhits", "nhitsnquant", "singlehit", "twohit"]),
    help="Pattern type",
    default=None,
)
@click.option("--npackets", "-n", help="N packets", type=int, default=64)
@click.option("--outfile", "-o", help="Output file name", default="wib_pattern")
@click.option("--hit/--no-hit", "hit", help="Dump hit packets", default=False)
@click.option(
    "--hitfile/--no-hitfile",
    "hitfile",
    help="Dump hits in hitfile format",
    default=False,
)
@click.option(
    "--hitfullmode/--no-hitfullmode",
    "hitfullmode",
    help="Dump hits in CR interface format",
    default=False,
)
@click.option(
    "--verbose/--not-verbose",
    "verbose",
    help="Print out resulting packets",
    default=False,
)
def cli(type, npackets, outfile, hit, hitfullmode, hitfile, verbose):

    hits = hitpattgen.hitList(type, npackets)
    fmt_hit = fmt_hitfile.HitFileFormatter()
    fmt_crif = fmt_hitfullmode.CrInterfaceFormatter()

    if hit:
        print("\n##### HIT PACKETS #####\n")
        if outfile:
            # Write out hit packets (for testing purposes)
            file_hit = open(outfile + "_hitpacket.txt", "w")
            for packet in hits:
                file_hit.write(str(packet.hitHeader.crate) + " ")
                file_hit.write(str(packet.hitHeader.fibre) + " ")
                file_hit.write(str(packet.hitHeader.wireIndex) + " ")
                file_hit.write(str(packet.hitHeader.pipeline) + " ")
                file_hit.write(str(packet.hitHeader.slot) + " ")
                file_hit.write(str(packet.hitHeader.flags) + " ")
                file_hit.write(str(packet.hitHeader.tstamp) + " ")

                for hit in packet.hitPayload.hits:
                    file_hit.write(str(hit.hitStartTime) + " ")
                    file_hit.write(str(hit.hitEndTime) + " ")
                    file_hit.write(str(hit.hitPeakADC) + " ")
                    file_hit.write(str(hit.hitPeakTime) + " ")
                    file_hit.write(str(hit.hitSumADC) + " ")
                    file_hit.write(str(hit.hitFlags) + " ")
                    # for h in hit:
                    #     outfile.write(str(h))
                    #     outfile.write(" ")

                file_hit.write("\n")
            file_hit.close()
        if verbose:
            for packet in hits:
                print(packet)

    if hitfile:
        print("\n##### HIT FILE #####\n")
        streamHit = fmt_hit._formatHitsToHitfile(hits)
        if outfile:
            file_hitfile = open(outfile + "_hitfilefmt.txt", "w")
            fmt_hit._writeHitfile(file_hitfile, streamHit)
            file_hitfile.close()
        if verbose:
            fmt_hit._dumpHitfile(streamHit)

    if hitfullmode:
        print("\n##### HIT FULLMODE 33B #####\n")
        stream33b = fmt_crif._formatHitsToCR(data, m=0)

        if outfile:
            # Write out hits in fullmode (33b) format
            file_hitfm = open(outfile + "_hitFullModeFmt.txt", "w")
            fmt_crif._writeHitsToCR(file_hitfm, stream33b)
            file_hitfm.close()
        # Print out hits in fullmode (33b) format (CR Interface)
        if verbose:
            fmt_crif._dumpCrInterface(stream33b)


if __name__ == "__main__":
    cli()
