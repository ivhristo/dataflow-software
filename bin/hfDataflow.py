#!/usr/bin/env python

import click
import sys
import os

sys.path.insert(1, os.environ["DF_ROOT"] + "/dataformats")

import wib2g
import axi4s
import hit as hitClass

import fmt_hitfullmode
import fmt_hitfile
import fmt_axi4sfile
import fmt_axi4sHit
import fmt_fullmode
import fmt_dpr
import fmt_unpacker
import fmt_philfile
import fmt_wibfile

import wib_pattgen
import hitpattgen
import pattGenPhil

CONTEXT_SETTINGS = dict(help_option_names=["-h", "--help"])


@click.command(context_settings=CONTEXT_SETTINGS)
@click.argument("infile", nargs=-1, type=click.File("r"))
@click.option(
    "--intype",
    "intype",
    type=click.Choice(
        [
            "hitfile",
            "hit",
            "arb",
            "hitfullmode",
            "wib",
            "fm-ascii",
            "axi4stpg",
            "axi4s-ascii",
            "up",
            "phil",
        ]
    ),
    help="Input file format type, choose from: hitfile, hit, hitfullmode, wib, fm-ascii, axi4s-ascii, up, phil",
    default=None,
)
@click.option("--outfile", "-o", help="Output file name", default=None)
@click.option(
    "--pattern",
    "pattern",
    type=click.Choice(["rnd", "hit_rnd"]),
    help="Pattern type",
    default=None,
)
@click.option("--n-packets", "n_packets", help="Number of packets", default=64)
@click.option(
    "--n-philpackets", "n_philpackets", help="Number of packets", default=None
)
@click.option("--wib/--no-wib", "wib", help="Dump WIB packets", default=False)
@click.option("--dpr/--no-dpr", "dpr", help="Dump dpr file", default=False)
@click.option(
    "--fm-ascii/--no-fm-ascii",
    "fm_ascii",
    help="Dump FULLMODE in ASCII format",
    default=False,
)
@click.option("--up/--no-up", "up", help="Dump unpacked AXI4S packets", default=False)
@click.option(
    "--axi4stpg/--no-axi4stpg", "axi4stpg", help="Dump axi4s hits", default=False
)
@click.option("--hit/--no-hit", "hit", help="Dump hit packets", default=False)
@click.option("--phil/--no-phil", "phil", help="Dump philfile", default=False)
@click.option(
    "--hitfile/--no-hitfile",
    "hitfile",
    help="Dump hits in hitfile format",
    default=False,
)
@click.option(
    "--hitfullmode/--no-hitfullmode",
    "hitfullmode",
    help="Dump hits in CR interface format",
    default=False,
)
@click.option(
    "--verbose/--not-verbose",
    "verbose",
    help="Print out resulting packets",
    default=False,
)
@click.option(
    "--timestamp",
    "timestamp",
    help="Add custom (64-bit) timestamp to philfile or hitfile",
    default=0,
)
def cli(
    infile,
    outfile,
    intype,
    pattern,
    n_packets,
    n_philpackets,
    dpr,
    wib,
    phil,
    fm_ascii,
    up,
    hit,
    hitfile,
    hitfullmode,
    axi4stpg,
    verbose,
    timestamp,
):
    if timestamp > int(18446744073709551615):
        raise Exception(
                "Timestamp must be a 64 bit number, i.e. less than {}".format(
                    int(18446744073709551615)
                )
            )
            
    data = []
    fmt_phil = fmt_philfile.PhilFileFormatter()
    fmt_hit = fmt_hitfile.HitFileFormatter()
    fmt_crif = fmt_hitfullmode.CrInterfaceFormatter()
    fmt_wib = fmt_wibfile.WibFileFormatter()
    fmt_up = fmt_unpacker.UnpackerFormatter()
    fmt_fm = fmt_fullmode.FullModeWibFormatter()
    fmt_d = fmt_dpr.DPRWibFormatter()
    fmt_axi4shit = fmt_axi4sHit.Axi4sHitFormatter()
    fmt_axi4stpg = fmt_axi4sfile.Axi4sTPGFormatter()

    # Generate a list of random wib packets
    if pattern == "rnd":
        data = wib_pattgen.wibListRandom(n_packets)

    # Generate a list of random hit packets
    elif pattern == "hit_rnd":
        data = hitpattgen.hitList(pattern, n_packets)

    # Use the input file if given
    elif infile:

        if intype == "hit":
            data = []

            for line in infile[0]:
                packet = hitClass.HitPacket()
                l = line.split()
                packet.hitHeader.crate = int(l[0])
                packet.hitHeader.fibre = int(l[1])
                packet.hitHeader.wireIndex = int(l[2])
                packet.hitHeader.pipeline = int(l[3])
                packet.hitHeader.slot = int(l[4])
                packet.hitHeader.flags = int(l[5])
                packet.hitHeader.tstamp = int(l[6])
                hs = []

                for ival, val in enumerate(l):

                    if ival > 6:
                        j = (ival - 6) % 6
                        hs.append(int(val))

                        if j == 0 and ival > 10:
                            h = hitClass.HitFrame()
                            h.hitStartTime = hs[0]
                            h.hitEndTime = hs[1]
                            h.hitPeakADC = hs[2]
                            h.hitPeakTime = hs[3]
                            h.hitSumADC = hs[4]
                            h.hitFlags = hs[5]

                            packet.hitPayload.hits.append(h)
                            hs = []

                data.append(packet)

        # Read hits from hitfile format file and print out
        elif intype == "hitfile":
            data = fmt_hit._formatHitfileToHits(infile[0], ts=timestamp)

        # Read hits from CR interface format file and print out
        elif intype == "hitfullmode":
            data = fmt_crif._formatFullModeToHits(infile[0])

        # Read in data from a philfile and print it out if verbose
        elif intype == "phil":
            data = fmt_phil._formatPhilToWib(
                infile[0], ts=timestamp, n_packets=n_philpackets
            )

        # Read in data from HF output
        elif intype == "axi4stpg":
            # for file in infile:
            data = fmt_axi4shit._formatAxi4sToHit(
                fmt_axi4stpg._formatStreamsToAxi4s(infile, intype)
            )

        # Read in data from HF input
        elif intype == "up":
            # for file in infile:
            data = fmt_up.formatAllTicks(
                fmt_axi4stpg._formatStreamsToAxi4s(infile, intype)
            )

        elif intype == "arb":
            data = fmt_axi4shit._formatAxi4sToHit(
                fmt_axi4stpg._formatArbitratorToAxi4s(infile[0], intype)
            )

        else:
            data = infile

    # Write data out in a philfile
    if phil:
        print("\n##### PHIL FILE #####\n")
        philData = fmt_phil._formatWibToPhil(data)
        if outfile:
            file_phil = open(outfile + "_phil.txt", "w")
            fmt_phil._writePhilFile(file_phil, philData)
            file_phil.close()

        # Print out Philfile (Pretty)
        if verbose:
            fmt_phil._dumpPhilFile(philData)

    # Print out WIB packets (pretty)
    if wib:
        print("\n##### WIB PACKETS #####\n")
        if outfile:
            file_wib = open(outfile + "_wib.txt", "w")
            file_wib.write(fmt_wib.writeWibList(data))
            file_wib.close()

        if verbose:
            for packet in data:
                print(packet)

    if fm_ascii:
        print("\n##### FULLMODE STREAM #####\n")
        fmData = fmt_fm.dumpListASCII(data)

        if outfile:
            file_fm = open(outfile + "_fm.txt", "w")
            fmt_fm.writeList(file_fm, data)
            file_fm.close()

        # Print out FULLMODE stream (ASCII)
        if verbose:
            print(fmData)

    if dpr:
        print("\n##### DPR #####\n")
        if outfile:
            # convert to file format (should this be in fmt_dpr) ?
            strlist = ["{0:#0{1}x}".format(x, 18) for x in fmt_d.dumpList(data)]
            s = "\n".join(strlist)

            file_d = open(outfile + "_dpr.txt", "w")
            file_d.write(s)
            file_d.close()
        
        if verbose:
            strlist = ["{0:#0{1}x}".format(x, 18) for x in fmt_d.dumpList(data)]
            s = "\n".join(strlist)
            print(s)

    # Print out unpacked packets (pretty)
    if up:
        print("\n##### UNPACKED PACKETS #####\n")
        up_packets = fmt_up.formatAllChannels(data)
        axi4sStream = fmt_axi4stpg._formatAxi4sToTPGInStream(up_packets)

        if outfile:

            # Write out unpacked packets in axi4 stream format
            file1_axi4s = open(outfile + "_axi4s_1.txt", "w")
            file2_axi4s = open(outfile + "_axi4s_2.txt", "w")
            file3_axi4s = open(outfile + "_axi4s_3.txt", "w")
            file4_axi4s = open(outfile + "_axi4s_4.txt", "w")
            files = [file1_axi4s, file2_axi4s, file3_axi4s, file4_axi4s]
            fmt_axi4stpg._writeAxi4sUpStream(files, axi4sStream)
            for file in files:
                file.close()

        if verbose:
            fmt_axi4stpg._dumpAxi4sUpStream(axi4sStream)

    # Print out Hit packets (pretty)
    if hit:
        print("\n##### HIT PACKETS #####\n")

        if outfile:
            # Write out hit packets (for testing purposes)
            file_hit = open(outfile + "_hit.txt", "w")
            fmt_axi4shit._writeHits(file_hit, data)
            file_hit.close()

        if verbose:
            for packet in data:
                print(packet)

    # Print out hitfile
    if hitfile:
        print("\n##### HIT FILE #####\n")

        streamHit = fmt_hit._formatHitsToHitfile(data)

        if outfile:
            file_hitfile = open(outfile + "_hitfile.txt", "w")
            fmt_hit._writeHitfile(file_hitfile, streamHit)
            file_hitfile.close()

        if verbose:
            fmt_hit._dumpHitfile(streamHit)

    if hitfullmode:
        print("\n##### HIT FULLMODE 33B #####\n")
        stream33b = fmt_crif._formatHitsToCR(data, m=0)

        if outfile:
            # Write out hits in fullmode (33b) format
            file_hitfm = open(outfile + "_hitfullmode.txt", "w")
            fmt_crif._writeHitsToCR(file_hitfm, stream33b)
            file_hitfm.close()
        # Print out hits in fullmode (33b) format (CR Interface)
        if verbose:
            fmt_crif._dumpCrInterface(stream33b)

    if axi4stpg:
        print("\n##### AXI4S TPG OUTPUT #####\n")

        axi4sStream = fmt_axi4stpg._formatAxi4sToTPGOutStream(
            fmt_axi4shit._formatHitToAxi4s(data)
        )

        if outfile:
            # Write out hits in axi4stpg format
            file1_axi4s = open(outfile + "_axi4stpg_0.txt", "w")
            file2_axi4s = open(outfile + "_axi4stpg_1.txt", "w")
            file3_axi4s = open(outfile + "_axi4stpg_2.txt", "w")
            file4_axi4s = open(outfile + "_axi4stpg_3.txt", "w")
            files = [file1_axi4s, file2_axi4s, file3_axi4s, file4_axi4s]
            fmt_axi4stpg._writeAxi4sTPGStream(files, axi4sStream)
            for file in files:
                file.close()

        # Print out hits in axi4stpg format: [data tvalid tuser tlast tkeep tready]
        if verbose:
            fmt_axi4stpg._dumpAxi4sTPGStream(axi4sStream)

if __name__ == "__main__":
    cli()
