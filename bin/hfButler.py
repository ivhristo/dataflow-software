#!/usr/bin/python

import sys
import time
import uhal
import os

import click

import setup
import dpr
import sink
import config

from pkg_resources import parse_version

# no functions in here expect those used by click !
CONTEXT_SETTINGS = dict(help_option_names=['-h', '--help'])


def doSetup(hw, sleep=True):

    print hw.id()

    setup.softReset(hw)

    if sleep is True:
        time.sleep(1)

    print "Enable source and sink"
    sink.enable(hw)


def doLoad(hw, infile):
    data = []
    for line in infile:
        data.append(int(line, 16))
    dpr.setup(hw, 0, 1)
    dpr.write(hw, data)


def padhex(d):
    return '0x' + hex(d)[2:].zfill(16)


# allows for multiple files to be processed in sequence, in a given directory. outputs into 4 files, one for each sink and contains all data.
def doLoadMulti(hw, directory, get, order, outfile, outtype):
    opentype = 'w+'
    start = time.time()
    if get == None:
        print "ERROR: No get method stated, this command can't be used with --read command, only --get."
        sys.exit()
    for filename in os.listdir(directory):
        if filename.endswith('.txt'):
            file = open(os.path.join(directory, filename), 'r')
            doLoad(hw, file)
            if get == 'raw':
                doGetRaw(hw, outfile, order, opentype)

            if get == 'hit':
                doGetHit(hw, outtype, opentype)
            opentype = 'a+'
            # doSetup(hw, sleep=False)
    end = time.time()
    print "time taken(s): " + str(end - start)


# allows for a single file to be processed multiple times, good for stability checks
def doLoadRepeat(repeat, hw, infile, get, order, outtype):
    opentype = "w+"
    start = time.time()
    if get == None:
        print "ERROR: No get method stated, this command can't be used with --read command, only --get <value>."
        sys.exit()
    infile.close()
    for i in range(repeat):
        file = open(infile.name, 'r')
        doLoad(hw, file)
        if get == 'raw':
            doGetRaw(hw, outfile, order, opentype)

        if get == 'hit':
            doGetHit(hw, outtype, opentype)
        opentype = "a+"
        # doSetup(hw, sleep=False)
    end = time.time()
    print "time taken(s): " + str(end - start)


# reads from all 4 buffers
def doRead(hw, outtype):
    for sinkNo in range(4):
        sink.readFromSink(hw, sinkNo, outtype)
    print "data written to output files, 1 for each buffer."


# converts raw data into 32bit hex, useful for comparing input and output data
def doGetRaw(hw, outtype, tryOrder, opentype='w+'):
    file = open("RawOut.txt", opentype)
    dataArrs = []
    packetPerSink = 0
    for sinkNo in range(4):
        arr = sink.getFromSink(hw, sinkNo, outtype)
        if arr is None:
            pass
        else:
            print len(arr)
            packetPerSink = len(arr)
            dataArrs.append(arr)
    flatd = []
    if tryOrder is False:
        for i in range(4):
            for j in range(len(dataArrs[i])):
                dataArrs[i][j][0] = [flatd.append(x) for x in dataArrs[i][j][0]]
                dataArrs[i][j][1] = [flatd.append(x) for x in dataArrs[i][j][1]]
    elif tryOrder is True:
        ordered = []
        for i in range(packetPerSink):
            [ordered.append(dataArrs[x][i]) for x in range(4)]
        for i in range(len(ordered)):
            [flatd.append(x) for x in ordered[i][0]]
            [flatd.append(x) for x in ordered[i][1]]
    flatd = [padhex(x) for x in flatd]
    for i in range(len(flatd)):
        file.write(flatd[i] + os.linesep)


# converts data into dtpc unpacker format, one file outputted per sink.
def doGetHit(hw, outtype, opentype='w+'):
    for sinkNo in range(4):
        file = open("out_sink_" + str(sinkNo + 1) + ".txt", opentype)
        packetPerSink = 0
        arr = sink.getFromSink(hw, sinkNo, outtype)
        if arr is None:
            pass
        else:
            print len(arr)
        arrf = []
        for i in range(len(arr)):
            [arrf.append(x) for x in arr[i]]
        arr = arrf
        arrf = []
        for i in range(len(arr)):
            [arrf.append(x) for x in arr[i]]
        arr = [padhex(x) for x in arrf]
        for i in range(len(arr)):
            line = arr[i][-5:]
            flag = bin(int(line[0]))[2:].zfill(2)
            line = line[1:] + ' ' + flag[0] + ' ' + flag[1]
            file.write(line + os.linesep)


def get_devices(ctx, args, incomplete):
    devs = setup.connectionManager(ctx.params['connection']).getDevices()
    return [k for k in devs if incomplete in k]

extra_autocompl = {'autocompletion': get_devices} if parse_version(click.__version__) >= parse_version('7.0') else {}


@click.command(context_settings=CONTEXT_SETTINGS)
@click.option('-c', '--connection', type=click.Path(exists=True), envvar='CONNECTION_FILE')
@click.argument('device', **extra_autocompl)
@click.option('--infile', '-i', help='Input file', type=click.File('r'))
@click.option(
    '--outfile',
    '-o',
    help='Output file name',
    type=click.File('w'),
    default='RawOut.txt',
)
@click.option('--intype', type=click.Choice(['phil', 'dpr']), default='phil')
@click.option('--outtype', type=click.Choice(['txt']), default='txt')
@click.option('--init/--no-init', is_flag=True, help='Setup', default=True)
@click.option('--load/--no-load', help='Load input patterns', default=False)
@click.option(
    '--loadmulti/--no-loadmulti', help='Load multiple input patterns', default=False
)
@click.option(
    '--repeat',
    '-r',
    help='repeat analysis of file multiple times, if 0 function is not called',
    type=click.INT,
    default=0,
)
@click.option('--directory', '-dir', help='directory to read multiple patterns from')
@click.option('--play/--no-play', help='Play patterns', default=False)
@click.option(
    '--status', is_flag=True, help='Checks sink and csr status', default=False
)
@click.option('--read/--no-read', help='Read output patterns', default=False)
@click.option(
    '--get',
    type=click.Choice(['raw', 'hit']),
    help='either "raw" (1 file) or "hit" (1 file per sink)',
)
@click.option(
    '--order/--no-order', help='Should doConvert order the payload', default=True
)
def doit(
    connection,
    device,
    infile,
    outfile,
    intype,
    outtype,
    init,
    load,
    loadmulti,
    repeat,
    play,
    status,
    get,
    read,
    order,
    directory,
):
    print ('Welcome to the HF Butler')
    hw = setup.connectionManager(connection).getDevice(str(device))

    if init:
        doSetup(hw)

    if load:
        if intype == 'phil':
            doLoadPhil(hw, infile)
        else:
            doLoad(hw, infile)

    if loadmulti:
        doLoadMulti(hw, directory, get, order, outfile, outtype)
        return

    if repeat != 0:
        doLoadRepeat(repeat, hw, infile, get, order, outtype)
        return

    if play:
        doPlay(hw)

    if status:
        sink.statusAll(hw)

    if get == 'raw':
        doGetRaw(hw, outfile, order)

    if get == 'hit':
        doGetHit(hw, outtype)

    if read:
        doRead(hw, outtype)


if __name__ == '__main__':
    doit(complete_var='_HFBUTLER_COMPLETE')
