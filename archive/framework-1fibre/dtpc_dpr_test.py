#!/usr/bin/python

# -*- coding: utf-8 -*-

#
# Test DUNE DAQ firmware input DPR
#
# D Cussans, June 19
#
import sys
import time
import uhal

#### Define Functions....
###############################################################################################
# Set up input and output Mux ( 1 = IPBus control )
#def setupDPRMux(hardWare,input_ctl , output_ctl):
def setupDPRMux(hardWare,mux_ctl):
        status = 0
        hardWare.getNode("dpr.config").write(mux_ctl)
        #hardWare.getNode("dpr.config.input_ctl").write(input_ctl & 0x00000001)
        #hardWare.getNode("dpr.config.output_ctl").write( (output_ctl<<1) & 0x00000002)
        hardWare.dispatch()

        return status
        
# Writes a 64-bit word to DPR
# N.B. Make sure that Mux is set to access write port from IPBus
def writeLongLongWood(hardWare, address , data):
        status = 0

        hardWare.getNode("dpr.write_data_low").write(data & 0xFFFFFFFF)
        hardWare.getNode("dpr.write_data_high").write((data>>32) & 0xFFFFFFFF)
        hardWare.getNode("dpr.write_addr").write(address)
        hardWare.dispatch()

        return status

# Read a 64-bit word to DPR
# N.B. Make sure that Mux is set to access write port from IPBus
def readLongLongWood(hardWare, address ):
        status = 0

        hardWare.getNode("dpr.read_addr").write(address)
        hardWare.dispatch() # set up read address. Put as separate dispatch to allow for read latency
        
        data_low = hardWare.getNode("dpr.read_data_low").read()
        data_high = hardWare.getNode("dpr.read_data_high").read()
        hardWare.dispatch()

        # print "low = " , hex(data_low)
        # print "high = ", hex(data_high)
        return data_low + (data_high<<32)

#### Start of Main
###############################################################################################

uhal.setLogLevelTo(uhal.LogLevel.NOTICE)
manager = uhal.ConnectionManager("file://connections.xml")
print sys.argv
hw = manager.getDevice(sys.argv[1])

#####################################

print "Setting Input and output Mux on DPR to IPBus control"
INPUT_MUX_IS_IPBUS = 1;
OUTPUT_MUX_IS_IPBUS = 2;
setupDPRMux(hw,INPUT_MUX_IS_IPBUS | OUTPUT_MUX_IS_IPBUS )


print "Writing to DPR"
data_to_write_to_10 = 0xDEADBEEFFEEDCAFE
writeLongLongWood(hw,0x010 , data_to_write_to_10 )
writeLongLongWood(hw,0x100 , 0xBEAD12345678FEAF )

print "Reading from DPR"
data = readLongLongWood(hw,0x010)
print "Data at 0x10 = %x" % data
assert data == data_to_write_to_10
 
data = readLongLongWood(hw,0x100)
print "Data at 0x100 = %x" % data

data = readLongLongWood(hw,0x010)
print "Data at 0x10 = %x" % data

ptr_to_write = 0x25
print "Writing %x to WIB frame pointer"% ptr_to_write
hw.getNode("dpr.wib_frame_ptr_w").write(ptr_to_write)
hw.dispatch()

print "Reading WIB frame pointer"
ptr = hw.getNode("dpr.wib_frame_ptr_r").read()
hw.dispatch()
print "WIB frame pointer read back as %x" % ptr

assert ptr_to_write == ptr

print "DPR passes basic test. Good"
