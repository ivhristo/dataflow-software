"""
Created on %(date)

@author: % Shyam Bhuller

This code was used to check if data reception test patterns were created properly.
This code only translates COLDATA so not the headers.

"""

# each wib packet is 120 words long
def readWIBFile(file):
    lines = []
    lines = [l[:-2] for l in file]
    packets = [lines[i: i+120] for i in range(0, len(lines), 120)]
    return packets


def PreparePackets(packets):
    # ignore headers, SOF and EOF for now
    for i in range(len(packets)):
        packets[i] = packets[i][9:-3]

    for i in range(len(packets)):
        for j in range(1, 4):
            packets[i].pop(24*j)
            packets[i].pop(24*j)
            packets[i].pop(24*j)
            packets[i].pop(24*j)
    return packets


def translate(packets):
    dpr_data = []
    for packet in packets:
        dpr_words = []
        #ADC2 CH2[3:0], ADC2 CH1[11:8], ADC1 CH2[3:0], ADC1 CH1[11:8], ADC2 CH1[7:0], ADC1 CH1[7:0]
        #ADC2 CH3[7:0], ADC1 CH3[7:0], ADC2 CH2[11:4], ADC1 CH2[11:4]
        #ADC2 CH4[11:4], ADC1 CH4[11:4], ADC2 CH4[3:0], ADC2 CH3[11:8], ADC1 CH4[3:0], ADC1 CH3[11:8]			


        for i in range(0, len(packet), 3):
            word1 = packet[i]
            word2 = packet[i+1]
            word3 = packet[i+2]

            d = [0]*8

            d[0] = word1[5] + word1[8:10]
            d[4]= word1[3] + word1[6:8]
            d[1]= word2[8:10] + word1[4]
            d[5]= word2[6:8] + word1[2]
            d[2]= word3[9] + word2[4:6]
            d[6]= word3[7] + word2[2:4]
            d[3]= word3[4:6] + word3[8]
            d[7]= word3[2:4] + word3[6]

            for i in range(8):
                d[i] = d[i].zfill(4)

            dpr_words.append('0x' + d[3] + d[2] + d[1] + d[0])
            dpr_words.append('0x' + d[7] + d[6] + d[5] + d[4])

        dpr_data.append(dpr_words)
    for j in range(len(dpr_data)):
        packet = dpr_data[j]
        for i in range(len(packet)):
            if (i % 4 == 1):
                temp = packet[i]
                packet[i] = packet[i+1]
                packet[i+1] = temp
    return dpr_data


file = open('input_wib.txt', 'r+')
packets = readWIBFile(file)
packets = PreparePackets(packets)
dpr_data = translate(packets)[0]

