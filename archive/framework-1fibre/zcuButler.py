#!/usr/bin/python

import sys
import time
import uhal
import os

import click


def resetLogic(hw):

        print "Resetting logic"
        hw.getNode("csr.ctrl.soft_rst").write(1)
        hw.dispatch()
        csrVal = hw.getNode("csr.ctrl.soft_rst").read()
        hw.dispatch()
        print "CSR value after reset = " , csrVal

        if csrVal != 0:
                print "Warning. CSR not magically reset! Clearing"
                hw.getNode("csr.ctrl.soft_rst").write(0)
                hw.dispatch()
                csrVal = hw.getNode("csr.ctrl.soft_rst").read()
                hw.dispatch()
                print "CSR value after reset (should be zero) = " , csrVal


def sinkEnable(hw):
        hw.getNode("sink0.csr.ctrl.en").write(1)
        hw.getNode("sink1.csr.ctrl.en").write(1)
        hw.getNode("sink2.csr.ctrl.en").write(1)
        hw.getNode("sink3.csr.ctrl.en").write(1)
	hw.dispatch()


def init(device):
        uhal.setLogLevelTo(uhal.LogLevel.NOTICE)
        manager = uhal.ConnectionManager("file://connections.xml")
        hw = manager.getDevice(str(device))
        return hw


def doSetup(hw, sleep=True):
        print hw.id()
	resetLogic(hw)
	"""
        print "Reset"
        hw.getNode("csr.ctrl.soft_rst").write(1)
        hw.dispatch()
	"""
	if sleep is True:
        	time.sleep(1)

        print "Enable source and sink"
	sinkEnable(hw)

 # function irrelevent (for current firmware)
def doLoadPhil(hw, infile):

        N_CHANS = 64
        N_SAMPS=256

        print "Reading the PhilPhile"
        data = []
        i = 0
        for l in infile:
        	data.append([int(x) for x in l.split()][2:])
        	i += 1
        	if i == N_CHANS: break
        	print "Channels:", hex(i), "Samples:", hex(len(data[0]))
        t = 0

        print "Filling buffer", hex(t), hex(len(data[0]))
        hw.getNode("src.buf.addr").write(0)
        for i in range(N_SAMPS):
                d = []
                for j in range(N_CHANS):
                        d.append(data[j][t + i])
                hw.getNode("src.buf.data").writeBlock(d) # Channels
                hw.dispatch()

# Set up input and output Mux ( 1 = IPBus control )
#def setupDPRMux(hardWare,input_ctl , output_ctl):
def setupDPRMux(hardWare,mux_ctl):
        status = 0
        hardWare.getNode("dpr.config").write(mux_ctl^1)
        #hardWare.getNode("dpr.config.input_ctl").write(input_ctl & 0x00000001)
        #hardWare.getNode("dpr.config.output_ctl").write( (output_ctl<<1) & 0x00000002)
        hardWare.dispatch()

        return status

# Writes a 64-bit word to DPR
# N.B. Make sure that Mux is set to access write port from IPBus
def writeDprLongLongWord(hardWare, address , data):
        status = 0

        hardWare.getNode("dpr.write_data_low").write(data & 0xFFFFFFFF)
        hardWare.getNode("dpr.write_data_high").write((data>>32) & 0xFFFFFFFF)
        hardWare.getNode("dpr.write_addr").write(address)
        hardWare.dispatch()

        return status

# Read a 64-bit word to DPR
# N.B. Make sure that Mux is set to access write port from IPBus
def readDprLongLongWord(hardWare, address ):
        status = 0

        hardWare.getNode("dpr.read_addr").write(address)
        hardWare.dispatch() # set up read address. Put as separate dispatch to allow for read latency
        
        data_low = hardWare.getNode("dpr.read_data_low").read()
        data_high = hardWare.getNode("dpr.read_data_high").read()
        hardWare.dispatch()

        # print "low = " , hex(data_low)
        # print "high = ", hex(data_high)
        return data_low + (data_high<<32)


def writeWibPtr(hw,ptr_to_write):
        print "Writing %x to WIB frame pointer"% ptr_to_write
        hw.getNode("dpr.wib_frame_ptr_w").write(ptr_to_write)
        hw.dispatch()


def writeToDPR(hw,data):
        print "Writing data to DPR"
        nwordsInFrame = 75
        previousWIBFrame = -1

        for addr in range(len(data)):
                wibFrame  = addr / nwordsInFrame
                word = data[addr]
                if wibFrame != previousWIBFrame:
                        print "New Frame"
                        writeWibPtr(hw,wibFrame)

                print hex(addr) , hex(word)  ,hex(wibFrame) 
                writeDprLongLongWord(hw, addr , word)
                previousWIBFrame = wibFrame 


def doLoad(hw, infile):
        data = []
        for line in infile:
                data.append(int(line,16))
        setupDPRMux(hw, 1)
        writeToDPR(hw,data)

# This function is currently irrelevent
def doPlay(hw):

	print "Starting playback"
	hw.dispatch()


def padhex(d):
    return '0x'+hex(d)[2:].zfill(16)

# prints terminal output and adds to a list to be written to a file
def ReadMessege(line, lines):
	line = str(line)
	print line
	lines.append(line)
	return lines

# old version of reading the data to the terminal, not used anymore
def doReadOld(hw):	
	id=0
	while True:
        	v = hw.getNode("src.csr.stat.waiting").read()
        	hw.dispatch()
        	if v == 1: break
        	time.sleep(1)
		
	print "Done"
	s = hw.getNode("sink.csr.stat").read()
	c = hw.getNode("sink.buf.count").read()
	hw.dispatch()

	if s & 0x6 == 0:
		print "Buffer error"
		sys.exit()
	elif c != 0:
		l = int(c) + 1
		d = hw.getNode("sink0.buf.data").readBlock(l)
		hw.dispatch()
                di = [int(x) for x in d]
		while l > 6:
            		print "Packet:", hex(id)
            		id += 1
			print "\tHeader:", [hex(x) for x in di[:6]]
            		del di[:6]
            		l -= 6
			p = []
			while True:
				r = di.pop(0)
				p.append(r)
				if r & 0x10000 != 0:
					l -= len(p)
					break
			print "\tPayload:", [hex(x) for x in p]

# read data from sinks, outputs in terminal style and saves to file
def readFromSink(hw, sinkNo, outtype):
	file = open("output" + str(sinkNo) + "." + outtype, 'w+')
	id = 0
    	lines = []
	statArr = doStatus(hw, sinkNo)
	sCmd = statArr[0]
	s = statArr[1]
	c = statArr[2]
	lines.append("sCmd = " + sCmd)
	lines.append("Sink status , sink count = " + str(s) + str(c) + os.linesep)
	if s & 0x6 != 0:
		ReadMessege("Warning - Buffer error", lines)
		# sys.exit()
#	elif c != 0:
	if c != 0:
		l = int(c) + 1
		ReadMessege("l = " + str(l), lines)
		d = hw.getNode("sink%s.buf.data" % sinkNo ).readBlock(l)
		hw.dispatch()
		ReadMessege("d = " + str(d), lines)
		di = [int(x) for x in d]
		while l > 6:
			ReadMessege("Sink %s , Packet:"%sinkNo + str(hex(id)), lines)
			id += 1
			ReadMessege("\tSink %s , Header:"%sinkNo + str([hex(x) for x in di[:6]]), lines)
			del di[:6]
            		l -= 6
			p = []
			while True:
                		if len(di):
                   			r = di.pop(0)
                		else:
					ReadMessege("Warning - attemping to pop from empty list", lines)
                    			l = 0
                    			break
				p.append(r)
				if r & 0x10000 != 0:
					l -= len(p)
					break
			ReadMessege("\tSink %s , Payload:"%sinkNo + str([hex(x) for x in p]), lines)

	for i in range(0, len(lines)):
		file.write(lines[i] + os.linesep)


# gets raw data from sink ready to convert into 32bit hex
def getFromSink(hw, sinkNo, outtype):
	statArr = doStatus(hw, sinkNo)
	sCmd = statArr[0]
	s = statArr[1]
	c = statArr[2]
	if s & 0x6 != 0:
		print "Warning - Buffer error"
		# sys.exit()
#	elif c != 0:
	if c != 0:
		l = int(c) + 1
		d = hw.getNode("sink%s.buf.data" % sinkNo ).readBlock(l)
		hw.dispatch()
		di = [int(x) for x in d]
		data = []
		while l > 6:
			h = di[:6]
         		del di[:6]
            		l -= 6
			p = []
			while True:
                		if len(di):
                			r = di.pop(0)
            			else:
                			l = 0
                			break
				p.append(r)
				if r & 0x10000 != 0:
					l -= len(p)
					break
            		compList = [h, p]
            		data.append(compList)
		return data
	if c == 0:
		print "ERROR: no data passed to this buffer!"
		sys.exit()

# allows for multiple files to be processed in sequence, in a given directory. outputs into 4 files, one for each sink and contains all data.
def doLoadMulti(hw, directory, get, order, outfile, outtype):
	opentype = 'w+'
	start = time.time()
	if get == None:
		print "ERROR: No get method stated, this command can't be used with --read command, only --get."
		sys.exit()
	for filename in os.listdir(directory):
		if filename.endswith('.txt'):
			file = open(os.path.join(directory, filename), 'r')
			doLoad(hw, file)
    			if get == 'raw':
    				doGetRaw(hw, outfile, order, opentype)

    			if get == 'hit':
    				doGetHit(hw, outtype, opentype)
			opentype = 'a+'
			#doSetup(hw, sleep=False)
	end = time.time()
	print "time taken(s): " + str(end - start)


# allows for a single file to be processed multiple times, good for stability checks
def doLoadRepeat(repeat, hw, infile, get, order, outtype):
	opentype = "w+"
	start = time.time()
	if get == None:
		print "ERROR: No get method stated, this command can't be used with --read command, only --get <value>."
		sys.exit()
	infile.close()
	for i in range(repeat):
		file = open(infile.name, 'r')
		doLoad(hw, file)
    		if get == 'raw':
    			doGetRaw(hw, outfile, order, opentype)

    		if get == 'hit':
    			doGetHit(hw, outtype, opentype)
		opentype = "a+"
		#doSetup(hw, sleep=False)
	end = time.time()
	print "time taken(s): " + str(end - start)




# reads from all 4 buffers
def doRead(hw, outtype):
        for sinkNo in range(4):
                readFromSink(hw, sinkNo, outtype)
	print "data written to output files, 1 for each buffer."

# converts raw data into 32bit hex, useful for comparing input and output data
def doGetRaw(hw, outtype, tryOrder, opentype = 'w+'):
	file = open("RawOut.txt", opentype)
	dataArrs = []
	packetPerSink = 0
	for sinkNo in range(4):
		arr = getFromSink(hw, sinkNo, outtype)
		if arr is None:
			pass
		else:
			print len(arr)
			packetPerSink = len(arr)
			dataArrs.append(arr)
	flatd = []
	if tryOrder is False:
		for i in range(4):
    			for j in range(len(dataArrs[i])):
        			dataArrs[i][j][0] = [flatd.append(x) for x in dataArrs[i][j][0]]
        			dataArrs[i][j][1] = [flatd.append(x) for x in dataArrs[i][j][1]]
	elif tryOrder is True:
		ordered = []
		for i in range(packetPerSink):
			[ordered.append(dataArrs[x][i]) for x in range(4)]
		for i in range(len(ordered)):
			[flatd.append(x) for x in ordered[i][0]]
			[flatd.append(x) for x in ordered[i][1]]
	flatd = [padhex(x) for x in flatd]
	for i in range(len(flatd)):
		file.write(flatd[i] + os.linesep)


# converts data into dtpc unpacker format, one file outputted per sink.
def doGetHit(hw, outtype, opentype = 'w+'):
	for sinkNo in range(4):
		file = open("out_sink_" + str(sinkNo+1) + ".txt", opentype)
		packetPerSink = 0
		arr = getFromSink(hw, sinkNo, outtype)
		if arr is None:
			pass
		else:
			print len(arr)
		arrf = []
		for i in range(len(arr)):
			[arrf.append(x) for x in arr[i]]
		arr = arrf
		arrf = []
		for i in range(len(arr)):
			[arrf.append(x) for x in arr[i]]
		arr = [padhex(x) for x in arrf]
		for i in range(len(arr)):
			line = arr[i][-5:]
			flag = bin(int(line[0]))[2:].zfill(2)
			line = line[1:] + ' ' + flag[0] + ' ' + flag[1]
			file.write(line + os.linesep)


def checkStatus(hw):
        for sinkNo in range(4):
                doStatus(hw, sinkNo)


def doStatus(hw, sinkNo):
	sCmd = "sink%s.csr.stat" %sinkNo
    	print "sCmd = " , sCmd
	s = hw.getNode("sink%s.csr.stat" %sinkNo).read()
	c = hw.getNode("sink%s.buf.count" %sinkNo).read()
	hw.dispatch()
        print "Sink status , sink count = " , s , c
	return sCmd, s, c


@click.command()
@click.option('--device', '-d', help='Device name')
@click.option('--infile', '-i', help='Input file', type=click.File('r'))
@click.option('--outfile', '-o', help='Output file name', type=click.File('w'), default='RawOut.txt')
@click.option('--intype', type=click.Choice(['phil', 'dpr']), default='phil')
@click.option('--outtype', type=click.Choice(['txt']), default='txt')
@click.option('--setup/--no-setup', is_flag=True, help='Setup', default=True)
@click.option('--load/--no-load', help='Load input patterns', default=False)
@click.option('--loadmulti/--no-loadmulti', help='Load multiple input patterns', default=False)
@click.option('--repeat', '-r', help='repeat analysis of file multiple times, if 0 function is not called', type=click.INT,default=0)
@click.option('--directory', '-dir', help='directory to read multiple patterns from')
@click.option('--play/--no-play', help='Play patterns', default=False)
@click.option('--status', is_flag=True, help='Checks sink and csr status', default=False)
@click.option('--read/--no-read', help='Read output patterns', default=False)
@click.option('--get', type=click.Choice(['raw', 'hit']), help='either "raw" (1 file) or "hit" (1 file per sink)')
@click.option('--order/--no-order', help='Should doConvert order the payload', default=True)
def doit(device, infile, outfile, intype, outtype, setup, load, loadmulti, repeat, play, status, get, read, order, directory):
        print('Welcome to the ZCU Butler')
        hw = init(device)
        if setup:
                doSetup(hw)
                
        if load:
                if intype=='phil':
                        doLoadPhil(hw, infile)
                else:
                        doLoad(hw, infile)

	if loadmulti:
		doLoadMulti(hw, directory, get, order, outfile, outtype)
		return

	if repeat != 0:
		doLoadRepeat(repeat, hw, infile, get, order, outtype)
		return

        if play:
                doPlay(hw)

    	if status:
    		  checkStatus(hw)
    
    	if get == 'raw':
    		doGetRaw(hw, outfile, order)

    	if get == 'hit':
    		doGetHit(hw, outtype)

        if read:
                doRead(hw, outtype)
	
if __name__ == '__main__':
        doit()
