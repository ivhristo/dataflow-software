"""
Created on Fri Oct 4 2019

@author: Kunal Kothekar

This file is used to create a WIB Data Rx format from a Phil file.

"""

import sys
#import numpy as np
import random

import click

# WIB packet contains ADC values for 256 channels x 1 clock tick
# arranged as 4 COLDATA chips x 8 ADCs x 8 channels
# plus some headers

# create a WIB packet from ADC data
def wibPacket(slot, crate, fibre, version, tstamp, data):
    return {'slot'      : slot,
            'crate'     : crate,
            'fibre'     : fibre,
            'version'   : version,
            'tstamp'    : tstamp,
            'data'      : data}

# read a Phil file
# note that this assumes data is presented by increasing channel
# create a WIB packet from Phil File
def readPhilFile(file, nchan, fchan, nsamp):
    data = []
    #not known if should start at 0 or 1 to count crate etc in metadata
    channels = []
    crates = []
    slots = []
    fibres = []

    n=0
    for l in file:
        chars = l.split()
        channels.append(chars[1])

        if (int(chars[1])==int(fchan) or n>0):
            data.append([int(x) for x in chars[3:]])
            n+=1
        if n==nchan: break
    
    for chan in channels:
        chan2 = int(int(chan) + fchan)#make sure we only include the channels we care about
        crateWise=int((chan2/2560) % 6)#up to 6 APAs (protoDUNE)
        slotWise=int((chan2/512) % 5)#5 slots
        fibreWise=int((chan2/256) % 2)#4 fibres but only use two (0-1)
        crates.append(int(crateWise))
        slots.append(int(slotWise))
        fibres.append(int(fibreWise))

    packets = []
    t = 0
    for i in range(nsamp):
        d = []
        for j in range(nchan):
            d.append(data[j][i])#+ i])#why t+i?
            #if(i==2):
            #    print(data[j][t + i])
        #currently don't have packets which carry different wires
        #all packets are for identical 256 wires so don't falsely change hdr info
        packets.append(wibPacket(slots[0],crates[0],fibres[0],0x2,t,d))
        t += 1
    
    return packets

"""Writes WIB packets into WIB data reception format (this is how data is recieved and is conveted to dpr format)"""
def formatWIB(packets, bits):
    data = []
    for (packet, ipacket) in zip(packets, range(len(packets))):

        # WIB headers
        data.append(padhex(0x3C, bits)[:-1] + ' 1') # SOF

        word = ipacket
        word += (packet['version'] & 0x1f)<<8
        word += (packet['fibre'] & 0x7)<<13
        word += (packet['crate'] & 0x3f)<<16
        # slot number not added 21
        # reserved 24
        # end 32
        data.append(padhex(word, bits)[:-1] + ' 0')
        # blank line for MM + OOS + reserved (14) + WIB errors
        word = 0x0
        data.append(padhex(word, bits)[:-1] + ' 0')
        # timestamp split into 2 lines
        word = (packet['tstamp'] & 0xffff)
        data.append(padhex(word, bits)[:-1] + ' 0')
        word = (packet['tstamp'] & ~0xffff)
        data.append(padhex(word, bits)[:-1] + ' 0')
        # skiped Z (don't know what that is)

        for n in range(4):
            # COLDATA headers, checksums - not filled yet
            for i in range(4):
                data.append(padhex(0x0, bits)[:-1] + ' 0')
            p = packet['data']
            for i in range(64 * n, 64 * (n + 1), 16):
                # 1st row
                A1C1 = p[i]
                A1C2 = p[i+1]
                A2C1 = p[i+8]
                A2C2 = p[i+9]
                word  = (A1C1 & 0x00ff)
                word += (A2C1 & 0x00ff)<<8
                word += (A1C1 & ~0x00ff)<<8
                word += (A1C2 & 0x000f)<<20
                word += (A2C1 & ~0x00ff)<<16
                word += (A2C2 & 0x000f)<<28
                data.append(padhex(word, bits)[:-1] + ' 0')

                # 2nd row
                A1C3 = p[i+2]
                A2C3 = p[i+10]
                word  = (A1C2 & ~0x000f)>>4
                word += (A2C1 & ~0x000f)<<4
                word += (A1C3 & 0x00ff)<<16
                word += (A2C3 & 0x00ff)<<24
                data.append(padhex(word, bits)[:-1] + ' 0')
            
                # 3rd row
                A1C4 = p[i+3]
                A2C4 = p[i+11]
                word  = (A1C3 & ~0x00ff)>>8
                word += (A1C4 & 0x000f)<<4
                word += (A2C3 & ~0x00ff)
                word += (A2C4 & 0x000f)<<12
                word += (A1C4 & ~0x000f)<<12
                word += (A2C4 & ~0x000f)<<20
                data.append(padhex(word, bits)[:-1] + ' 0')
            
                # 4th row
                A1C1 = p[i+4]
                A1C2 = p[i+5]
                A2C1 = p[i+12]
                A2C2 = p[i+13]
                word  = (A1C1 & 0x00ff)
                word += (A2C1 & 0x00ff)<<8
                word += (A1C1 & ~0x00ff)<<8
                word += (A1C2 & 0x000f)<<20
                word += (A2C1 & ~0x00ff)<<16
                word += (A2C2 & 0x000f)<<28
                data.append(padhex(word, bits)[:-1] + ' 0')
            
                # 5th row
                A1C3 = p[i+6]
                A2C3 = p[i+14]
                word  = (A1C2 & ~0x000f)>>4
                word += (A2C1 & ~0x000f)<<4
                word += (A1C3 & 0x00ff)<<16
                word += (A2C3 & 0x00ff)<<24
                data.append(padhex(word, bits)[:-1] + ' 0')
            
                # 6th row
                A1C4 = p[i+7]
                A2C4 = p[i+15]
                word  = (A1C3 & ~0x00ff)>>8
                word += (A1C4 & 0x000f)<<4
                word += (A2C3 & ~0x00ff)
                word += (A2C4 & 0x000f)<<12
                word += (A1C4 & ~0x000f)<<12
                word += (A2C4 & ~0x000f)<<20
                data.append(padhex(word, bits)[:-1] + ' 0')
        # footer to be added
        EOF = 0xDC
        k28p5 = 0xBC
        word = EOF
        word += 0x0 # don't know what CRC-20 is
        data.append(padhex(word, bits)[:-1] + ' 1')
        word = k28p5
        data.append(padhex(word, bits)[:-1] + ' 1')
        data.append(padhex(word, bits)[:-1] + ' 1')

    return data

"""Writes WIB packets into WIB data reception format (this is how data is recieved and is conveted to dpr format)"""
def formatWIB(packets, bits):
    data = []
    for (packet, ipacket) in zip(packets, range(len(packets))):

        # WIB headers
        data.append(padhex(0x3C, bits)[:-1] + ' 1') # SOF

        word = ipacket
        word += (packet['version'] & 0x1f)<<8
        word += (packet['fibre'] & 0x7)<<13
        word += (packet['crate'] & 0x3f)<<16
        # slot number not added 21
        # reserved 24
        # end 32
        data.append(padhex(word, bits)[:-1] + ' 0')
        # blank line for MM + OOS + reserved (14) + WIB errors
        word = 0x0
        data.append(padhex(word, bits)[:-1] + ' 0')
        # timestamp split into 2 lines
        word = (packet['tstamp'] & 0xffff)
        data.append(padhex(word, bits)[:-1] + ' 0')
        word = (packet['tstamp'] & ~0xffff)
        data.append(padhex(word, bits)[:-1] + ' 0')
        # skiped Z (don't know what that is)

        for n in range(4):
            # COLDATA headers, checksums - not filled yet
            for i in range(4):
                data.append(padhex(0x0, bits)[:-1] + ' 0')
            p = packet['data']
            for i in range(64 * n, 64 * (n + 1), 16):
                # 1st row
                A1C1 = p[i]
                A1C2 = p[i+1]
                A2C1 = p[i+8]
                A2C2 = p[i+9]
                word  = (A1C1 & 0x00ff)
                word += (A2C1 & 0x00ff)<<8
                word += (A1C1 & ~0x00ff)<<8
                word += (A1C2 & 0x000f)<<20
                word += (A2C1 & ~0x00ff)<<16
                word += (A2C2 & 0x000f)<<28
                data.append(padhex(word, bits)[:-1] + ' 0')

                # 2nd row
                A1C3 = p[i+2]
                A2C3 = p[i+10]
                word  = (A1C2 & ~0x000f)>>4
                word += (A2C1 & ~0x000f)<<4
                word += (A1C3 & 0x00ff)<<16
                word += (A2C3 & 0x00ff)<<24
                data.append(padhex(word, bits)[:-1] + ' 0')
            
                # 3rd row
                A1C4 = p[i+3]
                A2C4 = p[i+11]
                word  = (A1C3 & ~0x00ff)>>8
                word += (A1C4 & 0x000f)<<4
                word += (A2C3 & ~0x00ff)
                word += (A2C4 & 0x000f)<<12
                word += (A1C4 & ~0x000f)<<12
                word += (A2C4 & ~0x000f)<<20
                data.append(padhex(word, bits)[:-1] + ' 0')
            
                # 4th row
                A1C1 = p[i+4]
                A1C2 = p[i+5]
                A2C1 = p[i+12]
                A2C2 = p[i+13]
                word  = (A1C1 & 0x00ff)
                word += (A2C1 & 0x00ff)<<8
                word += (A1C1 & ~0x00ff)<<8
                word += (A1C2 & 0x000f)<<20
                word += (A2C1 & ~0x00ff)<<16
                word += (A2C2 & 0x000f)<<28
                data.append(padhex(word, bits)[:-1] + ' 0')
            
                # 5th row
                A1C3 = p[i+6]
                A2C3 = p[i+14]
                word  = (A1C2 & ~0x000f)>>4
                word += (A2C1 & ~0x000f)<<4
                word += (A1C3 & 0x00ff)<<16
                word += (A2C3 & 0x00ff)<<24
                data.append(padhex(word, bits)[:-1] + ' 0')
            
                # 6th row
                A1C4 = p[i+7]
                A2C4 = p[i+15]
                word  = (A1C3 & ~0x00ff)>>8
                word += (A1C4 & 0x000f)<<4
                word += (A2C3 & ~0x00ff)
                word += (A2C4 & 0x000f)<<12
                word += (A1C4 & ~0x000f)<<12
                word += (A2C4 & ~0x000f)<<20
                data.append(padhex(word, bits)[:-1] + ' 0')
        # footer to be added
        EOF = 0xDC
        k28p5 = 0xBC
        word = EOF
        word += 0x0 # don't know what CRC-20 is
        data.append(padhex(word, bits)[:-1] + ' 1')
        word = k28p5
        data.append(padhex(word, bits)[:-1] + ' 1')
        data.append(padhex(word, bits)[:-1] + ' 1')

    return data

"""can convert data into a hex number of defined bit length"""
def padhex(d, bits):
    return '0x'+hex(d)[2:].zfill(int(bits/4))+'\n'


"""Writes dpr and wib files"""
def writeHexfile(data, file, bits):
    for d in data:
        file.write(padhex(d, bits))

infile = open(sys.argv[1], "r+")

"""Creates data in WIB data reception format"""
def testWIBFormat(file):
    bits = 32  # length of hex words to write to
    nchan = 256
    fchan = 1600
    nsamp = 128
    packets = readPhilFile(infile, nchan, fchan, nsamp)  # create packets
    data = formatWIB(packets, bits)  # converts to WIB data reception
    for d in data:
        file.write(d+'\n')  # write to file

def run(formattype, outfile):
    if formattype == 'dpr':
        simpleFile(outfile)
    if formattype == 'wib':
        testWIBFormat(outfile)
    outfile.close()


file = open(sys.argv[2], "w+")
ftype = 'wib'


if __name__ == '__main__':
    run(ftype, file)
