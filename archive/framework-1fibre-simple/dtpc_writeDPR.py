#!/usr/bin/python

# -*- coding: utf-8 -*-
import sys
import time
import uhal

#import numpy as np
#np.set_printoptions(formatter={'int':lambda x:hex(int(x))})


#### Define Functions....
###############################################################################################
def resetLogic(hardWare):

        #####################################
        print "Resetting logic"
        hardWare.getNode("csr.ctrl.soft_rst").write(1)
        hardWare.dispatch()
        csrVal = hardWare.getNode("csr.ctrl.soft_rst").read()
        hardWare.dispatch()
        print "CSR value after reset = " , csrVal

        if csrVal != 0:
                print "Warning. CSR not magically reset! Clearing"
                hardWare.getNode("csr.ctrl.soft_rst").write(0)
                hardWare.dispatch()
                csrVal = hardWare.getNode("csr.ctrl.soft_rst").read()
                hardWare.dispatch()
                print "CSR value after reset (should be zero) = " , csrVal


###############################################################################################
# Set up input and output Mux ( 1 = IPBus control )
#def setupDPRMux(hardWare,input_ctl , output_ctl):
def setupDPRMux(hardWare,mux_ctl):
        status = 0
        hardWare.getNode("dpr.config").write(mux_ctl)
        #hardWare.getNode("dpr.config.input_ctl").write(input_ctl & 0x00000001)
        #hardWare.getNode("dpr.config.output_ctl").write( (output_ctl<<1) & 0x00000002)
        hardWare.dispatch()

        return status
        
# Writes a 64-bit word to DPR
# N.B. Make sure that Mux is set to access write port from IPBus
def writeDprLongLongWord(hardWare, address , data):
        status = 0

        hardWare.getNode("dpr.write_data_low").write(data & 0xFFFFFFFF)
        hardWare.getNode("dpr.write_data_high").write((data>>32) & 0xFFFFFFFF)
        hardWare.getNode("dpr.write_addr").write(address)
        hardWare.dispatch()

        return status

# Read a 64-bit word to DPR
# N.B. Make sure that Mux is set to access write port from IPBus
def readDprLongLongWord(hardWare, address ):
        status = 0

        hardWare.getNode("dpr.read_addr").write(address)
        hardWare.dispatch() # set up read address. Put as separate dispatch to allow for read latency
        
        data_low = hardWare.getNode("dpr.read_data_low").read()
        data_high = hardWare.getNode("dpr.read_data_high").read()
        hardWare.dispatch()

        # print "low = " , hex(data_low)
        # print "high = ", hex(data_high)
        return data_low + (data_high<<32)

def writeWibPtr(hw,ptr_to_write):
        print "Writing %x to WIB frame pointer"% ptr_to_write
        hw.getNode("dpr.wib_frame_ptr_w").write(ptr_to_write)
        hw.dispatch()

# Write WIB data to DPR
def writeToDPR(hw,data):
        print "Writing data to DPR"
        nwordsInFrame = 75
        previousWIBFrame = -1

        for addr in range(len(data)):
                wibFrame  = addr / nwordsInFrame
                word = data[addr]
                if wibFrame != previousWIBFrame:
                        print "New Frame"
                        writeWibPtr(hw,wibFrame)

                print hex(addr) , hex(word)  ,hex(wibFrame) 
                writeDprLongLongWord(hw, addr , word)
                previousWIBFrame = wibFrame 

def readJimFile(f):
        data = []
        for line in f:
                data.append(int(line,16))
        return data

def readFromSink(hw, sinkNo ):
        sCmd = "sink%s.csr.stat" %sinkNo
        print "sCmd = " , sCmd
	s = hw.getNode("sink%s.csr.stat" %sinkNo).read()
	c = hw.getNode("sink%s.buf.count" %sinkNo).read()
	hw.dispatch()

        id = 0
        print " Sink status , sink count = " , s , c
	if s & 0x6 != 0:
		print "Warning - Buffer error"
		# sys.exit()
#	elif c != 0:
	if c != 0:
		l = int(c) + 1
                print "l = ", l
		d = hw.getNode("sink%s.buf.data" % sinkNo ).readBlock(l)
		hw.dispatch()
                print "d = ", d
                di = [int(x) for x in d]
		while l > 6:
                        print "Sink %s , Packet:"%sinkNo, hex(id)
                        id += 1
			print "\tSink %s , Header:"%sinkNo, [hex(x) for x in di[:6]]
                        del di[:6]
                        l -= 6
			p = []
			while True:
                                if len(di):
				        r = di.pop(0)
                                else:
                                        print "Warning - attemping to pop from empty list"
                                        l = 0
                                        break
				p.append(r)
				if r & 0x10000 != 0:
					l -= len(p)
					break
			print "\tSink %s , Payload:"%sinkNo, [hex(x) for x in p]


# Loop over all four data sinks and read contents.
def readFromAllSinks(hw):
        for sinkNo in range(4):
                readFromSink(hw,sinkNo)
                

#### Start of Main
###############################################################################################

uhal.setLogLevelTo(uhal.LogLevel.NOTICE)
manager = uhal.ConnectionManager("file://connections.xml")
print sys.argv
hw = manager.getDevice(sys.argv[1])
hw.getClient().setTimeoutPeriod(10000)


print "Reading the JimFile"
f = open(sys.argv[2], 'r')

data = readJimFile(f)
print "number of words in data = " ,len(data), " ( number of WIB frames = ",len(data)/75 , " )"

resetLogic(hw)

print "Setting Input Mux on DPR to IPBus control"
INPUT_MUX_IS_IPBUS = 1;
OUTPUT_MUX_IS_IPBUS = 2;
setupDPRMux(hw,INPUT_MUX_IS_IPBUS)

print "Enable sinks"
hw.getNode("sink0.csr.ctrl.en").write(1)
hw.getNode("sink1.csr.ctrl.en").write(1)
hw.getNode("sink2.csr.ctrl.en").write(1)
hw.getNode("sink3.csr.ctrl.en").write(1)
hw.dispatch()

print "Writing to DPR"
writeToDPR(hw,data)

print "Reading from output buffers"
readFromAllSinks(hw)


