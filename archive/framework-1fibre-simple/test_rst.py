#!/usr/bin/python

# -*- coding: utf-8 -*-
import sys
import time
import uhal

uhal.setLogLevelTo(uhal.LogLevel.NOTICE)
manager = uhal.ConnectionManager("file://connections.xml")
print sys.argv
hw = manager.getDevice(sys.argv[1])


print hw.id()
	
print "Reset"
hw.getNode("csr.ctrl.soft_rst").write(1)
hw.dispatch()
csrVal = hw.getNode("csr.ctrl.soft_rst").read()
hw.dispatch()
print "CSR value after reset = " , csrVal

if csrVal != 0:
    print "Warning. CSR not magically reset! Clearing"
    hw.getNode("csr.ctrl.soft_rst").write(0)
    hw.dispatch()
    csrVal = hw.getNode("csr.ctrl.soft_rst").read()
    hw.dispatch()
    print "CSR value after reset = " , csrVal
