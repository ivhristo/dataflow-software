#!/usr/bin/python

# -*- coding: utf-8 -*-
import sys
import time
import uhal

uhal.setLogLevelTo(uhal.LogLevel.NOTICE)
manager = uhal.ConnectionManager("file://connections.xml")
print sys.argv
hw = manager.getDevice(sys.argv[1])

N_CHANS = 64
N_SAMPS = 256
	
print "Reading the PhilPhile"
f = open(sys.argv[2], 'r')
data = []
i = 0
for l in f:
	data.append([int(x) for x in l.split()][2:])
        i += 1
        if i == N_CHANS: break
f.close()

print hw.id()
	
print "Reset"
hw.getNode("csr.ctrl.soft_rst").write(1)
hw.dispatch()
	
time.sleep(1)
	
print "Enable source and sink"
hw.getNode("src.csr.ctrl.en").write(1)
hw.getNode("sink.csr.ctrl.en").write(1)
hw.dispatch()
	
print "Filling buffer"
hw.getNode("src.buf.addr").write(0)
for i in range(N_SAMPS):
        d = []
        for j in range(N_CHANS):
                d.append(data[j][i])                
        hw.getNode("src.buf.data").writeBlock(d) # Channels
        hw.dispatch()

print "Starting playback"
hw.getNode("src.csr.ctrl.go").write(1)
hw.dispatch()
	
while True:
        v = hw.getNode("src.csr.stat.waiting").read()
        hw.dispatch()
        if v == 1: break
        time.sleep(1)
	
print "Done"
src_s = hw.getNode("src.csr.stat").read()
sink_s = hw.getNode("sink.csr.stat").read()
c = hw.getNode("sink.buf.count").read()
hw.dispatch()
	
print hex(src_s), hex(sink_s), hex(c)
	
d = hw.getNode("sink.buf.data").readBlock(int(c))
hw.dispatch()
	
for i in range(len(d)):
        print hex(i), hex(d[i])
