#threshold definition, gaussian of the hits must have a peak above this value
#decided to institute 20 hits on each channel
#hits can occur at he same time and overlap one another

import numpy as np
import click
import sys

##hits
#define number of hits to put on each channel
#define how far above threshold you want hits to be
##num_hits=20
##hit_above_thresh=50
##thresh=50
##hit_width_min=1
##hit_width_max=40

##cryostat information
#define number of APAs
##num_APAs=12
#define number of channels per APA
#channel numbers for 12 APAs range from 0-(12*2560) in offline
#to quicken script only look at 1 APA
#define number of ticks per channel
##num_ticks=4492
#number of collection channels per APA
##num_APA_col_chans=960
##num_APA_ind_chans=1600
##num_APA_chans=(num_APA_ind_chans+num_APA_col_chans)*num_APAs
#collection channel pedestal
##col_ped=500

##define the std dev and mean for gaussian noise distribution 
##noise_mean=0.0
##noise_sigma=2.5
##noise_samples=1000000
#create a ndarray of noise samples to draw from
##noise_float=np.random.normal(noise_mean,noise_sigma,noise_samples)
#discretize them
##noise=noise_float.astype(dtype=int)

##header information
#evt_no=1
#num_evts
##num_evts=1
#if(num_evts<2):
#  evts=np.arange(1,2)
#else:
#  evts=np.arange(1,num_evts)

#including 


def ListGenerator(option, chan, fchan):
    """
    Structure of list:
    p = [packet, packetNum, increaseThresh, centreOffset, allowHits]
    definitions:
        packets: which packet to place hit in
        packetNum : total number of packets to cycle through in a channel
        increaseThresh: how much to increase the amplitude of the hit by from the base value
        centreOffset: how much to offest the hit by in ticks
        allowHits: 0 if a hit shouldn't be placed in this channel, 1 if it should
    """
    p = [0, 0, 0, 0, 1]
    if option == 1:  # standard packet structure
        p[1] = 1
    if option == 2:
        if (chan - fchan) <= 63:  # hits in 1st packet for half the wires and hits in 2nd for second half of wires with centre offset
            p[0] = p[3] = 0
        if (chan - fchan) > 63:
            p[0] = 1
            p[3] = 5
        p[1] = 1
        p[2] = 0
    if option == 3:  # hits with amplitude increasing with wire number past wire number fchan
        p[1] = 1
        p[2] = chan - fchan
        if p[2] < 0:
            p[2] = 0
    if option == 4:  # only 2 hits. one in the first wire of the first packet and the second on the second wire of the seocnd pacekt
        if (chan - fchan) == 0:
            p[1] = 1
        if (chan - fchan) == 1:
            p[0] = 1
            p[1] = 1
            p[3] = 5
        if (chan - fchan) not in [0, 1]:
            p[4] = 0

    return p


def make_noise(noisemean,noisesig,noisesamples):
    #create a ndarray of noise samples to draw from
    noise_float = np.random.normal(noisemean, noisesig, noisesamples)
    #discretize them
    noise = noise_float.astype(dtype=int)
    print(noise[:5])
    return noise


def gaussian(x, amp, mu, sig):
    # creates gaussian pattern for array x
    return amp*np.exp(-np.power(x - mu, 2.) / (2 * np.power(sig, 2.)))


def PacketGenerator(gaussianoption, nticks, packetNum, peak, centre, width, fixedlength):
    if(gaussianoption == 1):  # create gaussian hits
        packet = np.linspace(0, nticks-1, nticks)  # generates tick values
        signal = gaussian(packet, peak, centre + 64*packetNum, width)  # generates gaussian signal
    elif(gaussianoption == 0):  # create square hits
        packet = np.zeros(nticks)
        hwidth = int(fixedlength/2)  # half width of square wave
        packet[centre-hwidth:centre+hwidth] = peak
        signal = packet
    return signal



@click.command()
#@click.option('--file','-f',help='Output file name',type=click.File('w'),default='FakePhilFile.txt')
@click.option('--filename','-f',help='Output file name',type=str,default='FakePhilFile.txt')
@click.option('--nhits', '-nh', help='number of randomly timed hits to add per channel', type=click.INT,default=1)
@click.option('--thresh','-t',help='threshold',type=click.INT,default=50)
@click.option('--exceedthresh','-et',help='hits are over threshold by -et ADC counts',type=click.INT,default=50)
@click.option('--hitwidthmin','-mn',help='minimum tick width of a hit',type=click.INT,default=1)
@click.option('--hitwidthmax','-mx',help='max tick width of a hit',type=click.INT,default=40)
@click.option('--napas','-na',help='number of APAs to use',type=click.INT,default=1)
@click.option('--nticks','-nt',help='number of ticks per channel',type=click.INT,default=4492)
@click.option('--ncolchans','-nc',help='number of collection chans per APA',type=click.INT,default=960)
@click.option('--nindchans','-ni',help='number of induction chans per APA',type=click.INT,default=1600)
@click.option('--colped','-pc',help='pedestal for col channels',type=click.INT,default=500)
@click.option('--noiseon','-no',help='1 is noise on, 0 if not',type=click.INT,default=1)
@click.option('--noisemean','-m',help='mean for noise - should be 0',type=click.INT,default=0)
@click.option('--noisesig','-s',help='sigma for noise - 2.5 is approx',type=click.INT,default=2.5)
@click.option('--noisesamples','-n',help='number of noise samples to draw from',type=click.INT,default=1000000)
@click.option('--nevts','-e',help='number of events to fake',type=click.INT,default=1)
#for single fixed (non-random timed) hit on 1 or more channels (identical on all chans)
@click.option('--randomtiming','-rt',help='0 for fixed start time of first hit, 1 for random',type=click.INT,default=1)
@click.option('--fixedtime','-ft',help='fixed time hit start in ticks',type=click.INT,default=10)
@click.option('--fixedlength','-fl',help='fixed length of hit',type=click.INT,default=1)
@click.option('--fixedsinglechan','-fs',help='put fixed hit on single channel denoted by this int. If -ve will put same hit on all chans',type=click.INT,default=-1)
#for single fixed (non-random-timed) Gaussian peak hit on 1 or more channels (identical on all chans)
@click.option('--gaussianoption','-go', help='use gaussian shaped hits', type=click.INT,default=0)#0 is off
@click.option('--gausswidth','-gw',help='std dev of gaussian',type=click.FLOAT,default=5)#default is tick 5 ticks std dev
@click.option('--fchan','-fc',help='first wire to have a signal', type=int,default=1600)
@click.option('--hittype','-pt',help='Type of pattern to use in signal generated. From 1-4',type=int,default=1)

def fakePhilFile(filename,hittype,nhits,thresh,exceedthresh,hitwidthmin,hitwidthmax,napas,nticks,ncolchans,nindchans,colped,noiseon,noisemean,noisesig,noisesamples,nevts,randomtiming,fixedtime,fixedlength,fixedsinglechan,gaussianoption,gausswidth,fchan):
    global collection
    global hit

    #fchan = 1600  # channels below this value don't matter
    noise=make_noise(noisemean,noisesig,noisesamples)  # create noise to add to signal
    print(noise[:5])
    #induction/collection 1/0
    ind_col=1

    #nAPA channels in offline sw format
    num_APA_chans=(nindchans+ncolchans)*napas

    #first create a vector of collection samples (all at pedestal)
    collection_base=np.full(nticks,colped,dtype=int)

    #create our output Phil file
    file=open(filename,"w")

    #now loop over evt
    for evt in range(nevts):
        #now loop over channels - continue if an induction channel
        for chan in range(num_APA_chans):
            if(chan%num_APA_chans<nindchans):
                continue
            #if collection do the following

            #add the hits to the data
            #vectors have the following structure: [packet, packetNum, increaseThresh, centreOffset, allowHits]
            p = ListGenerator(hittype, chan, fchan) # creates vectors to use in the data
            collection=collection_base
            # loop through the amount of packets you want to fill with hits
            for i in range(p[1]):
                # create the hit using PacektGenerator. takes in initial click value options plus list vectors for the hit type
                hit = PacketGenerator(gaussianoption, nticks, i + p[0], exceedthresh + p[2], fixedtime + p[3], gausswidth, fixedlength)
                collection=collection+hit  # add hit to collection signal
            #add the noise to the data
            if(noiseon==1):
                for i in range(nticks):
                    collection[i]=collection[i]+noise[np.random.randint(0,noisesamples)]
            #only collection wires can get this far
            ind_col=1
            #add the header to the front of the collection channel
            header=np.array([evt+1,chan,ind_col],dtype=int)
            collection=np.insert(collection,0,header)
            #write data to Phil file/array then file?
            #make sure you add a newline only when needed
            if(evt!=1 and chan!=nindchans):
                file.write('\n')

            for i in range(collection.size):
                if(i<nticks-1+header.size):
                    file.write(str(int(collection[i]))+' ')
                else:
                    file.write(str(int(collection[i])))
    
    print("Finished")
    return

if __name__ == '__main__':
    fakePhilFile()

"""
c = 32  # hit center
w = 5  # gaussian width
t = 50  # exceed thresh
b = 500  # pedestal
p = t + b  # peak value
g = 1  # are hits gaussian?
hittype = 'S'
if g == 1:
    hittype = 'G'

#name = "Phil" + hittype + "_C" + str(c) + "_W" + str(w) + "_P" + str(p) + ".txt"
name = 'test.txt'
file = open(name, "w")
fakePhilFile(file, 0, 1, 50, t, 1, 40, 1, 4492, 960, 1600, b, 0, 0, 2.5, 1000000, 1, 0, c, 20, -1, g, w)
file.close()
"""