#!/usr/bin/python

# -*- coding: utf-8 -*-
import sys
import time
import uhal

uhal.setLogLevelTo(uhal.LogLevel.NOTICE)
manager = uhal.ConnectionManager("file://connections.xml")
print sys.argv
hw = manager.getDevice(sys.argv[1])

N_CHANS = 64
N_SAMPS = 256
TOTAL_SAMPS = 256*256

print "Reading the PhilPhile"
f = open(sys.argv[2], 'r')
data = []
i = 0
for l in f:
	data.append([int(x) for x in l.split()][2:])
        i += 1
        if i == N_CHANS: break
f.close()
print "Channels:", hex(i), "Samples:", hex(len(data[0]))

samps = min(len(data[0]), TOTAL_SAMPS)

print hw.id()
	
print "Reset"
hw.getNode("csr.ctrl.soft_rst").write(1)
hw.dispatch()
	
time.sleep(1)
	
print "Enable source and sink"
hw.getNode("src.csr.ctrl.en").write(1)
hw.getNode("sink.csr.ctrl.en").write(1)
hw.dispatch()

t = 0
id = 0
hile t < (samps - N_SAMPS):

	print "Filling buffer", hex(t), hex(len(data[0]))
	hw.getNode("src.buf.addr").write(0)
	for i in range(N_SAMPS):
			d = []
			for j in range(N_CHANS):
					d.append(data[j][t + i])
			hw.getNode("src.buf.data").writeBlock(d) # Channels
			hw.dispatch()

	t += N_SAMPS;

	print "Starting playback"
	hw.getNode("src.csr.ctrl.go").write(1)
	hw.dispatch()
		
	while True:
			v = hw.getNode("src.csr.stat.waiting").read()
			hw.dispatch()
			if v == 1: break
			time.sleep(1)
		
	print "Done"
	s = hw.getNode("sink.csr.stat").read()
	c = hw.getNode("sink.buf.count").read()
	hw.dispatch()

	if s & 0x6 != 0:
		print "Buffer error"
		sys.exit()
	elif c != 0:
		l = int(c) + 1
		d = hw.getNode("sink.buf.data").readBlock(l)
		hw.dispatch()
                di = [int(x) for x in d]
		while l > 6:
                        print "Packet:", hex(id)
                        id += 1
			print "\tHeader:", [hex(x) for x in di[:6]]
                        del di[:6]
                        l -= 6
			p = []
			while True:
				r = di.pop(0)
				p.append(r)
				if r & 0x10000 != 0:
					l -= len(p)
					break
			print "\tPayload:", [hex(x) for x in p]
