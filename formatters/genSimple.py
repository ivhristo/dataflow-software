"""
Created on Fri Jun 28 15:49:22 2019

@author: Shyam Bhuller

Used to create simple test patterns for debugging/testing firmware in sim or hardware.
Uses many functionality from convertData.py, with the exception of reading Philfiles as of now.

"""
import random
import click


"""Create a WIB packet from ADC data"""


def wibPacket(crate, fibre, version, tstamp, data):
    return {
        'crate': crate,
        'fibre': fibre,
        'version': version,
        'tstamp': tstamp,
        'data': data,
    }


"""Create randomly generated coldata"""


def wibPacketRandom(data):
    for coldata in range(4):
        for adc in range(8):
            for chan in range(8):
                data.append(random.randint(0, 0x3FF))  # 0x3ff -> 1023


"""Create data which can track channel number"""


def channelPattern(data):
    for i in range(256):
        data.append(i)


"""Create data which can track sample number"""


def samplePattern(data, s):
    for i in range(256):
        data.append(s)


"""Create data which can track channel and sample number"""


def channelSamplePattern(data, s):
    # create data where 1st and 2nd digits are the channel number,
    # 3rd and 4th are samples (packet number)
    # e.g. second channel and 3rd sample is 0x0302
    for i in range(256):
        global sample
        sample = hex(s)
        channel = hex(i)
        d = int('0x' + sample[2:].zfill(2) + channel[2:].zfill(2), 16)
        data.append(d)


"""Creates data which tracks channel and sample number but is modified to work for the WIB file format type"""


def PatternSpecial(data, s):
    # note max value is 0xfff (4095) so data repeats itself for large samples
    for i in range(256):
        sample = hex(s % 16)
        channel = hex(i)
        d = int('0x' + sample[2:].zfill(2) + channel[2:].zfill(2), 16)
        data.append(d)


"""Creates data which tracks ADC number and channel number relative to ADC value"""


def DataADCChannel(data):
    # 8 ADC's for 8 channels
    for coldata in range(4):
        for adc in range(8):
            for chan in range(8):
                adch = hex(adc + 1)
                chanh = hex(chan + 1)
                d = int('0x' + adch[2:] + chanh[2:], 16)
                data.append(d)


"""Handles packet generation into WIB packets"""


def wibPacketSimple(t, sampleNum, pattern):
    # crate fibre and version not updated to accomodate wire number (see convertData.py)
    crate = 0x1
    fibre = 0x1
    version = 0x1
    tstamp = t
    data = []
    if pattern == "channel":
        channelPattern(data)
    if pattern == "sample":
        samplePattern(data, sampleNum)
    if pattern == "cs":
        channelSamplePattern(data, sampleNum)
    if pattern == "adcc":
        DataADCChannel(data)
    if pattern == "random":
        wibPacketRandom(data)
    if pattern == "special":
        PatternSpecial(data, sampleNum)

    return wibPacket(crate, fibre, version, tstamp, data)


"""Loops through samples to create packets and sends required data through"""


def createSimplePackets(n, t, pattern):
    packets = []
    for i in range(n):
        packets.append(wibPacketSimple(t + i, i, pattern))
    return packets


"""Writes WIB packets into Jim file format (can used in hardware)"""


def formatInputBuffer(packets):
    data = []
    for (packet, ipacket) in zip(packets, range(len(packets))):

        # WIB headers
        word = ipacket
        word += (packet['version'] & 0x1F) << 48
        word += (packet['fibre'] & 0x7) << 53
        word += (packet['crate'] & 0x3F) << 56
        data.append(word)

        # timestamp
        data.append(packet['tstamp'])

        # stream errors - not filled yet
        data.append(0x0)

        # COLDATA headers, checksums - not filled yet
        for i in range(8):
            data.append(0x0)

        # data
        for i in range(0, len(packet['data']), 4):
            word = packet['data'][i]
            word += packet['data'][i + 1] << 16
            word += packet['data'][i + 2] << 32
            word += packet['data'][i + 3] << 48
            data.append(word)
    return data


"""Writes WIB packets into WIB data reception format (this is how data is recieved and is conveted to dpr format)"""


def formatWIB(packets, bits):
    data = []
    for (packet, ipacket) in zip(packets, range(len(packets))):

        # WIB headers
        data.append(padhex(0x3C, bits)[:-1] + ' 1')  # SOF

        word = ipacket
        word += (packet['version'] & 0x1F) << 8
        word += (packet['fibre'] & 0x7) << 13
        word += (packet['crate'] & 0x3F) << 16
        # slot number not added 21
        # reserved 24
        # end 32
        data.append(padhex(word, bits)[:-1] + ' 0')
        # blank line for MM + OOS + reserved (14) + WIB errors
        word = 0x0
        data.append(padhex(word, bits)[:-1] + ' 0')
        # timestamp split into 2 lines
        word = packet['tstamp'] & 0xFFFF
        data.append(padhex(word, bits)[:-1] + ' 0')
        word = packet['tstamp'] & ~0xFFFF
        data.append(padhex(word, bits)[:-1] + ' 0')
        # skiped Z (don't know what that is)

        for n in range(4):
            # COLDATA headers, checksums - not filled yet
            for i in range(4):
                data.append(padhex(0x0, bits)[:-1] + ' 0')
            p = packet['data']
            for i in range(64 * n, 64 * (n + 1), 16):
                # 1st row
                A1C1 = p[i]
                A1C2 = p[i + 1]
                A2C1 = p[i + 8]
                A2C2 = p[i + 9]
                word = A1C1 & 0x00FF
                word += (A2C1 & 0x00FF) << 8
                word += (A1C1 & ~0x00FF) << 8
                word += (A1C2 & 0x000F) << 20
                word += (A2C1 & ~0x00FF) << 16
                word += (A2C2 & 0x000F) << 28
                data.append(padhex(word, bits)[:-1] + ' 0')

                # 2nd row
                A1C3 = p[i + 2]
                A2C3 = p[i + 10]
                word = (A1C2 & ~0x000F) >> 4
                word += (A2C1 & ~0x000F) << 4
                word += (A1C3 & 0x00FF) << 16
                word += (A2C3 & 0x00FF) << 24
                data.append(padhex(word, bits)[:-1] + ' 0')

                # 3rd row
                A1C4 = p[i + 3]
                A2C4 = p[i + 11]
                word = (A1C3 & ~0x00FF) >> 8
                word += (A1C4 & 0x000F) << 4
                word += A2C3 & ~0x00FF
                word += (A2C4 & 0x000F) << 12
                word += (A1C4 & ~0x000F) << 12
                word += (A2C4 & ~0x000F) << 20
                data.append(padhex(word, bits)[:-1] + ' 0')

                # 4th row
                A1C1 = p[i + 4]
                A1C2 = p[i + 5]
                A2C1 = p[i + 12]
                A2C2 = p[i + 13]
                word = A1C1 & 0x00FF
                word += (A2C1 & 0x00FF) << 8
                word += (A1C1 & ~0x00FF) << 8
                word += (A1C2 & 0x000F) << 20
                word += (A2C1 & ~0x00FF) << 16
                word += (A2C2 & 0x000F) << 28
                data.append(padhex(word, bits)[:-1] + ' 0')

                # 5th row
                A1C3 = p[i + 6]
                A2C3 = p[i + 14]
                word = (A1C2 & ~0x000F) >> 4
                word += (A2C1 & ~0x000F) << 4
                word += (A1C3 & 0x00FF) << 16
                word += (A2C3 & 0x00FF) << 24
                data.append(padhex(word, bits)[:-1] + ' 0')

                # 6th row
                A1C4 = p[i + 7]
                A2C4 = p[i + 15]
                word = (A1C3 & ~0x00FF) >> 8
                word += (A1C4 & 0x000F) << 4
                word += A2C3 & ~0x00FF
                word += (A2C4 & 0x000F) << 12
                word += (A1C4 & ~0x000F) << 12
                word += (A2C4 & ~0x000F) << 20
                data.append(padhex(word, bits)[:-1] + ' 0')
        # footer to be added
        EOF = 0xDC
        k28p5 = 0xBC
        word = EOF
        word += 0x0  # don't know what CRC-20 is
        data.append(padhex(word, bits)[:-1] + ' 1')
        word = k28p5
        data.append(padhex(word, bits)[:-1] + ' 1')
        data.append(padhex(word, bits)[:-1] + ' 1')

    return data


"""can convert data into a hex number of defined bit length"""


def padhex(d, bits):
    return '0x' + hex(d)[2:].zfill(int(bits / 4)) + '\n'


"""Writes dpr and wib files"""


def writeHexfile(data, file, bits):
    for d in data:
        file.write(padhex(d, bits))


"""Creates data in dpr format"""


def simpleFile(file, pattern):
    filePtr = open(file, "w+")
    bits = 64  # length of hex words to write to
    pacNum = 128  # sample number
    packets = createSimplePackets(pacNum, 1, pattern)  # create packets
    writeHexfile(formatInputBuffer(packets), filePtr, bits)  # converts to Jimfile format
    filePtr.close()

"""Creates data in WIB data reception format"""


def testWIBFormat(file, pattern):
    filePtr = open(file, "w+")
    bits = 32  # length of hex words to write to
    pacNum = 128  # sample number
    packets = createSimplePackets(pacNum, 1, pattern)  # create packets
    data = formatWIB(packets, bits)  # converts to WIB data reception
    for d in data:
        filePtr.write(d + '\n')  # write to file
    filePtr.close()

@click.command()
@click.option('--formattype', '-ft', type=click.Choice(['dpr', 'wib']), default='dpr')
@click.option('--outfile', '-o', help='Output file name', default='out.txt')
@click.option('--pattern', '-p', type=click.Choice(['channel', 'sample', 'cs', 'adcc', 'random', 'special']), default='channel')



def run(formattype, outfile, pattern):
    print "Outfile = " , outfile
    print "Format type = " , formattype
    print "Pattern = ", pattern
    

    #ftype = 'wib'

    if formattype == 'dpr':
        simpleFile(outfile, pattern)
    if formattype == 'wib':
        testWIBFormat(outfile, pattern)

if __name__ == '__main__':
        run()
