import sys

# import numpy as np
import random

import click

# WIB packet contains ADC values for 256 channels x 1 clock tick
# arranged as 4 COLDATA chips x 8 ADCs x 8 channels
# plus some headers

# create a WIB packet from ADC data
def wibPacket(slot, crate, fibre, version, tstamp, data):
    return {
        'slot': slot,
        'crate': crate,
        'fibre': fibre,
        'version': version,
        'tstamp': tstamp,
        'data': data,
    }


# create a fake WIB packet with random ADC values
def wibPacketRandom(t):
    slot = 0x1
    crate = 0x1
    fibre = 0x1
    version = 0x1
    tstamp = t
    data = []
    for coldata in range(4):
        for adc in range(8):
            for chan in range(8):
                data.append(random.randint(0, 0x3FF))

    return wibPacket(slot, crate, fibre, version, tstamp, data)


def createRandomPackets(n, t):
    packets = []
    for i in range(n):
        packets.append(wibPacketRandom(t + i))
    return packets


def outputHeaderFormat(hit):
    cratemask = int("1111100000000000", 2)
    fibremask = int("0000011100000000", 2)
    wireIndexmask = int("0000000011111100", 2)  # v1
    pipelinemask = int("0000000000000011", 2)  # v1
    # wireIndexmask=int("0000000000111111",2)#v2
    # pipelinemask=int("0000000011000000",2)#v2

    slotmask = int("1110000000000000", 2)
    hdrflagsmask = int("0001111111111111", 2)

    # no masks needed for timestamps

    crate = cratemask & hit[0]
    fibre = fibremask & hit[0]
    wireIndex = wireIndexmask & hit[0]
    pipeline = pipelinemask & hit[0]

    slot = slotmask & hit[1]
    hdrflags = hdrflagsmask & hit[1]

    timestamp = hit[2] + (hit[3] << 16) + (hit[4] << 32) + (hit[5] << 48)

    return {
        'slot': slot,
        'crate': crate,
        'fibre': fibre,
        'wireIndex': wireIndex,
        'pipeline': pipeline,
        'hdrflags': hdrflags,
        'timestamp': timestamp,
    }


def outputHitFormat(hit):

    # masks only needed for word 11
    hit_continuesmask = int("1000000000000000", 2)
    hitflagsmask = int("0111111111111111", 2)

    hitstart = hit[6]
    hitend = hit[7]
    hitpeakadc = hit[8]
    hitpeaktime = hit[9]
    hitsumadc = hit[10]
    hit_continues = hit_continuesmask & hit[11]
    hitflags = hitflagsmask & hit[11]

    return {
        'hitstart': hitstart,
        'hitend': hitend,
        'hitpeakadc': hitpeakadc,
        'hitpeaktime': hitpeaktime,
        'hitsumadc': hitsumadc,
        'hit_continues': hit_continues,
        'hitflags': hitflags,
    }


# format a list of WIB packets in input buffer format
def formatInputBuffer(packets):
    data = []
    for (packet, ipacket) in zip(packets, range(len(packets))):

        # WIB headers
        word = ipacket
        word += (packet['version'] & 0x1F) << 46
        word += (packet['fibre'] & 0x7) << 51
        word += (packet['crate'] & 0x1F) << 54
        word += (packet['slot'] & 0x7) << 59
        data.append(word)

        # timestamp
        data.append(packet['tstamp'])

        # stream errors - not filled yet
        data.append(0x0)

        # COLDATA headers, checksums - not filled yet
        for i in range(8):
            data.append(0x0)

        # data
        for i in range(0, len(packet['data']), 4):
            word = packet['data'][i]
            word += packet['data'][i + 1] << 16
            word += packet['data'][i + 2] << 32
            word += packet['data'][i + 3] << 48
            data.append(word)

    return data


def padhex(d):
    return '0x' + hex(d)[2:].zfill(16) + '\n'


def printHexFile(data):
    for d in data:
        print(padhex(d))


def writeHexfile(data, file):
    for d in data:
        file.write(padhex(d))


# read a Phil file
# note that this assumes data is presented by increasing channel #
def readPhilFile(file, nchan, fchan, nsamp):
    data = []
    # not known if should start at 0 or 1 to count crate etc in metadata
    channels = []
    crates = []
    slots = []
    fibres = []

    n = 0
    for l in file:
        chars = l.split()
        channels.append(chars[1])

        if int(chars[1]) == int(fchan) or n > 0:
            data.append([int(x) for x in chars[3:]])
            n += 1
        if n == nchan:
            break

    for chan in channels:
        chan2 = int(
            int(chan) + fchan
        )  # make sure we only include the channels we care about
        crateWise = int((chan2 / 2560) % 6)  # up to 6 APAs (protoDUNE)
        slotWise = int((chan2 / 512) % 5)  # 5 slots
        fibreWise = int((chan2 / 256) % 2)  # 4 fibres but only use two (0-1)
        crates.append(int(crateWise))
        slots.append(int(slotWise))
        fibres.append(int(fibreWise))

    packets = []
    t = 0
    for i in range(nsamp):
        d = []
        for j in range(nchan):
            d.append(data[j][i])  # + i])#why t+i?
            # if(i==2):
            #    print(data[j][t + i])
        # currently don't have packets which carry different wires
        # all packets are for identical 256 wires so don't falsely change hdr info
        packets.append(wibPacket(slots[0], crates[0], fibres[0], 0x2, t, d))
        t += 1

    return packets


# reads in the whole PhilFile and outputs a PhilFile with only
# the data that would make it into the inputbuffer
# probably want to add nchan,fchan,nsamp to click options
def readPhilFileToSubset(file, nchan, fchan, nsamp):
    data = []
    n = 0
    for l in file:
        chars = l.split()
        if int(chars[1]) == int(fchan) or n > 0:
            # instead of preserving channel number info it is dropped and
            # channel number is instead filled in from n+fchan
            # evt no always assumed be same as input and collection stays same as input
            # set channel number to n
            chars[1] = n + fchan
            # retain header and only nsamp adc values
            data.append([int(x) for x in chars[: nsamp + 3]])
            n += 1
        if n == nchan:
            break

    return data
    # now have desired subset of data in channel order with altered header


def writePhilFile(file, data, nchan, nsamp):
    for (chan, ichan) in zip(data, range(len(data))):
        writePhilChannel(file, chan, nsamp)
        # newline for all bar last channel
        if ichan + 1 < nchan:
            file.write('\n')


def writePhilChannel(file, chan, nsamp):
    for (chanadc, i) in zip(chan, range(len(chan))):
        file.write(str(chanadc))
        # don't add a space at the end of a channel
        if i + 1 < nsamp + 3:
            file.write(' ')


# want to read in hex as string, then convert to int, read out lsb first
# progressing to more significant bits. So adc1chan1 then adc1chan2...
def readInBuf(file, nchan, nsamp, fchan):
    data = []
    adcsamples = []
    packetno = 0
    row = 0
    for l in file:
        data.append(int(l, 16))
    # now have hex data in int format
    # dont care about anything from packet header at the moment
    # faking up:
    # evt no=1
    # chan_no starts at fchan
    # is collection always assumed collection

    # removing packet headers from data

    # taking each intger and extracting the 4 ADC samples from them
    # appending chans

    # split up processing by wib frame by counting how many rows through the `frame` you are
    for l in data:
        # create
        row += 1
        # check if in header still
        if row <= 11:
            continue
        # if adc data
        else:
            a = l - ((l >> 16) << 16)
            b = (l >> 16) - ((l >> 32) << 16)
            c = (l >> 32) - ((l >> 48) << 16)
            d = l >> 48
            adcsamples.append(int(a))
            adcsamples.append(int(b))
            adcsamples.append(int(c))
            adcsamples.append(int(d))

            # print(int(a))
            # print(int(b))
            # print(int(c))
            # print(int(d))
        # reset row if reach end of packet
        if row == int(nchan / 4) + 11:
            row = 0

    # now have adc samples in order adc1chan1 adc1chan2 adc1chan3... adc1chan256
    # in integer format
    # nchan samples per packet, with nsamp packets
    # so loop through list separating out samples from different channels

    # easiest way (not time efficient) is to read through list multiple times, once for each channel
    d = []  # holds all channels of channel-ordered data (phil file format)
    chandata = []  # holds 1 channel of channel-ordered data (single row of phil file)
    for chan in range(nchan):
        # reset list and start by filling with hdr
        chandata = [1, chan + fchan, 1]
        # print(chandata)
        for (adc, adci) in zip(adcsamples, range(len(adcsamples))):
            if adci % nchan == chan:
                # append adc values to list
                chandata.append(int(adc))

        d.append([int(x) for x in chandata])
    return d


def readInHitBuf(file):
    data = []
    # hold hdr and hit info for all hits in hitfileformat
    # hitfileformat is
    # evt chan col? hit_continues hitstart hitsize hitpeaktime hitpeakadc hitsumadc
    hits = []
    # hold info for a single hit
    hit = []
    row = 0

    for l in file:
        data.append(int(l, 16))

    # there are 3 forms of hit packet
    # 1) 1 hit in a packet
    # 2) multiple hits in a packet
    # 3) no hits in a packet (still has header)

    # all lines have only 18 significant bits, of which only 15 are used to store
    # hdr is 6 words long
    # hit_quant is 6 words long

    # if no data don't do anything and give warning
    if len(data) < 1:
        print("Error: no data. Returning empty hits list!")
        return hits

    hdr_frame = 1
    hdr_info = []
    hit_info = []

    for l in data:
        # need to reliably identify start of a hit packet
        # tl high at in last frame
        # tu high at end of packet and low in "last frame"
        # get tl
        tl = l >> 17
        # get tu
        tu = (l >> 16) - ((l >> 17) << 1)

        # if in last frame skip to next word and indicate next frame will be header
        if tl == 1:
            hdr_frame = 1
            # clear previous hdr information
            hdr_info = []
            continue
        # if it must be a hit frame
        elif tl == 0 and hdr_frame == 0:
            # fill up hit_info
            hit_info.append(l - ((l >> 16) << 16))
            # then combine with header info upon tu==1 and append
            # this vector which is now in format of 1 row of hitfileformat file
            # to list of vectors which will make up the hitfileformat file to be printed
            if tu == 1:
                for (word, i) in zip(hdr_info, range(len(hdr_info))):
                    hit.append(int(hdr_info[i]))
                for (word, i) in zip(hit_info, range(len(hit_info))):
                    hit.append(int(hit_info[i]))
                # after appending hits list with hit, clear previous hit_info
                hit_info = []

                # now have hdr and hit for this hit from output memory map
                # decode all information taken out of hdr+hit
                hdrdecode, hitdecode = outputHeaderFormat(hit), outputHitFormat(hit)

                # append hits with another hit from the decoded info
                hitfilehit = []
                hitfilehit.append(int(1))  # evt no
                pseudochannel = (hdrdecode['crate'] * 2560) + (
                    (hdrdecode['slot'] * 512)
                    + (
                        (hdrdecode['fibre'] * 256)
                        + ((hdrdecode['wireIndex']) + hdrdecode['pipeline'])
                    )
                )
                hitfilehit.append(int(pseudochannel))
                if int(pseudochannel) % 2560 >= 1600:
                    hitfilehit.append(int(1))
                else:
                    hitfilehit.append(int(0))  # col=1 ind=0
                hitfilehit.append(int(hitdecode['hit_continues']))  # hit_continues
                hitfilehit.append(
                    int(hdrdecode['timestamp'] + hitdecode['hitstart'])
                )  # hit_start_tick(offline)
                hitfilehit.append(
                    int(hitdecode['hitend'] - hitdecode['hitstart'])
                )  # hit_size
                hitfilehit.append(
                    int(hdrdecode['timestamp'] + hitdecode['hitpeaktime'])
                )  # hitpeaktime(offline)
                hitfilehit.append(int(hitdecode['hitpeakadc']))  # hitpeakadc
                hitfilehit.append(int(hitdecode['hitsumadc']))  # hitsumadc
                hit = []  # clear for next loop iteration

                # append list to hits list of hits
                hits.append([int(x) for x in hitfilehit])

        # last frame was "last frame" type so this frame is hdr frame
        elif hdr_frame == 1:
            # do hdr frame work
            hdr_info.append(l - ((l >> 16) << 16))
            if tu == 1:
                hdr_frame = 0

    return hits


def readInHitBufTestBench(file):
    data = []
    valid = []
    user = []
    last = []
    keep = []
    # hold hdr and hit info for all hits in hitfileformat
    # hitfileformat is
    # evt chan col? hit_continues hitstart hitsize hitpeaktime hitpeakadc hitsumadc
    hits = []
    # hold info for a single hit
    hit = []
    row = 0

    for l in file:
        # get list for each line of file
        # line format:
        # tdata tvalid tuser tlast tkeep
        line = l.split()
        # with this kind of input file we only want to keep
        # data when tvalid is high
        # so only keep lines where tvalid is high
        tvalid = int(line[1], 2)
        if tvalid == 1:
            # data will hold all tdata
            data.append(int(line[0], 16))
            valid.append(int(line[1], 16))
            user.append(int(line[2], 16))
            last.append(int(line[3], 16))
            keep.append(int(line[4], 16))

    # check we have some data to work with. If not give warning
    if len(data) < 1:
        print("Error: no valid data to work with. Returning empty hits list!")
        return hits

    # there are 3 forms of hit packet
    # 1) 1 hit in a packet
    # 2) multiple hits in a packet
    # 3) no hits in a packet (still has header)

    # hdr is 6 words long
    # hit_quant is 6 words long
    # always assume the first thing we see is start of the header frame

    hdr_frame = 1
    hdr_info = []
    hit_info = []

    for (l, lcount) in zip(data, range(len(data))):
        # need to reliably identify start of a hit packet
        # tl high at in last frame
        # tu high at end of packet and low in "last frame"
        # get tl
        tl = last[lcount]
        # get tu
        tu = user[lcount]

        # if in last frame skip to next word and indicate next frame will be header
        if tl == 1:
            hdr_frame = 1
            # clear previous hdr information
            hdr_info = []
            continue
        # if it must be a hit frame
        elif tl == 0 and hdr_frame == 0:
            # fill up hit_info
            hit_info.append(l)
            # then combine with header info upon tu==1 and append
            # this vector which is now in format of 1 row of hitfileformat file
            # to list of vectors which will make up the hitfileformat file to be printed
            if tu == 1:
                for (word, i) in zip(hdr_info, range(len(hdr_info))):
                    hit.append(int(hdr_info[i]))
                for (word, i) in zip(hit_info, range(len(hit_info))):
                    hit.append(int(hit_info[i]))
                # after appending hits list with hit, clear previous hit_info
                hit_info = []

                # now have hdr and hit for this hit from output memory map
                # decode all information taken out of hdr+hit
                hdrdecode, hitdecode = outputHeaderFormat(hit), outputHitFormat(hit)

                # append hits with another hit from the decoded info
                hitfilehit = []
                hitfilehit.append(int(1))  # evt no
                pseudochannel = (hdrdecode['crate'] * 2560) + (
                    (hdrdecode['slot'] * 512)
                    + (
                        (hdrdecode['fibre'] * 256)
                        + ((hdrdecode['wireIndex']) + hdrdecode['pipeline'])
                    )
                )
                hitfilehit.append(pseudochannel)
                if int(pseudochannel) % 2560 >= 1600:
                    hitfilehit.append(int(1))
                else:
                    hitfilehit.append(int(0))  # col=1 ind=0
                hitfilehit.append(int(hitdecode['hit_continues']))  # hit_continues
                hitfilehit.append(
                    int(hdrdecode['timestamp'] + hitdecode['hitstart'])
                )  # hit_start_tick(offline)
                hitfilehit.append(
                    int(hitdecode['hitend'] - hitdecode['hitstart'])
                )  # hit_size
                hitfilehit.append(
                    int(hdrdecode['timestamp'] + hitdecode['hitpeaktime'])
                )  # hitpeaktime(offline)
                hitfilehit.append(int(hitdecode['hitpeakadc']))  # hitpeakadc
                hitfilehit.append(int(hitdecode['hitsumadc']))  # hitsumadc
                hit = []  # clear for next loop iteration

                # append list to hits list of hits
                hits.append([int(x) for x in hitfilehit])

        # last frame was "last frame" type so this frame is hdr frame
        elif hdr_frame == 1:
            # do hdr frame work
            hdr_info.append(l)
            if tu == 1:
                hdr_frame = 0

    return hits


# similar to the above function but does so for the hardware output format: 0000 0 0 (tdata) (tuser) (tlast)
def readInHitBufHardware(file):
    data = []
    user = []
    last = []
    # hold hdr and hit info for all hits in hitfileformat
    # hitfileformat is
    # evt chan col? hit_continues hitstart hitsize hitpeaktime hitpeakadc hitsumadc
    hits = []
    # hold info for a single hit
    hit = []
    row = 0

    for l in file:
        # get list for each line of file
        # line format:
        # tdata tuser tlast
        line = l.split()
        # data will hold all tdata
        data.append(int(line[0], 16))
        user.append(int(line[1], 16))
        last.append(int(line[2], 16))

    # check we have some data to work with. If not give warning
    if len(data) < 1:
        print("Error: no valid data to work with. Returning empty hits list!")
        return hits

    # there are 3 forms of hit packet
    # 1) 1 hit in a packet
    # 2) multiple hits in a packet
    # 3) no hits in a packet (still has header)

    # hdr is 6 words long
    # hit_quant is 6 words long
    # always assume the first thing we see is start of the header frame

    hdr_frame = 1
    hdr_info = []
    hit_info = []

    for (l, lcount) in zip(data, range(len(data))):
        # need to reliably identify start of a hit packet
        # tl high at in last frame
        # tu high at end of packet and low in "last frame"
        # get tl
        tl = last[lcount]
        # get tu
        tu = user[lcount]

        # if in last frame skip to next word and indicate next frame will be header
        if tl == 1:
            hdr_frame = 1
            # clear previous hdr information
            hdr_info = []
            continue
        # if it must be a hit frame
        elif tl == 0 and hdr_frame == 0:
            # fill up hit_info
            hit_info.append(l)
            # then combine with header info upon tu==1 and append
            # this vector which is now in format of 1 row of hitfileformat file
            # to list of vectors which will make up the hitfileformat file to be printed
            if tu == 1:
                for (word, i) in zip(hdr_info, range(len(hdr_info))):
                    hit.append(int(hdr_info[i]))
                for (word, i) in zip(hit_info, range(len(hit_info))):
                    hit.append(int(hit_info[i]))
                # after appending hits list with hit, clear previous hit_info
                hit_info = []

                # now have hdr and hit for this hit from output memory map
                # decode all information taken out of hdr+hit
                hdrdecode, hitdecode = outputHeaderFormat(hit), outputHitFormat(hit)

                # append hits with another hit from the decoded info
                hitfilehit = []
                hitfilehit.append(int(1))  # evt no
                pseudochannel = (hdrdecode['crate'] * 2560) + (
                    (hdrdecode['slot'] * 512)
                    + (
                        (hdrdecode['fibre'] * 256)
                        + ((hdrdecode['wireIndex']) + hdrdecode['pipeline'])
                    )
                )
                hitfilehit.append(pseudochannel)
                if int(pseudochannel) % 2560 >= 1600:
                    hitfilehit.append(int(1))
                else:
                    hitfilehit.append(int(0))  # col=1 ind=0
                hitfilehit.append(int(hitdecode['hit_continues']))  # hit_continues
                hitfilehit.append(
                    int(hdrdecode['timestamp'] + hitdecode['hitstart'])
                )  # hit_start_tick(offline)
                hitfilehit.append(
                    int(hitdecode['hitend'] - hitdecode['hitstart'])
                )  # hit_size
                hitfilehit.append(
                    int(hdrdecode['timestamp'] + hitdecode['hitpeaktime'])
                )  # hitpeaktime(offline)
                hitfilehit.append(int(hitdecode['hitpeakadc']))  # hitpeakadc
                hitfilehit.append(int(hitdecode['hitsumadc']))  # hitsumadc
                hit = []  # clear for next loop iteration

                # append list to hits list of hits
                hits.append([int(x) for x in hitfilehit])

        # last frame was "last frame" type so this frame is hdr frame
        elif hdr_frame == 1:
            # do hdr frame work
            hdr_info.append(l)
            if tu == 1:
                hdr_frame = 0

    return hits


def writeTbFromPhil(file, data):
    """ line structure:"""
    """ #t_data #t_valid #t_user  #t_last #t_keep"""
    off = " 0"
    on = " 1"
    data = [data[i]['data'] for i in range(len(data))]
    data = [data[64 * i : 64 * (i + 1)] for i in range(int(len(data) / 64))]
    samples = []
    for t in range(len(data)):
        d = data[t]
        for p in range(len(d) * 4):
            samples.append(p)
            samples.append(0xABCD)
            samples.append(t * 64)
            for i in range(3):
                samples.append(0)
            for j in range(64):
                samples.append(d[j][p])

    packetNum = int(len(samples) / 70)
    t_v = on
    t_k = on
    for i in range(packetNum):
        packet = samples[70 * i : 70 * (i + 1)]
        for j in range(len(packet)):
            if j + 1 == 6 or j + 1 == 70:
                t_u = on
            else:
                t_u = off
            if j + 1 == 70:
                t_l = on
            else:
                t_l = off
            line = hex(packet[j])[2:].zfill(4)
            line += t_v
            line += t_u
            line += t_l
            line += t_k
            file.write(line + '\n')


def writeHitFile(file, hits):
    for (hit, ihit) in zip(hits, range(len(hits))):
        writeHit(file, hit)


def writeHit(file, hit):
    for (hq, i) in zip(hit, range(len(hit))):
        file.write(str(hq))
        if (i + 1) < 9:
            file.write(' ')
        elif i + 1 == 9:
            file.write('\n')


@click.command()
@click.option('--infile', '-i', help='Input file', type=click.File('r'))
@click.option(
    '--outfile', '-o', help='Output file name', type=click.File('w'), default='out.txt'
)
@click.option(
    '--intype',
    '-it',
    type=click.Choice(
        ['phil', 'inbuf', 'outbuf', 'tb_dtpc_unpacker_input', 'tb_dtpc_unpacker_output']
    ),
    default='phil',
)
@click.option(
    '--outtype',
    '-ot',
    type=click.Choice(
        [
            'inbuf',
            'txt',
            'phil',
            'hitfile',
            'tb_dtpc_unpacker_input',
            'tb_dtpc_unpacker_output',
        ]
    ),
    default='inbuf',
)
@click.option('--nchan', '-nc', type=click.INT, default=256)
@click.option('--fchan', '-fc', type=click.INT, default=1600)
@click.option('--nsamp', '-ns', type=click.INT, default=128)
def convert(infile, outfile, intype, outtype, nchan, fchan, nsamp):
    # can convert from:
    # philfile->subset of philfile
    # philfile->inbuf (same data output as subset of philfile)
    # philfile->txt " "
    # inbuf->subset of philfile
    # outbuf->hit file format
    # dtpc unpacker testbench format -> hit file format

    # inbuf/txt format is newline delimited hex/txt for the adc value in the first tick form all chans, then 2nd tick for all chans etc
    # subset of phil file is in PhilFile format with 'fake' channel numbers counting from 0 (just fewer channels and ticks)

    if intype == 'phil':
        # can create a subset of the PhilFile
        if outtype == 'phil':
            channels = readPhilFileToSubset(infile, nchan, fchan, nsamp)
            # print(channels)
        # or can convert to a packet format
        elif (
            outtype == 'inbuf'
            or outtype == 'txt'
            or outtype == 'tb_dtpc_unpacker_input'
        ):
            packets = readPhilFile(infile, nchan, fchan, nsamp)

    elif intype == 'inbuf':
        # only act if outtype is phil
        if outtype == 'phil':
            # convert inbuf to channels format
            channels = readInBuf(infile, nchan, nsamp, fchan)

    # outbuf includes files output into buffers by hit-finder
    elif intype == 'outbuf':
        # hits are not ordered by any means - only from top (first) of input file to bottom(last)
        hits = readInHitBufHardware(infile)

    elif intype == 'tb_dtpc_unpacker_output':
        hits = readInHitBufTestBench(infile)

    if outtype == 'inbuf':
        writeHexfile(formatInputBuffer(packets), outfile)
    elif outtype == 'txt':
        for packet in packets:
            outfile.write(str(packet) + '\n')
    elif outtype == 'phil':
        writePhilFile(outfile, channels, nchan, nsamp)
    elif outtype == 'hitfile':
        writeHitFile(outfile, hits)
    elif outtype == 'tb_dtpc_unpacker_input':
        writeTbFromPhil(outfile, packets)


if __name__ == '__main__':
    convert()
