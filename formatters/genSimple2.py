"""
Created on Tue Nov 5  2019

@author: David Cussans

Used to create simple test patterns for debugging/testing firmware in sim or hardware.
Will generate files in WIB , DPR or pipeline format.


"""
import random
import click

NPipelines = 4

NChansInWIBPacket = 256
NTicksInPipelinePacket = 64

# For testing can be useful to reduce the size of the packets.
#NChansInWIBPacket = 64
#NTicksInPipelinePacket = 8



"""
Package header information and payload into a dictionary that describes a WIB packet
pktArray contains crate,fibre,slot,version information together with and array of ADC samples
crate = 5 bits ( which crate is this WIB in? )
fibre = 3 bits ( which fibre output from WIB? )
slot = 3 bits ( which slot is this WIB in?)
version = 5 bits ( which format version is this data in? )
tstampBase = 64 bits ( 64 bit timestamp of the first WIB packet of the 64 that can be created from the data )
dataArray[channel][time-tick] = 12 bits = array of data for 64 time-tick x 256 channels
timeTick = which tick out of the 64 in dataArray ?
data[channel] = 12 bits = array of values for time tick
"""
def wibPacket(pktArray,  timeTick):
    assert len(pktArray['dataArray']) == NChansInWIBPacket ,  "len(dataArray) = %d"%len(pktArray['dataArray'])
    assert len(pktArray['dataArray'][0]) == NTicksInPipelinePacket , "len(dataArray[0]) = %d"%len(pktArray['dataArray'][0])
    assert timeTick < NTicksInPipelinePacket and timeTick > -1
    data = []
    for channel in range(len(pktArray['dataArray'])):
        data.append(pktArray['dataArray'][channel][timeTick])
    return {
        'crate': pktArray['crate'],      
        'fibre': pktArray['fibre'],
        'slot': pktArray['slot'],
        'version': pktArray['version'],
        'tstamp':  pktArray['tstamp'] + timeTick,
        'data': data
    }

"""
Package header information and payload into a dictionary that describes a collection of WIB packets written to DPR
crate = 5 bits ( which crate is this WIB in? )
fibre = 3 bits ( which fibre output from WIB? )
slot = 3 bits ( which slot is this WIB in?)
version = 5 bits ( which format version is this data in? )
tstampBase = 64 bits ( 64 bit timestamp of the first WIB packet of the 64 that can be created from the data )
dataArray[channel][time-tick] = 16 bits = array of data for 64 time-tick x 256 channels
"""
def packetArray(crate, fibre, slot, version, tstampBase, flags, dataArray ):
    assert len(dataArray) == NChansInWIBPacket ,  "len(dataArray) = %d"%len(dataArray)
    assert len(dataArray[0]) == NTicksInPipelinePacket , "len(dataArray[0]) = %d"%len(dataArray[0])
    return {
        'crate': crate,      
        'fibre': fibre,
        'slot': slot,
        'version': version,
        'tstamp': tstampBase ,
        'flags': flags,
        'dataArray': dataArray
    }

"""
Package header information and payload into a dictionary
crate = 5 bits ( which crate is this WIB in? )
fibre = 3 bits ( which fibre output from WIB? )
slot = 3 bits ( which slot is this WIB in?)
version = 5 bits ( which format version is this data in? )
tstampBase = 64 bits ( 64 bit timestamp of the first WIB packet of the 64  )
dataArray[channel][time-tick] = 16 bits = array of data for 64 time-tick x 256 channels
channelNumber = 0-255 = which of the channels are we looking at?
"""
def pipeLinePacket(pktArray , channelNumber ):
    assert len(dataArray) == NTicksInPipelinePacket
    assert len(dataArray[0]) == NChansInWIBPacket
    assert (channelNumber < NChansInWIBPacket ) and ( channelNumber > -1)
    data=[]

    # Extract the samples for a single channel 
    for idx in range(NTicksInPipelinePacket):
        data.append(pktArray['dataArray'][channelNumber][idx])
                    
    return {
        'crate': pktArray['crate'],      
        'fibre': pktArray['fibre'],
        'slot': pktArray['slot'],
        'version': pktArray['version'],
        'tstamp': pktArray['tstamp'],
        'data': data
    }

                    
"""Convert data into a hex number of defined bit length"""
def padhex32(d):
    return '0x' + "%08x"%(d) 

def padhex(d, bits , prefix):
    str = hex(d)[2:].zfill(int(bits / 4)).rstrip("L").upper()
    if prefix:
        str = '0x' + str 
    return str


                    
""" Take information needed for a WIB packet and return an array of formatted strings containing 32-bit words together with 1-bit flags
***** WARNING - produces incorrect data if more than the bottom 12-bits of ADC data are set"""
def formatWIBPacket(wibPacket,packetCtr):

    data=[]
    # WIB headers
    data.append({'value':0x3C, 'ctrlFlag':1})  # SOF

    # word = packetCtr
    word = 0x0
    word += (wibPacket['version'] & 0x1F) << 8
    word += (wibPacket['fibre'] & 0x07) << 13
    word += (wibPacket['crate'] & 0x1F) << 16
    word += (wibPacket['slot'] & 0x07) << 21
    # slot number not added 21
    # reserved 24
    # end 32
    data.append({'value':word, 'ctrlFlag':0})
    # blank line for MM + OOS + reserved (14) + WIB errors
    word = 0xDEADBEAD
    data.append({'value':word, 'ctrlFlag':0})
    # timestamp split into 2 lines
    timestamp = wibPacket['tstamp']
    p = wibPacket['data']

    word = timestamp & 0xFFFFFFFF
    data.append({'value':word, 'ctrlFlag':0})
    word = (timestamp>>32) & 0x7FFFFFFF
    z = 0 # I have no idea what Z is. Leave 0 for now.
    word += (z & 0x1)<< 31
    
    data.append({'value':word, 'ctrlFlag':0})


    # Each WIB packet contains the data from 4 front end motherboards
    NfrontEndMotherboards = 4
    NwordBlocks = 4

    for frontEndMotherboard in range(NfrontEndMotherboards):

        # COLDATA headers, checksums . One per FEMB- not filled yet
        data.append({'value':0xAAAAAAAA, 'ctrlFlag':0})
        data.append({'value':0xBBBBBBBB, 'ctrlFlag':0})
        data.append({'value':0xCCCCCCCC, 'ctrlFlag':0})
        data.append({'value':0xDDDDDDDD, 'ctrlFlag':0})

        # The data from each front end motherboard is packed as 4 blocks of 6 words.
        for wordBlock in range(NwordBlocks):

            i = frontEndMotherboard*(NChansInWIBPacket/NfrontEndMotherboards) + wordBlock*(NChansInWIBPacket/(NfrontEndMotherboards*NwordBlocks))

            print "femb , block , i , p[i] " , frontEndMotherboard, wordBlock,i,p[i]
            # 1st row
            A1C1 = p[i]
            A1C2 = p[i + 1]
            A2C1 = p[i + 8]
            A2C2 = p[i + 9]
            print "A1C1 , A1C2, A2C1, A2C2" , A1C1 , A1C2, A2C1, A2C2
            word = A1C1 & 0x00FF
            word += (A2C1 & 0x00FF) << 8
            word += ((A1C1 >>8) & 0x000F) << 16
            word += ( A1C2      & 0x000F) << 20
            word += ((A2C1 >>8) & 0x000F) << 24
            word += (A2C2 & 0x000F) << 28
            data.append({'value':word, 'ctrlFlag':0})

            # 2nd row
            A1C3 = p[i + 2]
            A2C3 = p[i + 10]
            word = ((A1C2>>4) & 0x00FF) 
            word +=((A2C2>>4) & 0x00FF) << 8
            word += (A1C3 & 0x00FF) << 16
            word += (A2C3 & 0x00FF) << 24
            data.append({'value':word, 'ctrlFlag':0})

            # 3rd row
            A1C4 = p[i + 3]
            A2C4 = p[i + 11]
            word = ((A1C3>>8) & 0x000F)
            word += (A1C4 & 0x000F) << 4
            word += ((A2C3>>8) & 0x000F)<<8
            word += (A2C4 & 0x000F) << 12
            word += ((A1C4>>4) & 0x00FF) << 16
            word += ((A2C4>>4) & 0x00FF) << 24
            data.append({'value':word, 'ctrlFlag':0})

            # 4th row
            A1C5 = p[i + 4]
            A1C6 = p[i + 5]
            A2C5 = p[i + 12]
            A2C6 = p[i + 13]
            word = A1C5 & 0x00FF
            word += (A2C5 & 0x00FF) << 8
            word += ((A1C5>>8) & 0x000F) << 16
            word += (A1C6 & 0x000F) << 20
            word += ((A2C5>>8) & 0x000F) << 24
            word += (A2C6 & 0x000F) << 28
            data.append({'value':word, 'ctrlFlag':0})

            # 5th row
            A1C7 = p[i + 6]
            A2C7 = p[i + 14]
            word = ((A1C6>>4) & 0x00FF)
            word +=((A2C6>>4) & 0x00FF) << 8
            word += (A1C7 & 0x00FF) << 16
            word += (A2C7 & 0x00FF) << 24
            data.append({'value':word, 'ctrlFlag':0})

            # 6th row
            A1C8 = p[i + 7]
            A2C8 = p[i + 15]
            word = ((A1C7>>8) & 0x000F) 
            word += (A1C8 & 0x000F) << 4
            word += ((A2C7>>8) & 0x000F) <<8 
            word += (A2C8 & 0x000F) << 12
            word += ((A1C8>>4) & 0x00FF) << 16
            word += ((A2C8>>4) & 0x00FF) << 24
            data.append({'value':word, 'ctrlFlag':0})
    # footer to be added
    EOF = 0xDC
    k28p5 = 0xBC
    word = EOF
    word += 0x0  # don't know what CRC-20 is
    data.append({'value':word, 'ctrlFlag':1})  
    word = k28p5
    data.append({'value':word, 'ctrlFlag':1})
    data.append({'value':word, 'ctrlFlag':1})  

    return data

def printWibPacket(filePtr,formattedWibPkt):
    numBits = 32
    for dataVal in formattedWibPkt:
        filePtr.write(padhex(dataVal['value'], numBits,True) + " %d\n"%dataVal['ctrlFlag'])

"""Return a block of 64 WIB packets"""
def formatWIBPacketBlock(pktArray,packetCtr):
    data=[]
    for timeTick in range(NTicksInPipelinePacket):
        wibPkt = wibPacket(pktArray,  timeTick)
        data = data + formatWIBPacket(wibPkt,packetCtr+timeTick)
    return data

def formatPipeLinePacket(packetArray):

    dataArray = packetArray['dataArray']
    assert len(dataArray) == NChansInWIBPacket ,  "len(dataArray) = %d"%len(dataArray)
    assert len(dataArray[0]) == NTicksInPipelinePacket , "len(dataArray[0]) = %d"%len(dataArray[0])

    # Construct header frame
    HdrChanNumberLocation = 0
    HdrFlagsLocation = 1
    HdrTimeStampLocation = 2
    flags = packetArray['flags'] # Need to add crate as MSB??
    pktHeaderLength = 6
    pktPayloadLength = NTicksInPipelinePacket


    pktTuser = ([0x0] * (pktHeaderLength-1)) + [0x1]
    pktTlast = [0x0] * pktHeaderLength

    data = {}
    data['tdata'] =  NPipelines*[[]]
    data['tuser0']  = NPipelines * [[]]
    data['tlast']  = NPipelines * [[]]
    data['tvalid']  = NPipelines * [[]]
    data['tkeep']  = NPipelines * [[]]


    for chanBase in range(0,NChansInWIBPacket,NPipelines): # loop over the number of samples per pipeline ( = 256/4 = 64 )

        for nPipe in range(NPipelines):

            channelIdx = chanBase + nPipe
            channel = ((packetArray['crate']& 0x1F) << 11) + ((packetArray['fibre']& 0x7) << 8) +  (channelIdx& 0xFF)  
            
            pktHeader = [0x0] * pktHeaderLength 
            pktHeader[HdrChanNumberLocation] = channel
            pktHeader[HdrFlagsLocation] = (flags & 0x1FFF) + ((packetArray['slot']&0x7) << 13) 
            pktHeader[HdrTimeStampLocation] = packetArray['tstamp'] & 0xFFFF
            pktHeader[HdrTimeStampLocation+1] = (packetArray['tstamp']>>16) & 0xFFFF
            pktHeader[HdrTimeStampLocation+2] = (packetArray['tstamp']>>32) & 0xFFFF
            # Bit 63 of timestamp is taken by "Z" flag in WIB data.
            z = 0
            pktHeader[HdrTimeStampLocation+3] = ( (packetArray['tstamp']>>48) & 0x7FFF) + (( z & 0x1)<<15) 
    
            data['tdata'][nPipe] = data['tdata'][nPipe] + pktHeader
            data['tuser0'][nPipe] = data['tuser0'][nPipe] + pktTuser
            data['tlast'][nPipe] = data['tlast'][nPipe] + pktTlast



            for timeTick in range(NTicksInPipelinePacket): # Loop over the 64 time ticks in a array of packets
                data['tdata'][nPipe].append(packetArray['dataArray'][channelIdx][timeTick])
            
            data['tuser0'][nPipe] = data['tuser0'][nPipe] + ([0x0] * (pktPayloadLength-1)) + [0x1]
            data['tlast'][nPipe] =  data['tlast'][nPipe] +  ([0x0] * (pktPayloadLength-1)) + [0x1]
            # Set TVALID, TKEEP permanently high
            data['tvalid'][nPipe] = data['tvalid'][nPipe] + ([0x1] * ( pktHeaderLength + pktPayloadLength ))
            data['tkeep'][nPipe] =  data['tvalid'][nPipe]


    return data

      
"""Return an array of 64-bit data to be written to DPR
ipacketBase = start value of packet counter
"""
def formatDPRPacket(packetArray,ipacketBase):
    data = []
    dataArray = packetArray['dataArray']
    assert len(dataArray) == NChansInWIBPacket ,  "len(dataArray) = %d"%len(dataArray)
    assert len(dataArray[0]) == NTicksInPipelinePacket , "len(dataArray[0]) = %d"%len(dataArray[0])

    for timeTick in range(NTicksInPipelinePacket): # Loop over the 64 time ticks in a array of packets

        # WIB headers
        word = (ipacketBase + timeTick) & 0x00003FFFFFFFFFFF
        word += (packetArray['version'] & 0x1F) << 46
        word += (packetArray['fibre'] & 0x7) << 51
        word += (packetArray['crate'] & 0x3F) << 54
        word += (packetArray['slot'] & 0x7) << 59
        data.append(word)

        # timestamp
        # Only 63 bits passed from WIB... ( plus mysterious Z flag . Which we ignore for now )
        data.append( (packetArray['tstamp'] + timeTick) & 0x7FFFFFFFFFFFFFFF  )

        # stream errors - not filled yet and flags
        word = 0x0 # put stream1,2 err here
        word += (packetArray['flags'] & 0xFFFF) <<48
        data.append(word)

        # COLDATA headers, checksums - not filled yet
        for i in range(8):
            data.append(0x0)


        for channel in range(0,NChansInWIBPacket,NPipelines): # loop over the number of samples per pipeline ( = 256/4 = 64 )
       
            word = ( packetArray['dataArray'][channel  ][timeTick]  & 0xFFFF )
            word +=((packetArray['dataArray'][channel+1][timeTick]) & 0xFFFF ) << 16
            word +=((packetArray['dataArray'][channel+2][timeTick]) & 0xFFFF ) << 32
            word +=((packetArray['dataArray'][channel+3][timeTick]) & 0xFFFF ) << 48
            data.append(word)
            
    return data

""" Print packet in DPR ( "JimFile" format)  to file"""
def writeDprPacket(filePtr,formattedDprPkt,lineCtr):
    numBits = 64
    for dataVal in formattedDprPkt:
        filePtr.write(padhex(dataVal, numBits,False)+" %d\n"%lineCtr )
        lineCtr += 1
    return lineCtr
        
""" Print the four pipelines to file"""
def writePipeLinePacket(filePtrArray, formattedPipeLinePacket, lineCtr):
    assert len(formattedPipeLinePacket['tdata'][0]) == len(formattedPipeLinePacket['tuser0'][0]) == len(formattedPipeLinePacket['tlast'][0]) == len(formattedPipeLinePacket['tvalid'][0])
    for pipe in range(NPipelines):
        print "Pipeline = " , pipe
        for tick in range(len(formattedPipeLinePacket['tdata'][pipe])):
            filePtrArray[pipe].write(padhex(formattedPipeLinePacket['tdata'][pipe][tick],16,False) + " %d %d %d %d\n"%(formattedPipeLinePacket['tuser0'][pipe][tick], formattedPipeLinePacket['tlast'][pipe][tick], formattedPipeLinePacket['tvalid'][pipe][tick],lineCtr+tick))

    # Calculate how many lines we have written to each file and return 
    lineCtr += len(formattedPipeLinePacket['tdata'][pipe])
    return lineCtr
    
""" Generate an array of words depending on pattern parameter 
timeStart = time-tick of start of dataArray
pattern = ignore for now
maxbits = truncate values to this number of bits.
"""
def generateDataArray(pattern,timeStart,maxbits):
    dataArray=[]
    mask = 2**maxbits -1
    for channel in range(NChansInWIBPacket):
        timeData = []
        for timeTick in range(NTicksInPipelinePacket):
            # val = (channel*256) + ((timeTick + timeStart)&0xFF)
            val = (channel) + (((timeTick + timeStart)&0xFF)*256)
            timeData.append( val & mask )
        dataArray.append(timeData)
    return dataArray

def debugPrintDataArray(dataAry):
    for channel in range(NChansInWIBPacket):
        for timeTick in range(NTicksInPipelinePacket):
            print "channel , time-tick , data= ", channel,timeTick,dataAry[channel][timeTick],hex(dataAry[channel][timeTick])

######################################################################


@click.command()
# @click.option('--formattype', '-ft', type=click.Choice(['dpr', 'wib']), default='dpr')
@click.option('--outfile', '-o', help='Base of filenames. WIB file = wib_%s , DPR file = dpr_%s , pipeline files = pipeline0_%s , pipeline1_%s , etc.', default='out.txt')
@click.option('--pattern', '-p', type=click.Choice(['channel', 'sample', 'cs', 'adcc', 'random', 'special']), help ='Type of pattern generated (at the moment only channel implemented)' , default='channel')
@click.option('--starttime', '-s', help='Timestamp at start of data. 64 bits', default=0xFEDCBA9876543210,type=int)
@click.option('--cycles', '-n', help='Number of cycles of 64 time-ticks', default=1,type=int)
@click.option('--truncate/--no-truncate', help='Truncate the last 64 packet cycle of WIB packets from the AXI4S pipeline output. Do this to match Questasim output', default=False)


def  run(outfile, pattern,starttime,cycles,truncate):

    """Used to create simple test patterns for debugging/testing firmware in sim or hardware. Generate files in WIB , DPR and AXI4S pipeline format."""


    print "Outfile = " , outfile
#    print "Format type = " , formattype
    print "Pattern = ", pattern
    print "Start time = ", hex(starttime)
    print "Number of cycles of 64 time-ticks = ", cycles
    print "Truncate AXI4S pipeline file? : " , truncate
    # Open the files for writing.
    fnameDpr = "dpr_" + outfile
    print "opening %s for writing DPR data\n"%fnameDpr
    fptrDPR = open(fnameDpr, "w+")

    fnameWib = "wib_" + outfile
    print "opening %s for writing WIB data\n"%fnameWib
    fptrWIB = open(fnameWib, "w+")

    fptrArrayPipe=[]
    for pipe in range(NPipelines):
        fname = "pipeline%d_"%pipe + outfile
        print "opening %s for writing pipeline %d data\n"%(fname,pipe)
        fptrArrayPipe.append(open(fname,"w+"))
        

    # Arbitary numbers....
    crate = 3
    fibre = 2
    slot = 4
    version =12

    # Flags is a field in DPR that is *not* in the WIB data
    # Used to indicate e.g. configuration
    # Set to zero if you want WIB --> AXI4S comparison between f/ware and scripts
    #flags = 0xC8C8
    flags = 0x0000

    packetCtr = 0xA5A5A0
    
    timeStart = 0
    dprLineCtr =0
    pipeLineCtr = 0

    maxbits = 12 # fill whole 16 bit word. NB must be 12 bits to fit in WIB format
    
    for ngroup in range(cycles):
        
        # Generate data values
        dataAry = generateDataArray(pattern,timeStart,maxbits)
        # debugPrintDataArray(dataAry)

        # Add crate, fibre etc information
        pktArray = packetArray(crate, fibre, slot, version,starttime, flags, dataAry )

        # Uncomment to get WIB packets....
        wibTimeTick = 0
        wibPacketCtr = 0
        # wibPkt = wibPacket(pktArray , wibTimeTick )
        # print wibPkt
        #formattedWibPkt = formatWIBPacket(wibPkt,wibPacketCtr)
        formattedWibPktBlk = formatWIBPacketBlock(pktArray,wibTimeTick)
        # print formattedWibPkt
        printWibPacket(fptrWIB,formattedWibPktBlk)
        
        # Package as words to be written to DPR
        formattedDprPkt = formatDPRPacket(pktArray,packetCtr)
        # And write to file
        dprLineCtr = writeDprPacket(fptrDPR,formattedDprPkt,dprLineCtr)
        #print formattedDprPkt

        # If the "Truncate" boolean flag is set then don't print out the last cycle of 64 packets to the
        # AXI4S pipeline files.
        if (not truncate) or (ngroup != (cycles-1)):
            # Package as words to be written to processing pipelines
            formattedPipelinePkt = formatPipeLinePacket(pktArray)
            # print formattedPipelinePkt
            # and write to file
            pipeLineCtr = writePipeLinePacket(fptrArrayPipe, formattedPipelinePkt, pipeLineCtr)

        timeStart += NTicksInPipelinePacket
        starttime += NTicksInPipelinePacket
        
    # Close the files at the end 
    fptrDPR.close()
    fptrWIB.close()
    for filePtr in fptrArrayPipe:
        filePtr.close()
    
        
if __name__ == '__main__':
        run()
