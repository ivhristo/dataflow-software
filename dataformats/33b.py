# -----------------------------------------------------------------------------
class Bus33bData(object):
    """This is just a wrapper around a list of 33-bit words"""

    def __init__(self):
        super(Bus33bData, self).__init__()
        self.data = []

    def __str__(self):
        return "\n".join(
            [
                "0x%1x 0x%08x" % ((int(d) >> 32) & (0x1), int(d) & 0xFFFFFFFF)
                for d in self.data
            ]
        )

    def isValid(self):
        """Return true if this is a valid packet (to be implemented)"""
        return True

    def getData(self):
        """Strip the k-char flag"""
        return [(int(d) & 0xFFFFFFFF) for d in self.data]
