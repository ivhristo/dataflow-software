# placeholder for unpacker formatter

import wib2g
import channel_map
import axi4s


class UnpackerFormatter(object):
    """Converts Nx64 WIB packets to N AXI4S stream packets
    """

    def __init__(self):
        super(UnpackerFormatter, self).__init__()
        self.cm = channel_map.ChannelMap()

    def _formatChannel(self, wiblist, iWireIndex):
        """Read 64 WIB packets and produce one AXI4S packet"""

        if not (len(wiblist) >= channel_map.N_TICKS_PER_TP_PACKET):
            raise Exception(
                "Require exactly {} WIB packets per TPG stream packet".format(
                    channel_map.N_TICKS_PER_TP_PACKET
                )
            )

        if iWireIndex >= channel_map.N_CHAN_PER_WIB:
            raise Exception(
                "Require exactly {} WIB packets per TPG stream packet".format(channel_map.N_TICKS_PER_TP_PACKET)
            )

        if iWireIndex >= channel_map.N_CHAN_PER_WIB:
            raise Exception(
                "Channel must be less than {}".format(channel_map.N_CHAN_PER_WIB)
        )

        packet = axi4s.Axi4sPacket()

        # here I assume the WIB list is sorted !
        packet.header.crate = wiblist[0].header.crate
        packet.header.slot = wiblist[0].header.slot
        packet.header.fibre = wiblist[0].header.fiber
        packet.header.channel = iWireIndex
        packet.header.wireIndex = 0
        packet.header.pipeline = iWireIndex % 4
        packet.header.flags = 0
        packet.header.timestamp = wiblist[0].header.timestamp

        # get the coordinates of this channel
        iCol = self.cm.tpColdata(iWireIndex)
        iADC = self.cm.tpADC(iWireIndex)
        iADCChan = self.cm.tpADCChan(iWireIndex)

        # now get the data from each packet in the list
        for wibpacket in wiblist:
            d = wibpacket.coldata[iCol].data.adcs[iADC][iADCChan]
            packet.payload.data.append(d)

        return packet

    def formatAllChannels(self, wiblist):
        """Read 64 WIB packets and produce 64 AXI4S packets"""

        packets = []

        for iWireChan in range(channel_map.N_CHAN_PER_WIB):
            packets.append(self._formatChannel(wiblist, iWireChan))

        return packets

    def _formatTick(self, axi4slist, itick):
        """Read 64 AXI4S packets and produce a single WIB packet (1 tick)"""

        if not (len(axi4slist) == channel_map.N_CHAN_PER_WIB_PACKET):
            raise Exception(
                "Need exactly {} AXI4S packets to convert to a WIB packet".format(
                    channel_map.N_CHAN_PER_WIB_PACKET
                )
            )

        packet = wib2g.WibData()
        packet.header.crate = axi4slist[0].header.crate
        packet.header.slot = axi4slist[0].header.slot
        packet.header.fiber = axi4slist[0].header.fiber

        for i in range(channel_map.N_CHAN_PER_WIB_PACKET):
            icol = self.cm.tpColdata(i)
            iadc = self.cm.tpASIC(i)
            ichan = self.cm.tpASICChan(i)
            packet.coldata[icol].adc[iadc][ichan] = axi4slist[i].data[itick]
            # need to get the right data from axi4s packet

        return packet

    def formatAllTicks(self, axi4slist):
        """Read 64 AXI4S packets and produce 64 WIB packets"""

        if len(axi4slist) < channel_map.N_CHAN_PER_WIB:
            raise Exception(
                "N AXI4S packets must be greater than {}".format(
                    channel_map.N_CHAN_PER_WIB
                )
            )

        packets = []

        for itick in range(channel_map.N_TICKS_PER_TP_PACKET):
            packets.append(self._formatTick(axi4slist, itick))

        return packets
