""" Placeholder for AXI4 Stream data """

# -----------------------------------------------------------------------------
class Axi4sPacket(object):
    """docstring for Axi4sData"""

    def __init__(self):
        super(Axi4sPacket, self).__init__()
        self.header = Axi4sHeader()
        self.payload = Axi4sPayload()
        self.flags = Axi4sFlags()

    def __str__(self):
        return "\n".join(
            ["header  : " + self.header.__str__()]
            + ["payload : " + self.payload.__str__()]
            + ["flags   : " + self.flags.__str__()]
        )


# -----------------------------------------------------------------------------
class Axi4sHeader(object):
    """docstring for Axi4sData"""

    def __init__(self):
        super(Axi4sHeader, self).__init__()
        self.channel = 0
        self.slot = 0
        self.crate = 0
        self.fibre = 0
        self.wireIndex = 0
        self.pipeline = 0
        self.flags = 0
        self.tstamp = 0

    def __str__(self):
        return str(vars(self))


# -----------------------------------------------------------------------------
class Axi4sPayload(object):
    """docstring for Axi4sData"""

    def __init__(self):
        super(Axi4sPayload, self).__init__()
        self.data = []  # [[0] * 64]

    def __str__(self):
        return str(vars(self))


# -----------------------------------------------------------------------------
class Axi4sFlags(object):
    """docstring for Axi4sData"""

    def __init__(self):
        super(Axi4sFlags, self).__init__()
        self.tvalid = (
            []
        )  # [0] * (64 + 1) * 6 # 64 ADC values in 6 words + 6 words for header information
        self.tuser = []  # [0] * (64 + 1) * 6
        self.tkeep = []  # [0] * (64 + 1) * 6
        self.tlast = []  # [0] * (64 + 1) * 6
        self.tready = []  # [0] * (64 + 1) * 6

    def __str__(self):
        return str(vars(self))
