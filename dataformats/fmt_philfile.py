import sys
import numpy as np
import random
import click
import channel_map
import wib2g

# -----------------------------------------------------------------------------
class PhilFileFormatter(object):
    """ Phil file formatter for wib2g class

    WIB packet contains ADC values for 256 channels x 1 clock tick
    arranged as 4 COLDATA chips x 8 ADCs x 8 channels
    plus some headers

    Refer to the document here for file type descriptions: https://docs.google.com/document/d/1rirDwFS_4Anp3v1A5W8cVngqi5dKGZzyoiDyt-lp2Pk/edit

    """

    def __init__(self):
        super(PhilFileFormatter, self).__init__()
        self.cm = channel_map.ChannelMap()

    def _getNumPhilPackets(self, philData, n_channels=None, n_packets=None):

        numChannels = int(len(philData))
        numADCsInFile = int(len(philData[0][3:]))
        numADCs = numADCsInFile
        numPackets = numADCsInFile // 64

        if n_packets:
            numPackets = n_packets
            numADCs = numPackets * 64
            if numADCsInFile < numADCs:
                numADCs = (numADCsInFile // 64) * 64

        if n_channels:
            numChannels = n_channels
            if numChansInFile < numChannels:
                numChannels = numChansInFile

        numWibPackets = int(
            numADCs * np.ceil(float(numChannels) / channel_map.N_CHAN_PER_WIB)
        )
        return numWibPackets, numADCs, numChannels

    def _formatPhilToWib(self, data, n_packets=None, ts=0, n_channels=None):

        if isinstance(data, list) == False:
            data = self._readPhilFile(data)  # , fchan, nchan)

        # Each wib in a fibre contains 256 channels, the remaining can be discarded as they belong to a separate fibre which we are currently NOT dealing with.
        # Ignore last 12 adcs if we have 4492 ticks (maybe relevant to something in Larsoft but 4492 does not divide by 64 so does not make sense, max number of packets is 4480/64 = 70)

        numPackets, numTicks, numChannels = self._getNumPhilPackets(
            data, n_channels, n_packets
        )

        # ROW NUMBER AS CHANNEL APPROACH, DECOUPLED FROM "PROPER" OFFLINE CHANNEL MAPPING
        wibPackets = [wib2g.WibData() for _ in range(numPackets)]
        crate = 0
        fibre = 0
        slot = 0

        for irow, row in enumerate(data):
            hfChan = irow
            iADC = self.cm.tpADCFromHFChannel(hfChan)
            iADCChan = self.cm.tpADCChannelFromHFChannel(hfChan)
            iColdata = self.cm.tpColdataFromHFChannel(hfChan)
            pipeline = hfChan % channel_map.N_CHAN_PER_WIB

            # Get crate, slot, fibre from philfile channel
            philChan    = row[1]
            crate       = self.cm.crateFromHfChannel(philChan)
            slot        = self.cm.slotFromHfChannel(philChan)
            fibre       = self.cm.fibreFromHfChannel(philChan)
            wireIndex   = self.cm.wireIndexFromHfChannel(philChan)

            # This is for arranging the wibpackets in the correct order 
            # inside the wiblist for subsequent conversions to work correctly.
            rowcrate = 0
            rowslot = 0 
            rowfibre = 0
            if irow > 0:
                if irow % channel_map.N_CHAN_PER_WIB == 0:
                    rowfibre += 1

                if irow % (4 * channel_map.N_CHAN_PER_WIB) == 0:
                    rowfibre = 0
                    rowslot += 1

                if irow % (16 * channel_map.N_CHAN_PER_WIB) == 0:
                    rowfibre = 0
                    rowslot = 0
                    rowcrate += 1

            packetOrder = numTicks * (rowfibre + (4 * rowslot) + (16 * rowcrate))

            for iPacket, adc in enumerate(row[3:]):

                Packet = wibPackets[iPacket + packetOrder]
                # print (iPacket+packetOrder), fibre
                Packet.header.crate = crate
                Packet.header.slot = slot
                Packet.header.fiber = fibre
                # Packet.header.wireIndex = wireIndex
                Packet.header.timestamp = ts + iPacket

                Packet.coldata[iColdata].data.adcs[iADC][iADCChan] = adc

        # for packet in wibPackets:
        #     print packet
        return wibPackets

    # CHANNEL LOOK-UP APPROACH
    """
        wibPackets = []
        existingHeads = []
        numDiffHead = 0
        for iline, line in enumerate(data):

            chan        = int(line[1])
            crate       = self.cm.crate(chan)
            slot        = self.cm.fiber(chan)
            fiber       = self.cm.slot(chan)
            head        = [crate,slot,fiber]

            iColdata    = self.cm.tpColdata(chan)
            iADC        = self.cm.tpADC(chan)
            iADCChan    = self.cm.tpADCChan(chan)
            iColdata2    = self.cm.chipChan(chan)
            iADC2        = self.cm.asic(chan)
            iADCChan2    = self.cm.asicChan(chan)

            if head not in existingHeads:
                existingHeads.append(head)
                numDiffHead += 1

                for itick in xrange(numADCs):

                    wibPacket = wib2g.WibData()
                    wibPacket.header.crate = crate
                    wibPacket.header.slot = slot
                    wibPacket.header.fiber = fiber

                    wibPacket.coldata[iColdata].data.adcs[iADC][iADCChan] = line[itick+3]
                    wibPackets.append(wibPacket)

            else:

                for itick in xrange(numADCs):
                    wibPackets[((numDiffHead-1)*numADCs)+itick].coldata[iColdata].data.adcs[iADC][iADCChan] = line[itick+3]

        return wibPackets
    """

    def _getNumWibChannels(self, wiblist):

        numPackets = len(wiblist)
        headers = [
            [wib.header.crate, wib.header.slot, wib.header.fiber] for wib in wiblist
        ]
        uniqHeaders = np.unique(headers, axis=0).tolist()
        # print len([h for h in headers if h == [0,0,0]])
        numChannels = len(uniqHeaders) * channel_map.N_CHAN_PER_WIB

        tickNums = []

        for head in uniqHeaders:
            tickNum = len([h for h in headers if h == head])
            tickNums.append(tickNum)

        checkTicks = np.unique(tickNums).tolist()

        numTicks = tickNums[0]

        if checkTicks[0] == 0:
            if numPackets == 0:
                raise Exception("Warning! No wib packets for formatting, terminating.")
            else:
                raise Exception(
                    "Warning! Different number of ticks for different wib header configurations! This should not happen."
                )

        return numChannels, numTicks

    def _formatWibToPhil(self, wiblist):

        numChannels, numTicks = self._getNumWibChannels(wiblist)

        event = 1
        numPacket = 0
        numFlags = 3
        philData = [[0 for _ in range(numTicks + numFlags)] for _ in range(numChannels)]
        numHeader = 0
        ts = min([wib.header.timestamp for wib in wiblist])

        for iWib, Wib in enumerate(wiblist):

            crate = Wib.header.crate
            slot = Wib.header.slot
            fibre = Wib.header.fiber
            pos1 = iWib % numTicks

            if iWib > 0 and pos1 == 0:
                numHeader += 1

            for iColdata, Coldata in enumerate(Wib.coldata):
                for iADC, adc in enumerate(Coldata.data.adcs):
                    for iADCChan, adcChan in enumerate(adc):

                        isCol = 1  # How do we determine that without proper channel mapping? only collection wires for now?
                        philPos = iADCChan + 8 * iADC + 64 * iColdata + 3
                        channel = self.cm.hfChannel(iColdata, iADC, iADCChan) # Channel number based on wib packet position. THIS SHOULD BE USED WHEN EVERYTHING IS CORRECT
                        hfChannel = self.cm.hfChannelHitfile (crate,slot,fibre,numHeader % 256) # Set channel number based on crate, slot, fibre
                        # print channel, numTicks, pos1+numFlags
                        chan = channel + (numHeader * 256) # The current setup of channel mapping resets after 256 rows, so subsequent rows need to be started after that.
                        philData[chan][0] = event
                        philData[chan][1] = hfChannel
                        philData[chan][2] = isCol
                        philData[chan][pos1 + numFlags] = adcChan

        # Each wib in a fibre contains 256 channels, the remaining can be discarded as they belong to a separate fibre which we are currently NOT dealing with.
        # numChannels = n_channels
        # Ignore last 12 adcs if we have 4492 ticks (maybe relevant to something in Larsoft but 4492 does not divide by 64 so does not make sense, max number of packets is 4480/64 = 70)

        return philData

    def _readPhilFile(self, infile):

        philData = []
        for line in infile:
            philLine = []
            chars = line.split()

            for c in chars:
                philLine.append(int(c))

            philData.append(philLine)

        return philData

    def _writePhilFile(self, outfile, philData):

        for arr in philData:
            for ival, val in enumerate(arr):
                outfile.write(str(int(val)))
                if ival < (len(arr) - 1):
                    outfile.write(" ")

            outfile.write("\n")

    def _dumpPhilFile(self, data):

        for line in data:
            event = int(line[0])
            channel = int(line[1])
            collection = int(line[2])
            adcs = [int(x) for x in line[3:]]
            print("event: {}".format(event))
            print("channel: {}".format(channel))
            print("collection?: {}".format(collection))
            print("adcs:" + " ".join([" " + str(adc) for adc in adcs]))
