import random
import wib2g

# single channel with random counts
def wibPacketRandom():
    data = wib2g.WibData()
    data.header.timestamp = random.getrandbits(64)
    data.coldata[random.randint(0, 3)].data.adcs[random.randint(0, 7)][
        random.randint(0, 7)
    ] = random.getrandbits(12)
    return data


def wibListRandom(npackets):
    return [wibPacketRandom() for _ in xrange(npackets)]


# ADC is channel
def wibPacketTest1(t):
    data = wib2g.WibData()
    data.header.timestamp = t
    for c in range(4):
        for i in range(8):
            for j in range(8):
                data.coldata[c].data.adcs[i][j] = (
                    (t & 0x1F << 8) + (c << 6) + (i << 3) + j
                )
    return data


def wibListTest1(npackets):
    return [wibPacketTest1(t) for t in xrange(npackets)]


# dictinoary of pattern generation methods
patterns = {"rnd": wibListRandom, "test1": wibListTest1}

# master method to produce patterns
def wibPattern(type, npackets):
    return patterns[type](npackets)
