# threshold definition, gaussian of the hits must have a peak above this value
# decided to institute 20 hits on each channel
# hits can occur at he same time and overlap one another

import numpy as np
import click
import sys


def ListGenerator(
    patterntype, chan, fchan, nhits, packetlength, standardpeakheight, nticks
):
    """
    Structure of list:
    p = [alterPeak_ADC, peakCentre, enableHits]
    definitions: 
        alterPeak_ADC: how much to increase/decrease the amplitude of the hit from the standardpeakheight
        peakCentre: tick of centre of hit - is combined with standardtickdelay in hit generation
    Important to only append to hitList when the channel condition is met - otherwise we may get
    a bunch of useless default hits
    """
    hitList = []
    for hit in range(nhits):
        p = [0, 0, 0]

        if patterntype == 1:  # standard packet structure -> 1 hit on each wire
            p[0] = 0
            p[1] = 0
            p[2] = 1
            hitList.append(p)

        elif patterntype == 2:
            # hits in 1st packet for half the wires and hits in 2nd for second half of wires with
            # Total 128 wires with signal
            if (chan - fchan) <= 63:
                p[0] = 0
                p[2] = 1
            if (chan - fchan) > 63 and (chan - fchan) <= 127:
                p[
                    0
                ] = 5  # increased gaussian peak height compared to first set of 64 wires
                p[1] = packetlength + 1  # displaced by +1 tick from first hit
                p[2] = 1
            hitList.append(p)

        elif patterntype == 3:
            # hits with amplitude increasing with wire number past wire number fchan
            # all channels have hit with same peak tick
            p[0] = chan - fchan
            p[1] = 0
            p[2] = 1
            if p[0] < 0:
                p[0] = 0
                p[2] = 0
            hitList.append(p)

        elif (
            patterntype == 4
        ):  # only 2 hits. one in the first wire of the first set of 64 wires and the second on the second wire of the seoond set of 64 wires
            # essentially same as option 2 but for 1 hit per 64 wires instead of 1 on each
            if (chan - fchan) == 0:
                p[1] = 0
                p[2] = 1
            if (chan - fchan) == 64:  # 64 wires processed down a channel at once
                p[0] = 5  # second wire hit with gaussian peak 5 higher
                p[1] = packetlength
                p[2] = 1
            if (chan - fchan) not in [0, 64]:
                p[0] = 0
                p[2] = 0
            hitList.append(p)

        elif patterntype == 5:  # nhits random hits on a single channel
            if (chan - fchan) == 0:
                p[0] = np.random.uniform(
                    low=0, high=standardpeakheight
                )  # vary the peak height by the default peak height
                p[2] = 1
                if p[0] < 0:
                    p[0] = 0
                p[1] = np.random.uniform(
                    low=0, high=nticks
                )  # put the centre of the hit at a random tick along full time wire is generated for
            hitList.append(p)

    return hitList


def make_noise(noisemean, noisesig, noisesamples):
    # create a ndarray of noise samples to draw from
    noise_float = np.random.normal(noisemean, noisesig, noisesamples)
    # discretize them
    noise = noise_float.astype(dtype=int)
    # print(noise[:5])
    return noise


def gaussian(x, amp, mu, sig):
    # creates gaussian pattern for array x
    return amp * np.exp(-np.power(x - mu, 2.0) / (2 * np.power(sig, 2.0)))


def PacketGenerator(
    gaussianoption, nticks, packetNum, peak, centre, width, fixedlength
):

    if gaussianoption == 1:  # create gaussian hits
        packet = np.linspace(0, nticks - 1, nticks)  # generates tick values
        signal = gaussian(
            packet, peak, centre + 64 * packetNum, width
        )  # generates gaussian signal

    elif gaussianoption == 0:  # create square hits
        packet = np.zeros(nticks)
        hwidth = int(fixedlength / 2)  # half width of square wave
        packet[centre - hwidth : centre + hwidth] = peak
        signal = packet

    return signal


def HitGenerator(gaussianoption, nticks, peak, centre, allowHits, width, fixedlength):

    if allowHits == 1:
        if gaussianoption == 1:
            packet = np.linspace(0, nticks - 1, nticks)
            signal = gaussian(packet, peak, centre, width)

        elif gaussianoption == 0:
            packet = np.zeros(nticks)
            hwidth = int(fixedlength / 2)
            packet[centre - hwidth : centre + hwidth] = peak
            signal = packet
        return signal

    else:
        signal = np.zeros(nticks)
        return signal


def fakePhilFile(
    hittype,
    nhits,
    standardpeakheight,
    napas,
    nticks,
    ncolchans,
    nindchans,
    colped,
    noiseon,
    noisemean,
    noisesig,
    noisesamples,
    nevts,
    standardtickdelay,
    fixedlength,
    gaussianoption,
    gausswidth,
    fchan,
    patterntype,
    packetlength,
):

    noise = make_noise(
        noisemean, noisesig, noisesamples
    )  # create noise to add to signal
    # print(noise[:5])
    # induction/collection 0/1
    ind_col = 1

    # nAPA channels in offline sw format
    num_APA_chans = (nindchans + ncolchans) * napas

    # first create a vector of collection samples (all at pedestal)
    collection_base = np.full(nticks, colped, dtype=int)

    # create our output Phil file
    # file = open(filename, "w")
    data = []

    # now loop over evt
    for evt in range(nevts):
        # now loop over channels - continue if an induction channel
        for chan in range(num_APA_chans):

            if chan % num_APA_chans < nindchans:  # if collection do stuff
                continue
            # check nhits is compatible with patternType
            if patterntype in [1, 2, 3, 4]:
                assert nhits == 1, "Expecting nhits == 1 for patterntypes 1-4. Exiting."
            # add the hits to the data one by one via a list of hits
            # hitList has the following structure: [nhits][alterPeak_ADC, peakCentre]
            hitList = ListGenerator(
                patterntype,
                chan,
                fchan,
                nhits,
                packetlength,
                standardpeakheight,
                nticks,
            )  # determines peak height and peak centre for each hit when using a given patterntype
            collection = collection_base
            # loop through the hits you are going to add in
            for hitIter in hitList:
                # create the hit using HitGenerator.
                # takes in initial click value options plus hitList for the hit (peak) properties
                hit = HitGenerator(
                    gaussianoption,
                    nticks,
                    standardpeakheight + hitIter[0],
                    standardtickdelay + hitIter[1],
                    hitIter[2],
                    gausswidth,
                    fixedlength,
                )
                collection = collection + hit  # add hit to collection signal
            # add the noise to the data
            # print('noiseon={}'.format(noiseon))
            if noiseon == 1:

                for i in range(nticks):
                    collection[i] = (
                        collection[i] + noise[np.random.randint(0, noisesamples)]
                    )
            # only collection wires can get this far
            ind_col = 1
            # add the header to the front of the collection channel
            header = np.array([evt + 1, chan, ind_col], dtype=int)
            collection = np.insert(collection, 0, header)
            data.append(collection)

    print("Finished")
    return data


"""
c = 32  # hit center
w = 5  # gaussian width
t = 50  # exceed thresh
b = 500  # pedestal
p = t + b  # peak value
g = 1  # are hits gaussian?
hittype = 'S'
if g == 1:
    hittype = 'G'

#name = "Phil" + hittype + "_C" + str(c) + "_W" + str(w) + "_P" + str(p) + ".txt"
name = 'test.txt'
file = open(name, "w")
fakePhilFile(file, 0, 1, 50, t, 1, 40, 1, 4492, 960, 1600, b, 0, 0, 2.5, 1000000, 1, 0, c, 20, -1, g, w)
file.close()
"""
