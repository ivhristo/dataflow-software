import hit
import axi4s
import cabling

# -----------------------------------------------------------------------------
class Axi4sHitFormatter(object):
    """ 
    Formatter for going between Axi4S and Hit objects
    """

    global Masks
    Masks = {
        "hit_continuesmask": int("1000000000000000", 2),
        "hitflagsmask": int("0111111111111111", 2),
    }

    def __init__(self):
        super(Axi4sHitFormatter, self).__init__()

    def _unpackHits(self, hitframe):

        hitstart = hitframe[0]
        hitend = hitframe[1]
        hitpeakadc = hitframe[2]
        hitpeaktime = hitframe[3]
        hitsumadc = hitframe[4]
        hit_continues = (Masks["hit_continuesmask"] & hitframe[5]) >> 15
        hitflags = Masks["hitflagsmask"] & hitframe[5]
        hitword5 = hitframe[5]

        hitInfo = {
            "hitstart": hitstart,
            "hitend": hitend,
            "hitpeakadc": hitpeakadc,
            "hitpeaktime": hitpeaktime,
            "hitsumadc": hitsumadc,
            "hit_continues": hit_continues,
            "hitflags": hitflags,
            "hitword5": hitword5,
        }
        return hitInfo

    def _formatAxi4sToHit(self, axi4sPackets):

        hitPackets = []
        for packet in axi4sPackets:
            hitPacket = hit.HitPacket()

            hitPacket.hitHeader.slot = packet.header.slot
            hitPacket.hitHeader.crate = packet.header.crate
            hitPacket.hitHeader.fibre = packet.header.fibre
            hitPacket.hitHeader.wireIndex = packet.header.wireIndex
            hitPacket.hitHeader.pipeline = packet.header.pipeline
            hitPacket.hitHeader.flags = packet.header.flags
            hitPacket.hitHeader.tstamp = packet.header.tstamp

            wordNum = 0
            hitFrame = hit.HitFrame()
            for bits in packet.payload.data:

                if wordNum == 0:
                    hitFrame.hitStartTime = bits
                    wordNum += 1

                elif wordNum == 1:
                    hitFrame.hitEndTime = bits
                    wordNum += 1

                elif wordNum == 2:
                    hitFrame.hitPeakADC = bits
                    wordNum += 1

                elif wordNum == 3:
                    hitFrame.hitPeakTime = bits
                    wordNum += 1

                elif wordNum == 4:
                    hitFrame.hitSumADC = bits
                    wordNum += 1

                elif wordNum == 5:
                    hitFrame.hitFlags = bits
                    hitFrame.hitContinues = (bits & Masks["hit_continuesmask"]) >> 15
                    wordNum = 0
                    hitPacket.hitPayload.hits.append(hitFrame)
                    hitFrame = hit.HitFrame()

            hitPackets.append(hitPacket)

        return hitPackets

    def _formatHitToAxi4s(self, hitPackets):

        axi4sPackets = []
        if isinstance(hitPackets, list) is False:
            # hitPackets = self._readHits(hitPackets)
            data = []
            for line in hitPackets:
                packet = hit.HitPacket()
                l = line.split()
                packet.hitHeader.crate = int(l[0])
                packet.hitHeader.fibre = int(l[1])
                packet.hitHeader.wireIndex = int(l[2])
                packet.hitHeader.pipeline = int(l[3])
                packet.hitHeader.slot = int(l[4])
                packet.hitHeader.flags = int(l[5])
                packet.hitHeader.tstamp = int(l[6])
                hs = []

                for ival, val in enumerate(l):

                    if ival > 6:
                        j = (ival - 6) % 6
                        hs.append(int(val))

                        if j == 0 and ival > 10:
                            packet.hitPayload.hits.append(hs)
                            hs = []

                data.append(packet)
            hitPackets = data

        for packet in hitPackets:
            hitcontinues = False
            axi4sPacket = axi4s.Axi4sPacket()

            axi4sPacket.header.slot = packet.hitHeader.slot
            axi4sPacket.header.crate = packet.hitHeader.crate
            axi4sPacket.header.fibre = packet.hitHeader.fibre
            axi4sPacket.header.wireIndex = packet.hitHeader.wireIndex
            axi4sPacket.header.pipeline = packet.hitHeader.pipeline
            axi4sPacket.header.flags = packet.hitHeader.flags
            axi4sPacket.header.tstamp = packet.hitHeader.tstamp

            for _ in xrange(6):
                axi4sPacket.flags.tvalid.append(1)
                if _ < 5:
                    tu = 0
                else:
                    tu = 1

                axi4sPacket.flags.tuser.append(tu)
                axi4sPacket.flags.tlast.append(0)
                axi4sPacket.flags.tkeep.append(3)
                axi4sPacket.flags.tready.append(1)

            for iframe, frame in zip(
                xrange(len(packet.hitPayload.hits)), packet.hitPayload.hits
            ):
                axi4sPacket.payload.data.append(frame.hitStartTime)
                axi4sPacket.payload.data.append(frame.hitEndTime)
                axi4sPacket.payload.data.append(frame.hitPeakADC)
                axi4sPacket.payload.data.append(frame.hitPeakTime)
                axi4sPacket.payload.data.append(frame.hitSumADC)
                axi4sPacket.payload.data.append(frame.hitFlags)

                for _ in xrange(6):
                    axi4sPacket.flags.tvalid.append(1)

                    if _ < 5:
                        tu = 0
                        tl = 0
                    else:
                        tu = 1
                        tl = 0
                        try:
                            test = packet.hitPayload.hits[iframe + 1]
                        except (IndexError):
                            if (
                                int((Masks["hit_continuesmask"] & frame.hitFlags) >> 15)
                                == 1
                            ):
                                tl = 1
                                hitcontinues = True
                            else:
                                tl = 0

                    axi4sPacket.flags.tuser.append(tu)
                    axi4sPacket.flags.tlast.append(tl)
                    axi4sPacket.flags.tkeep.append(3)
                    axi4sPacket.flags.tready.append(1)

            if not hitcontinues:
                axi4sPacket.payload.data.append(0)
                axi4sPacket.flags.tvalid.append(1)
                axi4sPacket.flags.tuser.append(0)
                axi4sPacket.flags.tlast.append(1)
                axi4sPacket.flags.tkeep.append(0)
                axi4sPacket.flags.tready.append(1)

            axi4sPackets.append(axi4sPacket)

        return axi4sPackets

    def _dumpPackets(self, Packets):

        for packet in Packets:
            print(packet)

    def _writeHits(self, file, Packets):

        try:
            if Packets[0].flags.tready == 0 or Packets[0].flags.tready == 1:
                Packets = self._formatAxi4sHitToHit(Packets)
        except (AttributeError, IndexError):
            Packets = Packets

        for packet in Packets:
            file.write(str(packet.hitHeader.crate) + " ")
            file.write(str(packet.hitHeader.fibre) + " ")
            file.write(str(packet.hitHeader.wireIndex) + " ")
            file.write(str(packet.hitHeader.pipeline) + " ")
            file.write(str(packet.hitHeader.slot) + " ")
            file.write(str(packet.hitHeader.flags) + " ")
            file.write(str(packet.hitHeader.tstamp) + " ")

            for hit in packet.hitPayload.hits:
                file.write(str(hit.hitStartTime) + " ")
                file.write(str(hit.hitEndTime) + " ")
                file.write(str(hit.hitPeakADC) + " ")
                file.write(str(hit.hitPeakTime) + " ")
                file.write(str(hit.hitSumADC) + " ")
                file.write(str(hit.hitFlags) + " ")

            file.write("\n")
