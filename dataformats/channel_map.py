import os
import numpy as np

# FEMB/WIB parameters
N_CHAN_PER_ASIC = 8  # this isn't right, we will need to fix it some time
N_ASIC_PER_FEMB = 8
N_FEMB_PER_WIB = 4
N_CHAN_PER_WIB = N_CHAN_PER_ASIC * N_ASIC_PER_FEMB * N_FEMB_PER_WIB

N_CHAN_PER_WIB_PACKET = 64

# TPG parameters
N_TICKS_PER_TP_PACKET = 64
N_PIPELINES = 4


class ChannelMap(object):
    """Class that will read and store the offline channel numbering scheme"""

    def __init__(self):
        super(ChannelMap, self).__init__()
        path = os.environ["DF_ROOT"] + "/data/protoDUNETPCChannelMap_FELIX_v4.txt"
        self.data = np.loadtxt(path, delimiter="\t", dtype="int")

    # Lookup various numbers by offline channel number
    def crate(self, iChan):
        irow = np.where(self.data[:, 12] == iChan)
        return int(self.data[irow, 0])

    def slot(self, iChan):
        irow = np.where(self.data[:, 12] == iChan)
        return int(self.data[irow, 1])

    def fiber(self, iChan):
        irow = np.where(self.data[:, 12] == iChan)
        return int(self.data[irow, 2])

    def fembChan(self, iChan):
        irow = np.where(self.data[:, 12] == iChan)
        return int(self.data[irow, 3])

    def streamChan(self, iChan):
        irow = np.where(self.data[:, 12] == iChan)
        return int(self.data[irow, 4])

    def slotID(self, iChan):
        irow = np.where(self.data[:, 12] == iChan)
        return int(self.data[irow, 5])

    def fiberID(self, iChan):
        irow = np.where(self.data[:, 12] == iChan)
        return int(self.data[irow, 6])

    def chip(self, iChan):
        irow = np.where(self.data[:, 12] == iChan)
        return int(self.data[irow, 7])

    def chipChan(self, iChan):
        irow = np.where(self.data[:, 12] == iChan)
        return int(self.data[irow, 8])

    def asic(self, iChan):
        irow = np.where(self.data[:, 12] == iChan)
        return int(self.data[irow, 9])

    def asicChan(self, iChan):
        irow = np.where(self.data[:, 12] == iChan)
        return int(self.data[irow, 10])

    def planeType(self, iChan):
        irow = np.where(self.data[:, 12] == iChan)
        return int(self.data[irow, 11])

    def channel(self, iCrate, iSlot, iFiber, iFEMBChan):
        """look up offline channel based on HW coordinates"""
        vals = np.array([iCrate, iSlot, iFiber, iFEMBChan])
        irow = np.where(np.all(self.data[:, [0, 1, 2, 3]] == vals, axis=1))
        return self.data[int(irow[0]), 12]

    def channel2(self, iCrate, iSlot, iFiber, iStreamChan):
        """Alternative lookup using stream channel"""
        vals = np.array([iCrate, iSlot, iFiber, iStreamChan])
        irow = np.where(np.all(self.data[:, [0, 1, 2, 4]] == vals, axis=1))
        return self.data[int(irow[0]), 12]

    ## These methods are for indexing data in a WIB packet or TP pipeline
    def tpChan(self, iCrate, iFiber, iWireIndex):
        return (iCrate & 0x1F) << 11 + (iFiber & 0x7) << 8 + (iWireIndex & 0xFF)

    def tpWireIndex(self, iColdata, iADC, iADCChan):
        return (iColData & 0x3) << 6 + (iADC & 0x7) << 3 + (iADCChan & 0x7)

    def tpColdata(self, iTPWireIndex):
        return (iTPWireIndex >> 6) & 0x3

    def tpADC(self, iTPWireIndex):
        return (iTPWireIndex >> 3) & 0x7

    def tpADCChan(self, iTPWireIndex):
        return iTPWireIndex & 0x7

    def tpPipe(self, iTPWireIndex):
        return iTPWireIndex & 0x3

    # This method is to help convert from TP numbering scheme to offline
    # It is almost certainly WRONG...
    # Offline numbering uses "chip" (0-7), "ASIC" (0-1), "ASICChan" (0-8)
    # TP/WIB packets use "COLDATA" (0-3), "ADC" (0-7), "ADCChan" (0-8)
    def tpWireIndex2(self, iChip, iASIC, iASICChan):
        return (iChip & 0x7) << 3 + (iASICChan & 0x7)

    # A workaround that decouples testing from the offline channel map
    # hfChannel is an arbitrary index between 0 and 255 that allows mapping of channels in a phil file into WIB packets
    def tpColdataFromHFChannel(self, iHFChannel):
        return (iHFChannel >> 6) & 0x3

    def tpADCFromHFChannel(self, iHFChannel):
        return (iHFChannel >> 3) & 0x7

    def tpADCChannelFromHFChannel(self, iHFChannel):
        return iHFChannel & 0x7

    def hfChannel(self, iColdata, iADC, iADCChan):
        return ((iColdata & 0x3) << 6) + ((iADC & 0x7) << 3) + (iADCChan & 0x7)

    def hfChannelHitfile(self, iCrate, iSlot, iFibre, iWireIndex):
        return (
            ((iCrate & 0x1F) << 12)
            + ((iSlot & 0x7) << 10)
            + ((iFibre & 0x7) << 8)
            + (iWireIndex & 0xFF)
        )

    def crateFromHfChannel(self, chan):
        return ((chan >> 12) & 0x1f)
    
    def slotFromHfChannel(self, chan):
        return (((chan >> 10) & 0x7) % 4)

    def fibreFromHfChannel(self, chan):
        return (((chan >> 8) & 0x7) % 4)
    
    def wireIndexFromHfChannel(self, chan):
        return (chan & 0xff)
