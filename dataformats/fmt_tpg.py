import numpy as np
import scipy.signal as signal

import axi4s
import hit


class TriggerPrimitiveGenerator:
    """Emulate the trigger primitive chain"""

    def __init__(
        self,
        thresh=30,
        ped=500,
        firCoeffs=[
            -0.0,
            -0.0,
            -0.0,
            -0.0,
            -0.0,
            -0.0,
            0.0,
            0.0,
            2.0,
            4.0,
            6.0,
            7.0,
            9.0,
            11.0,
            12.0,
            13.0,
            13.0,
            12.0,
            11.0,
            9.0,
            7.0,
            6.0,
            4.0,
            2.0,
            0.0,
            0.0,
            -0.0,
            -0.0,
            -0.0,
            -0.0,
            -0.0,
            -0.0,
        ],
        firShift=7,
    ):
        super(TriggerPrimitiveGenerator, self).__init__()
        self.firCoeffs = firCoeffs
        self.firShift = firShift
        self.threshold = thresh
        self.pedestal = ped

    def firFilter(self, adcs):
        out = signal.lfilter(self.firCoeffs, [1.0], adcs).astype(int)
        out = np.right_shift(out, self.firShift)
        return out

    def pedestalSubtraction(self, adcs):
        return adcs - self.pedestal

    def hitFinder(self, adcs):

        # index of ADCs above threshold
        i_gt = np.nonzero(adcs > self.threshold)

        # find start of hits
        i_st = np.insert(np.nonzero(np.diff(i_gt) != 1)[1] + 1, 0, 0)
        starts = np.take(i_gt, i_st)

        # find end of hits
        i_en = np.nonzero(np.diff(i_gt) != 1)[1]
        ends = np.take(i_gt, i_en) + 1
        ends = np.insert(ends, len(ends), i_gt[0][-1] + 1)

        # find hit sums
        sums = np.array([], dtype=int)
        for st, en in zip(starts, ends):
            s = np.sum(adcs[np.arange(st, en, 1)])
            sums = np.append(sums, s)
        return starts, ends, sums

    def tpg(self, adcs):
        a2 = self.pedestalSubtraction(adcs)
        a3 = self.firFilter(a2)
        return self.hitFinder(a3)

    def runTPG(self, ip):
        """Input *must* be an AXI4S packet in unpacker format"""

        # run the TP chain
        adcs = np.array(ip.payload.data)
        starts, ends, sums = self.tpg(adcs)

        # format the output
        op = hit.HitPacket()
        op.hitHeader.slot = ip.header.slot
        op.hitHeader.crate = ip.header.crate
        op.hitHeader.fibre = ip.header.fibre
        op.hitHeader.wireIndex = ip.header.wireIndex
        op.hitHeader.pipeline = ip.header.pipeline
        op.hitHeader.flags = ip.header.flags
        op.hitHeader.tstamp = ip.header.tstamp

        # store hits
        for t0, t1, s in zip(starts, ends, sums):
            h = hit.HitFrame()
            h.hitStartTime = t0
            h.hitEndTime = t1
            h.hitSumADC = s
            op.hitPayload.hits.append(h)

        return op

    def runTPGMany(self, ip):
        op = []
        for p in ip:
            op.append(runTPG(p))
        return op
