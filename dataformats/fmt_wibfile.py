import wib2g
import cabling


class WibFileFormatter(object):
    """WIB file formatter"""

    def __init__(self):
        super(WibFileFormatter, self).__init__()

    def _writeWibHeader(self, hdr):
        line = str()
        line += str(hdr.crate) + ","
        line += str(hdr.slot) + ","
        line += str(hdr.fiber) + ","
        line += str(hdr.version) + ","
        line += str(hdr.errors) + ","
        line += str(hdr.oos) + ","
        line += str(hdr.mm) + ","
        line += str(hdr.timestamp) + ","
        line += str(hdr.counter)
        line += "\n"
        return line

    def _readWibHeader(self, line):
        vars = line.split(",")
        hdr = wib2g.WibHeader()
        hdr.crate = int(vars[0])
        hdr.slot = int(vars[1])
        hdr.fiber = int(vars[2])
        hdr.version = int(vars[3])
        hdr.errors = int(vars[4])
        hdr.oos = int(vars[5])
        hdr.mm = int(vars[6])
        hdr.timestamp = long(vars[7])
        hdr.counter = int(vars[8])
        return hdr

    def _writeColdataHeader(self, chdr):
        line = str()
        line += str(chdr.chksma) + ","
        line += str(chdr.chksmb) + ","
        line += str(chdr.errorregister) + ","
        line += str(chdr.stream1err) + ","
        line += str(chdr.stream2err) + ","
        line += str(chdr.convertcount) + ","
        for i in range(8):
            line += str(chdr.hdr[i]) + ","
        line += "\n"
        return line

    def _readColdataHeader(self, line):
        vars = line.split(",")
        chdr = wib2g.ColdataHeader()
        chdr.chksma = int(vars[0])
        chdr.chksmb = int(vars[1])
        chdr.errorregister = int(vars[2])
        chdr.stream1err = int(vars[3])
        chdr.stream2err = int(vars[4])
        chdr.convertcount = int(vars[5])
        for i in range(8):
            chdr.hdr[i] = int(vars[6 + i])
        return chdr

    def _writeColdataADCCollection(self, coll):
        s = str()
        for i in range(8):
            for j in range(8):
                s += str(coll.adcs[i][j]) + ","
            s += "\n"
        return s

    def _readColdataADCCollection(self, lines):
        coll = wib2g.ColdataADCCollection()
        for line, i in zip(lines, range(8)):
            vars = line.split(",")
            for var, j in zip(vars, range(8)):
                coll.adcs[i][j] = int(var)
        return coll

    def writeWibPacket(self, packet):
        s = str()
        s += self._writeWibHeader(packet.header)
        for i in range(4):
            s += self._writeColdataHeader(packet.coldata[i].header)
            s += self._writeColdataADCCollection(packet.coldata[i].data)
        s += "\n"
        return s

    def readWibPacket(self, lines):
        packet = wib2g.WibData()
        packet.header = self._readWibHeader(lines[0])
        for i in range(4):
            packet.coldata[i].header = self._readColdataHeader(lines[(i * 9) + 1])
            packet.coldata[i].data = self._readColdataADCCollection(
                lines[(i * 9) + 2 : (i * 9) + 10]
            )
        return packet

    def writeWibList(self, packets):
        s = str()
        for packet in packets:
            s += self.writeWibPacket(packet)
        return s

    def readWibList(self, s):
        lines = s.split("\n")
        packets = []
        npackets = int(len(lines) / 38)
        for i in range(npackets):
            packets.append(self.readWibPacket(lines[i * 38 : i * 38 + 37]))
        return packets
