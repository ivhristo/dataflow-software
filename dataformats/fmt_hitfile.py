import hit
import cabling
import channel_map

# -----------------------------------------------------------------------------
class HitFileFormatter(object):
    """ Hit file formatter

    Dumps and loads axi4 stream hit data (outbuf) from/to hitfile format used in lArSoft.
    Hitfile format is:
    # 1: event, channel, collection?, hit_continues, hitstart, hitsize, hitpeak(time), hitpeak(adc), hitsumadc
    # 2: event, channel, ...
    # .
    # .
    # .

    # There are 3 forms of hit packet in the AXI4 Stream:
    # 1) 1 hit in a packet.
    # 2) multiple hits in a packet.
    # 3) no hits in a packet (still has header).
    Refer to the document here for format descriptions: https://docs.google.com/document/d/1rirDwFS_4Anp3v1A5W8cVngqi5dKGZzyoiDyt-lp2Pk/edit

    # All lines have only 18 significant bits, of which only 16 are used to store data.
    # The header is 6 (x 16 bit) words long.
    # The hit payload is N x 6 (x 16 bit) words long, where N is the number of hits in one packet.
    """

    def __init__(self):
        super(HitFileFormatter, self).__init__()
        self.cm = channel_map.ChannelMap()

    def _formatHitsToHitfile(self, hitPackets):

        hitStream = []
        event = 0
        chanIncrement = 0

        for packet in hitPackets:

            # masks only needed for last word in hit frame
            hit_continuesmask = int("1000000000000000", 2)
            hit_flagsmask = int("0111111111111111", 2)

            ####### SET AND CALCULATE PARAMETERS FOR HITFILE FORMAT ########

            event = int(event)
            crate = int(packet.hitHeader.crate)
            fibre = int(packet.hitHeader.fibre)
            wireIndex = int(packet.hitHeader.wireIndex)
            pipeline = int(packet.hitHeader.pipeline)
            slot = int(packet.hitHeader.slot)
            hdrflags = int(packet.hitHeader.flags)

            ts = int(packet.hitHeader.tstamp)
            collection = int(1)  # What to do with induction wires?
            channel = self.cm.hfChannelHitfile(crate, slot, fibre, wireIndex)

            for h in packet.hitPayload.hits:
                hitstart = h.hitStartTime  # + ts
                hitsize = h.hitEndTime - h.hitStartTime
                hitpeakadc = h.hitPeakADC
                hitpeaktime = h.hitPeakTime  # + ts
                hitsumadc = h.hitSumADC
                hit_continues = int((hit_continuesmask & h.hitFlags) >> 15)
                hit_flags = hit_flagsmask & h.hitFlags

                hitStream.append(
                    [
                        event,
                        channel,
                        collection,
                        hit_continues,
                        hitstart,
                        hitsize,
                        hitpeaktime,
                        hitpeakadc,
                        hitsumadc,
                    ]
                )

        return hitStream

    # Currently there is no way of knowing how many hit frames there were in a single hit packet from the hitfile.
    # Also, it is not possible to recover header information until mapping between hf and larsoft channels is fixed.
    # Hit event has no meaning for HF data.
    def _formatHitfileToHits(self, data, ts=0):
        chans = []
        hitPackets = []
        i = 0
        """
        packet = hit.HitPacket()
        """

        if isinstance(data, list) is False:
            data = self.readHitfile(data)
        else:
            data = data

        # Loop over each line in the hitfile.
        for iline, line in enumerate(data):

            # Get each parameter on one line as an integer.
            event = int(line[0])
            chan = int(line[1])
            chans.append(chan)
            hfChan = int(iline)
            col = int(line[2])
            wireIndex = hfChan % 256
            fibre = (hfChan // 256) % 4
            slot = (hfChan // (256 * 4)) % 4
            crate = hfChan // (256 * 4 * 4)
            pipeline = hfChan % 4

            # Not possible to recover flags contained in word 6 of hit frame?
            cont = int(line[3])
            hitstart = int(line[4])
            hitsize = int(line[5])
            hitpeaktime = int(line[6])
            hitpeakadc = int(line[7])
            hitsadc = int(line[8])

            # Hit end time is just the reverse calculation for hit size. Currently now way of recovering
            # timestamp. Is it necessary?
            hitend = hitstart + hitsize

            hitframe = hit.HitFrame()
            hitframe.hitStartTime = hitstart
            hitframe.hitEndTime = hitend
            hitframe.hitPeakADC = hitpeakadc
            hitframe.hitPeakTime = hitpeaktime
            hitframe.hitSumADC = hitsadc
            hitframe.hitContinues = cont
            hitframe.hitFlags = int(cont << 15)

            # If channel number is fixed, we could count and store the channel number and pack subsequent hits
            # with same channel number into one hit packet with the following logic below.

            if i > 0 and chans[i - 1] == chan:
                packet.hitPayload.hits.append(hitframe)

            elif i == 0:
                packet = hit.HitPacket()

                packet.hitHeader.crate = crate
                packet.hitHeader.fibre = fibre
                packet.hitHeader.wireIndex = wireIndex
                packet.hitHeader.pipeline = pipeline
                packet.hitHeader.slot = slot
                packet.hitHeader.flags = 0
                packet.hitHeader.tstamp = ts

                packet.hitPayload.hits.append(hitframe)

            elif chans[i - 1] != chan:
                hitPackets.append(packet)
                packet = hit.HitPacket()

                packet.hitHeader.crate = crate
                packet.hitHeader.fibre = fibre
                packet.hitHeader.wireIndex = wireIndex
                packet.hitHeader.pipeline = pipeline
                packet.hitHeader.slot = slot
                packet.hitHeader.flags = 0
                packet.hitHeader.tstamp = ts

                packet.hitPayload.hits.append(hitframe)

            try:
                nextLine = data[i + 1]
            except (IndexError):
                hitPackets.append(packet)
                packet = hit.HitPacket()

            i += 1

        return hitPackets

    def readHitfile(self, file):

        data = []
        for line in file:
            # Split the line by spaces between numbers (the way it was formatted).
            elems = [int(elem) for elem in line.split()]
            data.append(elems)

        return data

    def _writeHitfile(self, file, data):

        for hitfileLine in data:

            for elem, i in zip(hitfileLine, xrange(len(hitfileLine))):

                file.write(str(elem))

                if (i + 1) < 9:
                    file.write(" ")

            file.write("\n")

    def _dumpHitfile(self, data):

        for h in data:
            print(
                "Event: {}, Channel: {}, Collection?: {}, Hit continues?: {}, Hit start: {}, Hit size: {}, Hit peak time: {}, Hit peak ADC: {}, Hit sum ADC: {}".format(
                    h[0], h[1], h[2], h[3], h[4], h[5], h[6], h[7], h[8]
                )
            )
