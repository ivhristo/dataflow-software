""" General placeholder for hits to enable conversion between formats post-hitfinding """
#
# Data format described here: https://gitlab.cern.ch/DUNE-SP-TDR-DAQ/dataflow-firmware/blob/documentation/docs/dune_output_memory_map_02_v1.2.xlsx
# and here: https://docs.google.com/document/d/1rirDwFS_4Anp3v1A5W8cVngqi5dKGZzyoiDyt-lp2Pk/edit
# See this link to understand tuser, tlast, tvalid, etc.: https://wiki.dunescience.org/w/img_auth.php/d/df/Output_Interface_v1.1.pdf
#

# -----------------------------------------------------------------------------
class HitPacket(object):
    """docstring for HitData"""

    def __init__(self):
        super(HitPacket, self).__init__()
        self.hitHeader = HitHeader()
        self.hitPayload = HitPayload()

    def __str__(self):
        headerSortOrder = {
            "crate": 0,
            "fibre": 1,
            "wireIndex": 2,
            "pipeline": 3,
            "slot": 4,
            "flags": 5,
            "tstamp": 6,
        }
        return "\n".join(
            [
                "Hit Header: \n"
                + ", ".join(
                    [
                        var + ": " + str(self.hitHeader.__str__()[var])
                        for var in sorted(
                            self.hitHeader.__str__(), key=lambda x: headerSortOrder[x]
                        )
                    ]
                )
            ]
            + ["\nHit Payload: \n" + self.hitPayload.__str__() + "\n"]
            # + '\n'.join(['   '+l for l in c.__str__().split('\n')]) for i, c in enumerate(self.hitProperties)]
        )

    def __eq__(self, other):
        return self.hitHeader == other.hitHeader and self.hitPayload == other.hitPayload

    def __ne__(self, other):
        return self.hitHeader != other.hitHeader or self.hitPayload != other.hitPayload

    def __hash__(self):
        return hash((self.hitHeader, self.hitPayload))


# -----------------------------------------------------------------------------
# Store parameters from hit header as numbers.
class HitHeader(object):
    """docstring for HitHeader"""

    def __init__(self):
        super(HitHeader, self).__init__()
        # self.tlast      = [[0] for _ in xrange(6)]
        # self.tuser      = [[0] for _ in xrange(6)]
        self.crate = 0
        self.fibre = 0
        self.wireIndex = 0
        self.pipeline = 0
        self.slot = 0
        self.flags = 0
        self.tstamp = 0

    def __str__(self):
        return vars(self)

    def __eq__(self, other):
        return (
            self.crate == other.crate
            and self.fibre == other.fibre
            and self.wireIndex == other.wireIndex
            and self.pipeline == other.pipeline
            and self.slot == other.slot
            and self.flags == other.flags  # and
            # self.tstamp     == other.tstamp
        )

    def __ne__(self, other):
        return (
            self.crate != other.crate
            or self.fibre != other.fibre
            or self.wireIndex != other.wireIndex
            or self.pipeline != other.pipeline
            or self.slot != other.slot
            or self.flags != other.flags  # or
            # self.tstamp     != other.tstamp
        )

    def __hash__(self):
        return hash(
            (
                self.crate,
                self.fibre,
                self.wireIndex,
                self.pipeline,
                self.slot,
                self.flags,
                self.tstamp,
            )
        )


# -----------------------------------------------------------------------------
# 1) The class supports dynamically filling the self.hits array with the data based on how many distinct
# hits are input to the class. This means tstart, tend, etc. will have to be input as arrays [hit 1 tstart, hit 2 tstart, etc]
# and the flags tlast and tuser are not stored.
#
# 2) Can create a default empty array by HitPayload()
#
# 3) Can also create an exact array filled with 0s for x hits by HitPayload(frames=x)


class HitPayload(object):
    """docstring for HitPayload"""

    def __init__(self):
        super(HitPayload, self).__init__()

        self.hits = []

    def __str__(self):
        # return
        sortorder = {
            "hitStartTime": 0,
            "hitEndTime": 1,
            "hitPeakADC": 2,
            "hitPeakTime": 3,
            "hitSumADC": 4,
            "hitFlags": 5,
            "hitContinues": 6,
        }
        return "\n".join(
            [
                (
                    "%d : " % i
                    + ", ".join(
                        [
                            h + ": " + str(hit.__str__()[h])
                            for h in sorted(hit.__str__(), key=lambda x: sortorder[x])
                        ]
                    )
                )
                for i, hit in enumerate(self.hits)
            ]
        )  #', '.join(['%d '%j for k, j in enumerate(self.hit)])for i, hit in enumerate(self.hits)])

        # Can return just the variables if storing the other way:
        # return str(vars(self))

    def __eq__(self, other):
        return self.hits == other.hits

    def __ne__(self, other):
        return self.hits != other.hits

    def __hash__(self):
        return hash((self.hits))


class HitFrame(object):
    """docstring for a Hit"""

    def __init__(self):
        super(HitFrame, self).__init__()

        self.hitStartTime = 0
        self.hitEndTime = 0
        self.hitPeakADC = 0
        self.hitPeakTime = 0
        self.hitSumADC = 0
        self.hitFlags = 0  # HitContinues (1 bit) + other flags (12 bits)
        self.hitContinues = 0

    def __str__(self):
        return vars(self)

    def __eq__(self, other):
        return (
            self.hitStartTime == other.hitStartTime
            and self.hitEndTime == other.hitEndTime
            and self.hitPeakADC == other.hitPeakADC
            and self.hitPeakTime == other.hitPeakTime
            and self.hitSumADC == other.hitSumADC
            and self.hitContinues == other.hitContinues
            and self.hitFlags == other.hitFlags
        )

    def __ne__(self, other):
        return (
            self.hitStartTime != other.hitStartTime
            or self.hitEndTime != other.hitEndTime
            or self.hitPeakADC != other.hitPeakADC
            or self.hitPeakTime != other.hitPeakTime
            or self.hitSumADC != other.hitSumADC
            or self.hitContinues != other.hitContinues
            or self.hitFlags != other.hitFlags
        )

    def __hash__(self):
        return hash(
            (
                self.hitStartTime,
                self.hitEndTime,
                self.hitPeakADC,
                self.hitPeakTime,
                self.hitSumADC,
                self.hitFlags,
                self.hitContinues,
            )
        )
