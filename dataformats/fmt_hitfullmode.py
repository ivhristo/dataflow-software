import wib2g
import hit

b33 = __import__("33b")
import fmt_hitfile
import cabling

# -----------------------------------------------------------------------------
class CrInterfaceFormatter(object):
    """
    CR Interface WibData formatter

    Dumps wib data from hitfinder output axi4 data stream (outbuf format).

    CR / CR Interface format described by S. Ponzio here:
    https://docs.google.com/document/d/1vchwT3Yam2A4Cecq5Tnr_aV9oz9gBMU4lk2miEv1xew/edit

    The Hit data will be packed into 32b packets with 1b flag as follows:

bit:  32,    31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0   
      1     [                                                       Word SOF                                                     ]  
      0     [      Crate       ][   Fibre  ][       WireIndex      ][ Pipe ][ Slot no. ][            Header Flags                ]  
      0     [                                                       Timestamp                                                    ]  
      0     [                                                       Timestamp                                                    ]  
      0     [                     Hit 1 Start Time                      ][                    Hit 1 End Time                     ]  
      0     [                     Hit 1 Peak ADC                        ][                    Hit 1 Peak Time                    ]  
      0     [                     Hit 1 Sum ADC                         ][                Hit 1 Continue & Flags                 ]  
      0                                                                 .                                                           
      0                                                                 .                                                           
      0                                                                 .                                                           
      0     [                     Hit n Start Time                      ][                    Hit n End Time                     ]  
      0     [                     Hit n Peak ADC                        ][                    Hit n Peak Time                    ]  
      0     [                     Hit n Sum ADC                         ][                Hit n Continue & Flags                 ]  
      1     [                                                       Word EOP                                                     ]  
      1     [                                                     Word 1 IDLE                                                    ]  
      1                                                                 .                                                           
      1                                                                 .                                                           
      1                                                                 .                                                           
      1     [                                                     Word m IDLE                                                    ]                                  

    """

    def __init__(self):
        super(CrInterfaceFormatter, self).__init__()

    def padhex(self, d, bits):
        return "0x" + hex(d)[2:].zfill(int(bits / 4)) + "\n"

    # Function to convert hit objects to a 33b bus (CR Interface function).
    def _formatHitsToCR(self, hitpackets, m=0):

        # Constants.
        WORD_IDL = 0x1000000BC
        WORD_SOF = 0x10000003C
        WORD_EOF = 0x1000000DC

        # Initialise 33b bus object.
        bus = b33.Bus33bData()

        # Loop over all hitpackets in the data and count.
        for packet, ipacket in zip(hitpackets, xrange(len(hitpackets))):

            # Append SOF infront of each packet.
            # data.append(padhex(WORD_SOF,32)[:-1] + ' 1')
            bus.data.append(WORD_SOF)

            # Append hit header frame information to 33b bus.
            word = 0 << 32
            word += (packet.hitHeader.crate & 0x1F) << 27
            word += (packet.hitHeader.fibre & 0x7) << 24
            word += (packet.hitHeader.wireIndex & 0x3F) << 18
            word += (packet.hitHeader.pipeline & 0x3) << 16
            word += (packet.hitHeader.slot & 0x7) << 13
            word += packet.hitHeader.flags & 0x1FFF
            # data.append(self.padhex(word, 32)[:-1] + ' 0')
            bus.data.append(word)

            # 2nd word in header (timestamp). Store the leading 32 bits of the 64 bit timestamp
            # in this line.
            word = 0 << 32
            word += int((packet.hitHeader.tstamp >> 32) & 0xFFFFFFFF)
            # data.append(self.padhex(word, 32)[:-1] + ' 0')
            bus.data.append(word)

            # 3rd word in header (timestamp). Store the last 32 bits of the 64 bit timestamp on
            # this line. This completes the hit packet header formatting.
            word = 0 << 32
            word += int(packet.hitHeader.tstamp & 0xFFFFFFFF)
            # data.append(self.padhex(word, 32)[:-1] + ' 0')
            bus.data.append(word)

            # # Initalise new word with leading 0 flag for hit quantities.
            # word = 0 << 32

            # Append hit frame information for N hit frames in the packet.
            for hit, ihit in zip(
                packet.hitPayload.hits, xrange(len(packet.hitPayload.hits))
            ):

                # Initalise new word with leading 0 flag for hit quantities.
                word = 0 << 32
                word += (hit.hitStartTime & 0xFFFF) << 16
                word += hit.hitEndTime & 0xFFFF
                bus.data.append(word)

                word = 0 << 32
                word += (hit.hitPeakADC & 0xFFFF) << 16
                word += hit.hitPeakTime & 0xFFFF
                bus.data.append(word)

                word = 0 << 32
                word += (hit.hitSumADC & 0xFFFF) << 16
                word += hit.hitFlags & 0xFFFF
                bus.data.append(word)

                # # Loop over each hit quantity in hit frame (tstart, tend, hpeak, tpeak, sadc, hitcontinues).
                # for var, ivar in zip(hit, range(len(hit))):
                #     num = ivar%2

                #     # IF it is the 1st (tstart), 3rd (hpeak), or 5th (sadc) quantity, pack it in the
                #     # first 16 bits of the 32 bit word.
                #     if num == 0:
                #         word = (var & 0xffff) << 16

                #     # IF it is the 2st (tend), 4th (tpeak), or 6th (hit continues) quantity, pack
                #     # it in the last 16 bits of the 32 bit word.
                #     elif num == 1:
                #         word += (var & 0xffff)
                #         # data.append(self.padhex(word, 32)[:-1] + ' 0')
                #         bus.data.append(word)

                #         # After every 2 hit quantities, the 32 bits are filled and a new word is initialised
                #         # with flag bit 0 as the 33rd bit.
                #         word = 0 << 32

            # End of Frame
            # data.append(self.padhex(WORD_EOP,32)[:-1] + ' 1')
            bus.data.append(WORD_EOF)

            # Idle for M idle frames
            for i in xrange(m):
                # data.append(self.padhex(WORD_IDL,32)[:-1] + ' 1')
                bus.data.append(WORD_IDL)
        return bus.data

    def _readHitsFromCrInterface(self, file):

        bus = b33.Bus33bData()

        # Loop over the lines in the file and store data in the 33b bus object as integers.
        for line in file:
            l = line.split()
            bstring = str(l[1]) + format(int(l[0], 16), "032b")
            bus.data.append(int(bstring, 2))

        return bus.data

    def _formatFullModeToHits(self, stream):

        if isinstance(stream, list) is False:
            data = self._readHitsFromCrInterface(stream)
        else:
            data = stream

        # Array to store each hitpacket as it comes.
        hitpackets = []

        # Don't know what errors to deal with yet.
        # errors = []

        # Constants.
        WORD_IDL = 0x1000000BC
        WORD_SOF = 0x10000003C
        WORD_EOF = 0x1000000DC

        # Integer to identify which hit quantities are we looking at.
        i = 0

        # Array to store hit quantities for a single hit frame.
        h = hit.HitFrame()  # []

        # Initialise a hit object to store data.
        hitpacket = hit.HitPacket()

        # Loop over each line in the file and count line number.
        for (line, iline) in zip(data, xrange(len(data))):

            # Assume first line in datafile is the SOF word .
            if iline > 0 and line != WORD_SOF:

                # Assume first line after SOF is always a header word and unpack according to memory map above.
                if data[iline - 1] == WORD_SOF:
                    hitpacket.hitHeader.flags = line & 0x1FFF
                    hitpacket.hitHeader.slot = (line & 0xE000) >> 13
                    hitpacket.hitHeader.pipeline = (line & 0x30000) >> 16
                    hitpacket.hitHeader.wireIndex = (line & 0xFC0000) >> 18
                    hitpacket.hitHeader.fibre = (line & 0x7000000) >> 24
                    hitpacket.hitHeader.crate = (line & 0xF8000000) >> 27
                    flag = (line & 0x100000000) >> 32

                # Assume 2nd and 3rd words after SOF contain timestamp.
                elif data[iline - 2] == WORD_SOF:
                    hitpacket.hitHeader.tstamp = (line & 0xFFFFFFFF) << 32
                    flag = (line & 0x100000000) >> 32

                # Assume 2nd and 3rd words after SOF contain timestamp.
                elif data[iline - 3] == WORD_SOF:
                    hitpacket.hitHeader.tstamp += line & 0xFFFFFFFF
                    flag = (line & 0x100000000) >> 32

                # Assume header frame is always 3 x 33b word AND a hit frame follows right after.
                # IF EOF or IDLE word follows then there is no hits in this packet.
                elif line != WORD_EOF and line != WORD_IDL:

                    # First 33b word in hit frame contains tstart and tend.
                    if i == 0:
                        tstart = (line & 0xFFFF0000) >> 16
                        tend = line & 0x0000FFFF
                        flag = (line & 0x100000000) >> 32
                        h.hitStartTime = tstart  # append(tstart)
                        h.hitEndTime = tend  # append(tend)
                        i = i + 1

                    # Second 33b word in hit frame contains hpeak and tpeak.
                    elif i == 1:
                        hpeak = (line & 0xFFFF0000) >> 16
                        tpeak = line & 0x0000FFFF
                        flag = (line & 0x100000000) >> 32
                        h.hitPeakADC = hpeak  # append(hpeak)
                        h.hitPeakTime = tpeak  # append(tpeak)
                        i = i + 1

                    # Third 33b word in hit frame contains hsum and hitcontinues.
                    # Assume every hit frame is 3 x 33b words as in memory map above and reset i for
                    # next hit or new header if next line is EOF or IDLE.
                    # Append all hit quantities to the hitpacket object.
                    elif i == 2:
                        hsum = (line & 0xFFFF0000) >> 16
                        cont = line & 0x0000FFFF
                        flag = (line & 0x100000000) >> 32
                        h.hitSumADC = hsum  # append(hsum)
                        h.hitContinues = (cont & 0x8000) >> 15
                        h.hitFlags = cont  # append(cont)

                        hitpacket.hitPayload.hits.append(h)

                        h = hit.HitFrame()  # []
                        i = 0

                # IF line is EOF then append the complete hit packet to array (hitpackets) and
                # reset the hitpacket object for next data.
                elif line == WORD_EOF:
                    hitpackets.append(hitpacket)
                    hitpacket = hit.HitPacket()

                # IF line in data is IDLE, skip to next line.
                elif line == WORD_IDL:
                    continue

                else:
                    print("Warning! Line {:.0f} is not a valid word.".format(iline))

            # Check that first line is Start of frame
            else:
                try:
                    assert line == WORD_SOF
                    continue

                # If not then check if first line in data is IDLE
                except (AssertionError):
                    try:
                        assert line == WORD_IDL
                        print("First line in data is IDLE", format(WORD_IDL, "032b"))
                        continue

                    # If it is not SOF or IDLE, throw warning
                    except (AssertionError):
                        print(
                            "WARNING! First line in data is not Start of Frame, ",
                            format(WORD_SOF, "032b"),
                            " nor IDLE, ",
                            format(WORD_IDL, "032b"),
                        )
                        continue

        return hitpackets

    def _writeHitsToCR(self, file, data):

        # data = self._formatHitsToCR(hitpackets, m=0)   # Formatting done in hfDataflow
        for d in data:

            # file.write(self.padhex(d,33))             # Not sure how exactly the data should look like?
            # file.write(str(format(d,"033b")))
            # file.write("\n")

            # if write33bpat:
            flag = (d & 0x100000000) >> 32
            line = d & 0xFFFFFFFF

            file.write("0x" + str(hex(line)[2:].zfill(8)) + " " + str(flag))
            file.write("\n")

    def _dumpCrInterface(self, data):

        # stream = self._formatHitsToCR(hitpackets, m=0) # Formatting done in hfDataflow
        for d in data:
            flag = (d & 0x100000000) >> 32
            line = d & 0xFFFFFFFF
            print("0x" + str(hex(line)[2:].zfill(8)) + " " + str(flag))
            # print(format(d,"033b"))
