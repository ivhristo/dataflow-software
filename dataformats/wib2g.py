# -----------------------------------------------------------------------------
class WibData(object):
    """docstring for WibData"""

    def __init__(self):
        super(WibData, self).__init__()
        self.header = WibHeader()
        self.coldata = [Coldata() for _ in xrange(4)]

    def __str__(self):
        return "\n".join(
            ["header     : " + self.header.__str__()]
            + [
                ("coldata[%d]\n" % i)
                + "\n".join(["   " + l for l in c.__str__().split("\n")])
                for i, c in enumerate(self.coldata)
            ]
        )

    def __repr__(self):
        return str(self)

    def __eq__(self, other):
        return self.header == other.header and self.coldata == other.coldata


# -----------------------------------------------------------------------------
class WibHeader(object):
    """docstring for WibHeader"""

    def __init__(self):
        super(WibHeader, self).__init__()
        self.slot = 0
        self.crate = 0
        self.fiber = 0
        self.version = 0
        self.errors = 0
        self.oos = 0
        self.mm = 0
        self.timestamp = 0
        self.counter = 0

    def __str__(self):
        return str(vars(self))

    def __eq__(self, other):
        return (
            self.slot == other.slot
            and self.crate == other.crate
            and self.fiber == other.fiber
            and self.version == other.version
            and self.errors == other.errors
            and self.oos == other.oos
            and self.mm == other.mm
            and self.timestamp == other.timestamp
            and self.counter == other.counter
        )


# -----------------------------------------------------------------------------
class Coldata(object):
    """docstring for Coldata"""

    def __init__(self):
        super(Coldata, self).__init__()
        self.header = ColdataHeader()
        self.data = ColdataADCCollection()

    def __str__(self):
        return "\n".join(
            ["header     : " + self.header.__str__()] + ["data\n" + self.data.__str__()]
        )

    def __eq__(self, other):
        return self.header == other.header and self.data == other.data


# -----------------------------------------------------------------------------
class ColdataHeader(object):
    """docstring for ColdataHeader"""

    def __init__(self):
        super(ColdataHeader, self).__init__()
        self.chksma = 0
        self.chksmb = 0
        self.errorregister = 0
        self.stream1err = 0
        self.stream2err = 0
        self.convertcount = 0
        self.hdr = [0] * 8

    def __str__(self):
        return str(vars(self))

    def __eq__(self, other):
        return (
            self.chksma == other.chksma
            and self.chksmb == other.chksmb
            and self.errorregister == other.errorregister
            and self.stream1err == other.stream1err
            and self.stream2err == other.stream2err
            and self.convertcount == other.convertcount
            and self.hdr == other.hdr
        )


# -----------------------------------------------------------------------------
class ColdataADCCollection(object):
    """docstring for ColdataADCCollection"""

    def __init__(self):
        super(ColdataADCCollection, self).__init__()
        self.adcs = [[0] * 8 for _ in xrange(8)]

    def __str__(self):
        return "\n".join(
            [
                ("%d : " % i) + ", ".join(["0x%03x" % j for j in adc])
                for i, adc in enumerate(self.adcs)
            ]
        )

    def __eq__(self, other):
        return self.adcs == other.adcs
