# DUNE DAQ electronics map
# conversion between numbering schemes

# WIB parameters
N_FEMB_PER_WIB = 4
N_ADC_PER_FEMB = 8
N_CHAN_PER_ADC = 8

N_CHANS_PER_WIB = N_FEMB_PER_WIB * N_ADC_PER_FEMB * N_CHAN_PER_ADC

# HF parameters
N_STREAMS = 4
N_TICKS_PER_PACKET = 64


def icol(hfchan):
    return hfchan // N_CHANS_PER_WIB


def iadc(hfchan):
    return hfchan % N_ADC_PER_FEMB


def ich(hfchan):
    return hfchan % (N_CHAN_PER_ADC)
