import wib2g

# -----------------------------------------------------------------------------
class DPRWibFormatter(object):
    """FullMode WibData formatter

    dumps and loads wib data from/to fullmode data stream (33b words)
    """

    def __init__(self):
        super(DPRWibFormatter, self).__init__()

    def _formatHeader(self, header):
        stream = [0] * 3
        stream[0] = (
            ((header.version & 0x1F) << 46)
            | ((header.fiber & 0x7) << 51)
            | ((header.crate & 0x1F) << 54)
            | ((header.slot & 0x7) << 59)
            | ((header.mm & 0x1) << 62)
            | ((header.oos & 0x1) << 63)
        )
        stream[1] = header.timestamp
        # stream[2] = (
        #     ((header.errors & 0xffff) << 16)
        #     )
        return stream

        # def _formatColdata(self, coldata):
        # stream = []
        # stream += self._formatColdataHdr(coldata.header)
        # stream += self._formatColdataAdcs(coldata.data.adcs)

        return stream

    def _formatColdataHdr(self, header):
        stream = [0] * 2
        stream[0] = (
            ((header.chksma & 0xFFFF) << 0)
            | ((header.chksmb & 0xFFFF) << 16)
            | ((header.convertcount & 0xFFFF) << 32)
            | ((header.errorregister & 0xFFFF) << 48)
        )
        #     (header.stream1err & 0xf) |
        #     ((header.stream2err & 0xf) << 4) |
        for i in xrange(8):
            stream[1] |= (header.hdr[i] & 0xFF) << 4 * i

        return stream

    def _formatColdataAdcs(self, adcs):
        stream = [0] * 16

        for i in xrange(8):
            for j in xrange(2):
                for k in xrange(4):
                    stream[i * 2 + j] |= adcs[i][4 * j + k] << 16 * k

        return stream

    def dump(self, wibdata):

        stream = []
        stream += self._formatHeader(wibdata.header)
        for i in xrange(4):
            stream += self._formatColdataHdr(wibdata.coldata[i].header)

        for i in xrange(4):
            stream += self._formatColdataAdcs(wibdata.coldata[i].data.adcs)

        #        for i, w in enumerate(stream):
        #            print "%03d" % i, '0x%016x' % w
        return stream

    def dumpList(self, wiblist, n_idles=2):
        stream = []

        for w in wiblist:
            stream += self.dump(w)

        return stream
