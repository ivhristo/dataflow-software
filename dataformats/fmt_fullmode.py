import wib2g

# -----------------------------------------------------------------------------
class FullModeWibFormatter(object):
    """FullMode WibData formatter

    dumps and loads wib data from/to fullmode data stream (33b words)
    """

    WORD_IDL = 0x1000000BC
    WORD_SOF = 0x10000003C
    WORD_EOF = 0x1000000DC

    def __init__(self):
        super(FullModeWibFormatter, self).__init__()

    def _formatHeader(self, header):
        stream = [0] * 4
        stream[0] = (
            ((header.version & 0x1F) << 8)
            | ((header.fiber & 0x7) << 13)
            | ((header.crate & 0x1F) << 16)
            | ((header.slot & 0x7) << 21)
        )
        stream[1] = (
            (header.mm & 0x1)
            | ((header.oos & 0x1) << 1)
            | ((header.errors & 0xFFFF) << 16)
        )
        stream[2] = header.timestamp & 0xFFFFFFFF
        stream[3] = (header.timestamp >> 32) & 0xFFFFFFFF
        return stream

    def _formatColdata(self, coldata):
        stream = []
        stream += self._formatColdataHdr(coldata.header)
        stream += self._formatColdataAdcs(coldata.data.adcs)
        return stream

    def _formatColdataHdr(self, header):
        stream = [0] * 4
        stream[0] = (
            (header.stream1err & 0xF)
            | ((header.stream2err & 0xF) << 4)
            | ((header.chksma & 0xFF) << 16)
            | ((header.chksmb & 0xFF) << 24)
        )
        stream[1] = (
            ((header.chksma >> 8) & 0xFF)
            | (((header.chksmb >> 8) & 0xFF) << 8)
            | ((header.convertcount & 0xFFFF) << 16)
        )
        stream[2] = header.errorregister & 0xFFFF
        for i in xrange(8):
            stream[3] |= (header.hdr[i] & 0xFF) << 4 * i

        return stream

    def _formatColdataAdcs(self, adcs):
        stream = [0] * 24

        for k in xrange(8):
            adc_off = 2 * (k // 2)
            ch_off = 4 * (k % 2)

            stream[3 * k] = (
                (adcs[adc_off][ch_off] & 0xFF) << 0
                | ((adcs[adc_off + 1][ch_off] & 0xFF) << 8)
                | ((adcs[adc_off][ch_off] >> 8) & 0xF) << 16
                | (adcs[adc_off][ch_off + 1] & 0xF) << 20
                | ((adcs[adc_off + 1][ch_off] >> 8) & 0xF) << 24
                | (adcs[adc_off + 1][ch_off + 1] & 0xF) << 28
            )
            stream[3 * k + 1] = (
                (((adcs[adc_off][ch_off + 1]) >> 4) & 0xFF) << 0
                | (((adcs[adc_off + 1][ch_off + 1]) >> 4) & 0xFF) << 8
                | (adcs[adc_off][ch_off + 2] & 0xFF) << 16
                | (adcs[adc_off + 1][ch_off + 2] & 0xFF) << 24
            )
            stream[3 * k + 2] = (
                ((adcs[adc_off][ch_off + 2] >> 8) & 0xF) << 0
                | (adcs[adc_off][ch_off + 3] & 0xF) << 4
                | ((adcs[adc_off + 1][ch_off + 2] >> 8) & 0xF) << 8
                | (adcs[adc_off + 1][ch_off + 3] & 0xF) << 12
                | ((adcs[adc_off][ch_off + 3] >> 4) & 0xFF) << 16
                | ((adcs[adc_off + 1][ch_off + 3] >> 4) & 0xFF) << 24
            )

        return stream

    def dump(self, wibdata):

        stream = []
        stream += self._formatHeader(wibdata.header)
        for i in xrange(4):
            stream += self._formatColdata(wibdata.coldata[i])

        return stream

    def dumpList(self, wiblist, n_idles=2):
        stream = []
        stream += [self.WORD_IDL] * n_idles

        for w in wiblist:
            # Insert Header
            stream += [self.WORD_SOF] * 1
            stream += self.dump(w)
            # Insert Trailer (CRC?)
            stream += [self.WORD_EOF] * 1
            stream += [self.WORD_IDL] * n_idles

        return stream

    def dumpListASCII(self, wiblist, n_idles=2):
        string = ""
        for d in self.dumpList(wiblist):
            string += str("0x%0*x \n" % (9, d))
        return string

    def writeList(self, file, wiblist, n_idles=2):

        for d in self.dumpList(wiblist):
            flag = (int(d) & 0x100000000) >> 32
            line = int(d) & 0xFFFFFFFF
            file.write("0x" + str(hex(line)[2:].zfill(8)) + " " + str(flag))
            file.write("\n")  # print(format(d,"033b"))
