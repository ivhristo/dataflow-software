# Script to generate data patterns in various formats for testing

import random
import numpy as np
import hit

patterns = ["hit_rnd"]

# Generate a random hit packet with 0 to 10 random hits.
def hitPacketRandom():

    data = hit.HitPacket()

    # Fill header frame with random numbers
    data.hitHeader.crate = int(random.getrandbits(5))
    data.hitHeader.fibre = int(random.getrandbits(3))
    data.hitHeader.pipeline = int(random.getrandbits(2))
    data.hitHeader.wireIndex = int(random.getrandbits(6))
    data.hitHeader.slot = int(random.getrandbits(3))
    data.hitHeader.flags = int(random.getrandbits(13))
    data.hitHeader.tstamp = int(random.getrandbits(64))

    # Fill hit frame with random number of hits (0 to 10) of random quantities
    numHitFramesInPacket = random.randint(0, 10)
    startTimes = sorted(
        [int(random.getrandbits(16)) for i in xrange(numHitFramesInPacket)]
    )
    for i in xrange(numHitFramesInPacket):

        Hit = hit.HitFrame()

        Hit.hitStartTime = startTimes[i]
        try:
            nextStartTime = startTimes[i + 1]
        except (IndexError):
            nextStartTime = 65537
        while True:
            endTime = int(random.getrandbits(16))
            hitsize = random.randint(1, 200)

            if (
                endTime > Hit.hitStartTime
                and endTime < Hit.hitStartTime + hitsize
                and endTime < nextStartTime
            ):
                Hit.hitEndTime = endTime
                break

        # Hit.hitEndTime      = startTimes[i] + int(abs(random.gauss(5,50)))
        hitADCs = []
        over = False
        for _ in xrange(Hit.hitEndTime - Hit.hitStartTime):
            # adc = 0
            # while adc <= 0:
            adc = int(
                abs(random.gauss(random.randint(-100, 100), random.randint(1, 100)))
            )
            hitADCs.append(adc)

            if sum(hitADCs) > 65536:
                hitADCs.pop()
                Hit.hitEndTime = int(Hit.hitStartTime + len(hitADCs))
                over = True
                break

        Hit.hitPeakADC = int(max(hitADCs))
        Hit.hitPeakTime = int(
            random.randint(Hit.hitStartTime, Hit.hitEndTime)
        )  # int(random.getrandbits(16))

        Hit.hitSumADC = int(sum(hitADCs))

        if over == True:
            Hit.hitFlags = 1 << 15
            Hit.hitFlags += int(random.getrandbits(15))

        else:
            Hit.hitFlags = 0 << 15
            Hit.hitFlags += int(random.getrandbits(15))

        data.hitPayload.hits.append(Hit)

        # data.hitPayload.hits.append([])
        # for j in xrange(6):
        #     data.hitPayload.hits[i].append(int(random.getrandbits(16)))
    return data


def oneHitPerPacket(n, time=15, cont=0, tstamp=0):

    data = hit.HitPacket()

    # Fill hit packet header
    data.hitHeader.crate = int(1)
    data.hitHeader.fibre = int(1)
    data.hitHeader.pipeline = int(3)
    data.hitHeader.wireIndex = int(n % 4)
    data.hitHeader.slot = int(1)
    data.hitHeader.flags = int(0)
    data.hitHeader.tstamp = int(tstamp)

    # Fill hit frame with a single hit in packet
    aHit = hit.HitFrame()
    aHit.hitStartTime = time
    aHit.hitEndTime = time
    aHit.hitPeakADC = 2780
    aHit.hitPeakTime = time
    aHit.hitSumADC = 23260
    aHit.hitFlags = int(cont) << 15
    aHit.hitFlags += int(0)

    data.hitPayload.hits.append(aHit)

    return data


def nHitsOnNthPacket(n, cont=0, tstamp=0):
    data = hit.HitPacket()

    # Fill hit packet header
    data.hitHeader.crate = int(1)
    data.hitHeader.fibre = int(1)
    data.hitHeader.pipeline = int(3)
    data.hitHeader.wireIndex = int(n % 4)
    data.hitHeader.slot = int(1)
    data.hitHeader.flags = int(0)
    data.hitHeader.tstamp = int(tstamp)

    startTimes = [1 + i * 20 + i for i in xrange(n + 1)]
    endTimes = [st + 20 for st in startTimes]
    peakTimes = [(st + et) // 2 for st, et in zip(startTimes, endTimes)]
    # Fill hit frame with n hits
    for i in xrange(n + 1):
        aHit = hit.HitFrame()
        aHit.hitStartTime = int(startTimes[i])
        aHit.hitEndTime = int(endTimes[i])
        aHit.hitPeakADC = int(2780)
        aHit.hitPeakTime = int(peakTimes[i])
        aHit.hitSumADC = int(23260)
        aHit.hitFlags = int(cont) << 15
        aHit.hitFlags += int(0)

        data.hitPayload.hits.append(aHit)

    return data


def nHitsOnNthPacketWithNQuantities(n, cont=0, tstamp=0):
    data = hit.HitPacket()

    # Fill hit packet header
    data.hitHeader.crate = int(1)
    data.hitHeader.fibre = int(1)
    data.hitHeader.pipeline = int(3)
    data.hitHeader.wireIndex = int(n % 4)
    data.hitHeader.slot = int(1)
    data.hitHeader.flags = int(0)
    data.hitHeader.tstamp = int(tstamp)

    k = n + 3
    for i in xrange(n + 1):
        aHit = hit.HitFrame()
        aHit.hitStartTime = int(k)
        aHit.hitEndTime = int(k)
        aHit.hitPeakADC = int(k)
        aHit.hitPeakTime = int(k)
        aHit.hitSumADC = int(k)
        aHit.hitFlags = int(cont) << 15
        aHit.hitFlags += int(k)

        data.hitPayload.hits.append(aHit)

    return data


def oneHitFirstPacketWireOne(n, time=15, cont=0, tstamp=0):
    data = hit.HitPacket()

    # Fill hit packet header
    data.hitHeader.crate = int(1)
    data.hitHeader.fibre = int(1)
    data.hitHeader.pipeline = int(3)
    data.hitHeader.wireIndex = int(n % 4)
    data.hitHeader.slot = int(1)
    data.hitHeader.flags = int(0)
    data.hitHeader.tstamp = int(tstamp)

    # Fill hit frame with a single hit in first packet
    if n == 0:
        aHit = hit.HitFrame()
        aHit.hitStartTime = time
        aHit.hitEndTime = time
        aHit.hitPeakADC = 2780
        aHit.hitPeakTime = time
        aHit.hitSumADC = 2780
        aHit.hitFlags = int(cont) << 15
        aHit.hitFlags += int(0)

        data.hitPayload.hits.append(aHit)

    return data


def twoHit(n, time=15, cont=0, tstamp=0):
    data = hit.HitPacket()

    # Fill hit packet header
    data.hitHeader.crate = int(1)
    data.hitHeader.fibre = int(1)
    data.hitHeader.pipeline = int(3)
    data.hitHeader.wireIndex = int(n % 4)
    data.hitHeader.slot = int(1)
    data.hitHeader.flags = int(0)
    data.hitHeader.tstamp = int(tstamp)

    # Fill hit frame with a single hit in first packet
    if n == 0:
        aHit = hit.HitFrame()
        aHit.hitStartTime = time
        aHit.hitEndTime = time
        aHit.hitPeakADC = 2780
        aHit.hitPeakTime = time
        aHit.hitSumADC = 2780
        aHit.hitFlags = int(cont) << 15
        aHit.hitFlags += int(0)

        data.hitPayload.hits.append(aHit)

    elif n == 1:
        aHit = hit.HitFrame()
        aHit.hitStartTime = time + 15
        aHit.hitEndTime = time + 15
        aHit.hitPeakADC = 23260
        aHit.hitPeakTime = time + 15
        aHit.hitSumADC = 23260
        aHit.hitFlags = int(cont) << 15
        aHit.hitFlags += int(0)

        data.hitPayload.hits.append(aHit)

    return data


# Generate a list of hit packets
def hitList(type, npackets):
    if type == "rnd" or type == "hit_rnd":
        return [hitPacketRandom() for _ in xrange(npackets)]
    elif type == "onehit":
        return [oneHitPerPacket(n) for n in xrange(npackets)]
    elif type == "nhits":
        return [nHitsOnNthPacket(n) for n in xrange(npackets)]
    elif type == "nhitsnquant":
        return [nHitsOnNthPacketWithNQuantities(n) for n in xrange(npackets)]
    elif type == "singlehit":
        return [oneHitFirstPacketWireOne(n) for n in xrange(npackets)]
    elif type == "twohit":
        return [twoHit(n) for n in xrange(npackets)]
    else:
        return []
