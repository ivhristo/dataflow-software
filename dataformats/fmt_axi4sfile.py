import hit
import axi4s
import cabling
import channel_map

# -----------------------------------------------------------------------------
class Axi4sTPGFormatter(object):
    """ 
    Output sink formatter

    The format that is used to write hit data from zcuButler.py when data is received from the 
    4 sink buffers. It is similar to dtpc_unpacker tb input format but does not contain t_valid 
    or t_keep as all data read to the sinks should be valid anyway.

    Format of outbuf data file:
        tdata tuser tlast
    
    Always assume first row is header information. When tuser = 1, it indicates the end of a packet.
    A Hit packet always follows the header packet (i.e. no multiple header packets) and similarly, after
    the end of each hit packet tuser = 1. After the end of the last hit packet tlast = 1 and the next
    frame will start from header again.
    """

    global Masks
    Masks = {
        "cratemask": int("1111100000000000", 2),
        "fibremask": int("0000011100000000", 2),
        "wireIndexmask": int("0000000011111100", 2),
        "pipelinemask": int("0000000000000011", 2),
        "slotmask": int("1110000000000000", 2),
        "hdrflagsmask": int("0001111111111111", 2),
    }

    def __init__(self):
        super(Axi4sTPGFormatter, self).__init__()
        # self.cm = channel_map.ChannelMap()

    def _unpackAxi4sHeader(self, headerframe):

        crate = (Masks["cratemask"] & headerframe[0]) >> 11
        fibre = (Masks["fibremask"] & headerframe[0]) >> 8
        wireIndex = (Masks["wireIndexmask"] & headerframe[0]) >> 2
        pipeline = Masks["pipelinemask"] & headerframe[0]
        slot = (Masks["slotmask"] & headerframe[1]) >> 13
        hdrflags = Masks["hdrflagsmask"] & headerframe[1]
        timestamp = (
            headerframe[2]
            + (headerframe[3] << 16)
            + (headerframe[4] << 32)
            + (headerframe[5] << 48)
        )
        # No masks needed for timestamps

        headerInfo = {
            "slot": slot,
            "crate": crate,
            "fibre": fibre,
            "wireIndex": wireIndex,
            "pipeline": pipeline,
            "hdrflags": hdrflags,
            "timestamp": timestamp,
        }

        return headerInfo

    def _formatAxi4sToTPGOutStream(self, axi4sPackets):

        streams = [[] for _ in xrange(4)]
        numWord = 0
        numStream = 0

        for packet, ipacket in zip(axi4sPackets, xrange(len(axi4sPackets))):
            numPayload = 0
            hdr = True

            for t, it in zip(packet.flags.tvalid, xrange(len(packet.flags.tvalid))):
                tu = int(packet.flags.tuser[it])
                tl = int(packet.flags.tlast[it])
                tv = int(t)
                tr = int(packet.flags.tready[it])
                tk = int(packet.flags.tkeep[it])

                if hdr and tl == 0:

                    if numWord == 0:
                        word = int(packet.header.crate << 11)
                        word += int(packet.header.fibre << 8)
                        word += int(packet.header.wireIndex << 2)
                        word += int(packet.header.pipeline)
                        numStream = int(packet.header.pipeline)
                        numWord += 1
                        streams[numStream].append([word, tv, tu, tl, tk, tr])

                    elif numWord == 1:
                        word = int(packet.header.slot << 13)
                        word += int(packet.header.flags)
                        numWord += 1
                        streams[numStream].append([word, tv, tu, tl, tk, tr])

                    elif numWord == 2:
                        word = int((packet.header.tstamp) & 0x000000000000FFFF)
                        numWord += 1
                        streams[numStream].append([word, tv, tu, tl, tk, tr])

                    elif numWord == 3:
                        word = int((packet.header.tstamp >> 16) & 0x00000000FFFF)
                        numWord += 1
                        streams[numStream].append([word, tv, tu, tl, tk, tr])

                    elif numWord == 4:
                        word = int((packet.header.tstamp >> 32) & 0x0000FFFF)
                        numWord += 1
                        streams[numStream].append([word, tv, tu, tl, tk, tr])

                    elif numWord == 5:
                        word = int((packet.header.tstamp >> 48) & 0xFFFF)
                        numWord = 0
                        hdr = False
                        streams[numStream].append([word, tv, tu, tl, tk, tr])

                elif not hdr and tl == 0:
                    word = int(packet.payload.data[numPayload])
                    numPayload += 1
                    streams[numStream].append([word, tv, tu, tl, tk, tr])

                elif not hdr and tl == 1:
                    word = int(packet.payload.data[numPayload])
                    hdr = True
                    numPayload = 0
                    streams[numStream].append([word, tv, tu, tl, tk, tr])

                else:
                    if tv == 1:
                        word = int(0)
                        streams[numStream].append([word, tv, tu, tl, tk, tr])

                    else:
                        continue

        return streams

    def _formatAxi4sToTPGInStream(self, axi4sPackets):

        streams = [[] for _ in xrange(4)]

        for packet, ipacket in zip(axi4sPackets, xrange(len(axi4sPackets))):
            numStream = int(packet.header.pipeline)
            numPayload = 0

            for numWord in range(6):
                if numWord == 0:
                    word = int(packet.header.crate << 11)
                    word += int(packet.header.fibre << 8)
                    word += int(packet.header.wireIndex << 2)
                    word += int(packet.header.pipeline)
                    tv = int(1)
                    tu = int(0)
                    tl = int(0)
                    tr = int(1)

                    streams[numStream].append([word, tv, tu, tl, tr])

                elif numWord == 1:
                    word = int(packet.header.slot << 13)
                    word += int(packet.header.flags)
                    tv = int(1)
                    tu = int(0)
                    tl = int(0)
                    tr = int(1)

                    streams[numStream].append([word, tv, tu, tl, tr])

                elif numWord == 2:
                    word = int((packet.header.tstamp) & 0x000000000000FFFF)
                    tv = int(1)
                    tu = int(0)
                    tl = int(0)
                    tr = int(1)

                    streams[numStream].append([word, tv, tu, tl, tr])

                elif numWord == 3:
                    word = int((packet.header.tstamp >> 16) & 0x00000000FFFF)
                    tv = int(1)
                    tu = int(0)
                    tl = int(0)
                    tr = int(1)

                    streams[numStream].append([word, tv, tu, tl, tr])

                elif numWord == 4:
                    word = int((packet.header.tstamp >> 32) & 0x0000FFFF)
                    tv = int(1)
                    tu = int(0)
                    tl = int(0)
                    tr = int(1)

                    streams[numStream].append([word, tv, tu, tl, tr])

                elif numWord == 5:
                    word = int((packet.header.tstamp >> 48) & 0xFFFF)
                    tv = int(1)
                    tu = int(1)
                    tl = int(0)
                    tr = int(1)

                    streams[numStream].append([word, tv, tu, tl, tr])

            for adc in packet.payload.data:

                word = int(adc)
                numPayload += 1
                tv = int(1)
                tr = int(1)

                if numPayload == channel_map.N_TICKS_PER_TP_PACKET - 1:
                    tu = int(1)
                    tl = int(1)

                else:
                    tu = int(0)
                    tl = int(0)

                streams[numStream].append([word, tv, tu, tl, tr])

        return streams

    def _formatStreamToAxi4s(self, data, intype):

        if isinstance(data, list) is False:
            data = self._readFromAxi4sStream(data, intype)
        else:
            data = data

        axi4sPackets = []
        hdr = True
        packet = axi4s.Axi4sPacket()
        hdr_frame = []
        payload = []

        for (line, i) in zip(data, xrange(len(data))):
            # tl high at the end of last frame of hit packet
            # tu high at end of every frame

            word = int(line[0])
            tv = int(line[1])
            tu = int(line[2])
            tl = int(line[3])
            tk = int(line[4])
            tr = int(line[5])

            if tv == 1:
                packet.flags.tlast.append(tl)
                packet.flags.tuser.append(tu)
                packet.flags.tready.append(tr)
                packet.flags.tkeep.append(tk)
                packet.flags.tvalid.append(tv)
                if not hdr:
                    # fill up hit_info
                    if tk == 3 and tl == 0:
                        payload.append(word)
                        # print payload

                    elif (tk == 3 and tl == 1) or (tk == 0 and tl == 1):
                        hdr = True
                        header = self._unpackAxi4sHeader(hdr_frame)
                        # print hdr_frame, header
                        packet.header.slot = header["slot"]
                        packet.header.crate = header["crate"]
                        packet.header.fibre = header["fibre"]
                        packet.header.wireIndex = header["wireIndex"]
                        packet.header.pipeline = header["pipeline"]
                        packet.header.flags = header["hdrflags"]
                        packet.header.tstamp = header["timestamp"]

                        for payl in payload:
                            packet.payload.data.append(payl)

                        axi4sPackets.append(packet)
                        packet = axi4s.Axi4sPacket()
                        hdr_frame = []
                        payload = []

                    else:
                        continue

                elif hdr:
                    # do header frame work
                    if tk == 3:
                        hdr_frame.append(word)

                    if tu == 1:
                        hdr = False
                    else:
                        continue

        if len(axi4sPackets) < 1:
            print("Error: no data. Returning empty axi4s packet list!")

        return axi4sPackets

    def _formatStreamsToAxi4s(self, streams, intype):

        pipelines = []
        for stream in streams:
            data = self._formatStreamToAxi4s(stream, intype)
            pipelines.append(data)

        pipelines = pipelines[1:] + [pipelines[0]]

        numPipelines = len(pipelines)
        numPackets = []

        for i in xrange(numPipelines):
            numPackets.append(len(pipelines[i]))

        numpack = max(numPackets)

        Packets = [0 for _ in xrange(numPipelines * numpack)]

        for ipip, pip in enumerate(pipelines):
            for ipacket, packet in enumerate(pip):
                order = ipip + numPipelines * ipacket
                Packets[order] = packet

        Packets = filter(lambda e: e != 0, Packets)
        return Packets

    def _readFromAxi4sStream(self, file, intype):
        data = []
        valid = []
        user = []
        last = []
        keep = []
        ready = []
        for line in file:
            # get list for each line of file
            l = line.split()
            # data will hold all tdata
            if intype == "up":
                tdata = int(l[0], 16)
                tvalid = int(l[1], 2)
                tuser = int(l[2], 2)
                tlast = int(l[3], 2)
                tready = int(l[4], 2)
                if tvalid == 1 and tlast == 0:
                    tkeep = int(3)
                else:
                    tkeep = int(0)

            elif intype == "axi4stpg":
                if len(l) < 4:
                    tdata = int(l[0], 16)
                    tvalid = int(1)
                    tuser = int(l[1], 2)
                    tlast = int(l[2], 2)
                    tkeep = int(3)
                    tready = int(1)

                else:
                    tdata = int(l[0], 16)
                    tvalid = int(l[1], 2)
                    tuser = int(l[2], 2)
                    tlast = int(l[3], 2)
                    tkeep = int(l[4], 2)
                    tready = int(l[5], 2)

            elif intype == "arb":
                tdata = int(l[0], 16)
                tvalid = int(l[1], 2)
                tuser = int(l[2], 2)
                tlast = int(l[3], 2)
                tkeep = int(l[4], 2)
                tready = int(1)

            data.append([tdata, tvalid, tuser, tlast, tkeep, tready])
        return data

    def _dumpAxi4sTPGStream(self, data):

        for i, stream in enumerate(data):
            print("AXI4S STREAM {}".format(i))
            for line in stream:
                print(
                    str(hex(line[0])[2:].zfill(4)).upper()
                    + " "
                    + str(line[1])
                    + " "
                    + str(line[2])
                    + " "
                    + str(line[3])
                    + " "
                    + str(bin(line[4])[2:].zfill(2))
                    + " "
                    + str(line[5])
                )

    def _dumpAxi4sUpStream(self, data):

        for i, stream in enumerate(data):
            print("AXI4S STREAM {}".format(i))
            for line in stream:
                print(
                    str(hex(line[0])[2:].zfill(4)).upper()
                    + " "
                    + str(line[1])
                    + " "
                    + str(line[2])
                    + " "
                    + str(line[3])
                    + " "
                    + str(line[4])
                )

    def _writeAxi4sTPGStream(self, files, data):

        # data = self._formatAxi4sHitToAxi4sTPG(axi4sPackets)
        for file, stream in zip(files, data):
            for line in stream:

                file.write(str(hex(line[0])[2:].zfill(4)).upper() + " ")
                file.write(str(line[1]) + " ")
                file.write(str(line[2]) + " ")
                file.write(str(line[3]) + " ")
                file.write(str(bin(line[4])[2:].zfill(2)) + " ")
                file.write(str(line[5]))
                file.write("\n")

    def _writeAxi4sUpStream(self, files, data):

        # data = self._formatAxi4sHitToAxi4sTPG(axi4sPackets)
        for file, stream in zip(files, data):
            for line in stream:

                file.write(str(hex(line[0])[2:].zfill(4)).upper() + " ")
                file.write(str(line[1]) + " ")
                file.write(str(line[2]) + " ")
                file.write(str(line[3]) + " ")
                file.write(str(line[4]))
                file.write("\n")

        # return axi4sPackets

    def _formatArbitratorToAxi4s(self, data, intype):

        if isinstance(data, list) is False:
            data = self._readFromAxi4sStream(data, intype)
        else:
            data = data

        axi4sPackets = []
        hdr = True
        packet = axi4s.Axi4sPacket()
        hdr_frame = []
        payload = []

        for (line, i) in zip(data, xrange(len(data))):
            # tl high at the end of last frame of hit packet
            # tu high at end of every frame

            word = int(line[0])
            tv = int(line[1])
            tu = int(line[2])
            tl = int(line[3])
            tk = int(line[4])
            tr = int(line[5])

            if tv == 1:
                packet.flags.tlast.append(tl)
                packet.flags.tuser.append(tu)
                packet.flags.tready.append(tr)
                packet.flags.tkeep.append(tk)
                packet.flags.tvalid.append(tv)
                if not hdr:
                    # fill up hit_info
                    if tk == 3 and tl == 0:
                        payload.append(word)
                        # print payload

                    elif (tk == 3 and tl == 1) or (tk == 0 and tl == 1):
                        hdr = True
                        header = self._unpackAxi4sHeader(hdr_frame)
                        # print hdr_frame, header
                        packet.header.slot = header["slot"]
                        packet.header.crate = header["crate"]
                        packet.header.fibre = header["fibre"]
                        packet.header.wireIndex = header["wireIndex"]
                        packet.header.pipeline = header["pipeline"]
                        packet.header.flags = header["hdrflags"]
                        packet.header.tstamp = header["timestamp"]

                        for payl in payload:
                            packet.payload.data.append(payl)

                        axi4sPackets.append(packet)
                        packet = axi4s.Axi4sPacket()
                        hdr_frame = []
                        payload = []

                    else:
                        continue

                elif hdr:
                    # do header frame work
                    if tk == 3:
                        hdr_frame.append(word)

                    if tu == 1:
                        hdr = False

                    else:
                        continue

        if len(axi4sPackets) < 1:
            print("Error: no data. Returning empty axi4s packet list!")

        # orderedPackets = [0 for _ in range(len(axi4sPackets))]
        # pipelines0 = [p for p in axi4sPackets if p.header.pipeline == 0]
        # pipelines1 = [p for p in axi4sPackets if p.header.pipeline == 1]
        # pipelines2 = [p for p in axi4sPackets if p.header.pipeline == 2]
        # pipelines3 = [p for p in axi4sPackets if p.header.pipeline == 3]

        # for i in range(len(orderedPackets)):
        #     if i % 4 == 0:
        #         try:
        #             orderedPackets[i] = pipelines0[i//4]
        #         except(IndexError):
        #             try:
        #                 orderedPackets[i] = pipelines1[i//4]
        #             except(IndexError):
        #                 try:
        #                     orderedPackets[i] = pipelines2[i//4]
        #                 except(IndexError):
        #                         orderedPackets[i] = pipelines3[i//4]
        #     elif i % 4 == 1:
        #         try:
        #             orderedPackets[i] = pipelines1[i//4]
        #         except(IndexError):
        #             try:
        #                 orderedPackets[i] = pipelines2[i//4]
        #             except(IndexError):
        #                 try:
        #                     orderedPackets[i] = pipelines3[i//4]
        #                 except(IndexError):
        #                         orderedPackets[i] = pipelines0[i//4]

        #     elif i % 4 == 2:
        #         try:
        #             orderedPackets[i] = pipelines2[i//4]
        #         except(IndexError):
        #             try:
        #                 orderedPackets[i] = pipelines3[i//4]
        #             except(IndexError):
        #                 try:
        #                     orderedPackets[i] = pipelines0[i//4]
        #                 except(IndexError):
        #                         orderedPackets[i] = pipelines1[i//4]
        #     elif i % 4 == 3:
        #         try:
        #             orderedPackets[i] = pipelines3[i//4]
        #         except(IndexError):
        #             try:
        #                 orderedPackets[i] = pipelines0[i//4]
        #             except(IndexError):
        #                 try:
        #                     orderedPackets[i] = pipelines1[i//4]
        #                 except(IndexError):
        #                         orderedPackets[i] = pipelines2[i//4]

        return axi4sPackets
