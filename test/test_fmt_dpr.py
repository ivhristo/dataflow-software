#!/bin/env python2

print("test_fmt_dpr")

import sys
sys.path.insert(1, '../dataformats')
import wib2g
import wib_pattgen
import fmt_dpr

# create some random packets
packets = wib_pattgen.wibPattern('rnd', 1000)
# convert to file string and back again
formatter = fmt_dpr.DPRWibFormatter()
s = formatter.writePacketListDPR(packets)
packets2 = formatter.readPacketListDPR(s)

#Check for empty packets list - had occations where test 'passed' due to empty lists
if not packets or not packets2:
    print("Error: Empty packets(2) list")
    exit(1)

# compare with original
if packets == packets2:
    print("Success!")
    exit(0)
else:
    for p, p2 in zip(packets, packets2):
        if not p == p2:
            print("Difference!")
            print(p)
            print(p2)
    exit(1)