import setup
import config

hw = setup.init("SIMUDP")
setup.softReset(hw)

blockAddr = 0xBEAD
configAddr = 0x0AFE

data = [[1, 2, 3, 4]] * 256


config.writeConfig(hw, blockAddr, configAddr, data)

# config.readConfig(hw, pipe, chan , addr, data)
