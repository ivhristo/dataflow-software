#!/bin/env python2

print("test_fmt_axi4sfileUp")

import sys
sys.path.insert(1, '../dataformats')
import wib2g
import wib_pattgen
import fmt_unpacker
import fmt_axi4sfile

#Create some random packets
packets = wib_pattgen.wibPattern('rnd', 1000)
# convert to UP packets
formatter = fmt_unpacker.UnpackerFormatter()
unpack = formatter.formatAllChannels(packets)
# convert to axi4stpgIn format and back to UP via axi4s class
formatter2 = fmt_axi4sfile.Axi4sTPGFormatter()
tpgin = formatter2._formatAxi4sToTPGInStream(unpack)
axiclass = formatter2._formatStreamsToAxi4s(tpgin)
packets2 = formatter.formatAllTicks(axiclass)

#Check for empty packets list - had occations where test 'passed' due to empty lists
if not packets or not packets2:
    print("Error: Empty packets(2) list")
    exit(1)

#Compare with original
if packets == packets2:
    print("Success!")
    exit(0)
else:
    for p, p2 in zip(packets, packets2):
        if not p == p2:
            print(p)
            print(p2)
            print("Difference!")
    exit(1)