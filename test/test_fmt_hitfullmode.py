#!/bin/env python2

print("test_fmt_hitfullmode")

import sys
sys.path.insert(1, '../dataformats')
import hit
import hitpattgen
import fmt_hitfullmode

#Create some random hits
hits = hitpattgen.hitList('rnd', 100)
# convert to file string and back again
formatter = fmt_hitfullmode.CrInterfaceFormatter()
s = formatter._formatHitsToCR(hits)
hits2 = formatter._formatFullModeToHits(s)

#Check for empty hits list - had occations where test 'passed' due to empty lists
if not hits or not hits2:
    print("Error: Empty hits(2) list")
    exit(1)

#Compare with original
if hits == hits2:
    print("Success!")
    exit(0)
else:
    for h, h2 in zip(hits, hits2):
        if not h == h2:
            print(h)
            print(h2)
            print("Difference!")
    exit(1)
