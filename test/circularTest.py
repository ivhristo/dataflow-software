#!/usr/bin/env python

import click
import subprocess
import sys 
sys.path.insert(1, '../dataformats')
sys.path.insert(2, '../bin')

import wib2g
import axi4s
import hit as hitClass

import fmt_hitfullmode
import fmt_hitfile
import fmt_fullmode
import fmt_dpr
import fmt_unpacker
import fmt_philfile

import pattgen
import hitpattgen
import philfilegen

import hfDataflow


@click.command()
#Add more info/help below? Read in existing file option?
@click.option('--pattern', 'pattern', help="Pattern Type", type=str, default=None)
@click.option('--n-packets', 'n_packets', help="Number of packets", type=str, default=1)
@click.option('--fmt', 'fmt', 
	type=click.Choice(
		['hitfullmode', 'hitfile', 'fullmode', 'dpr', 'unpacker', 'phil']),
	help="Which formatter to test",
	default=None
)

def test(pattern, n_packets, fmt):

	#Define outputypes for fmt_fullmode
	if (fmt=='hitfullmode'):
		out_1='--hit'

	#Define outputypes for fmt_hitfile
	elif (fmt=='hitfile'):
		out_1='--hit'
		
	#Define outtypes for fmt_fullmode
	elif (fmt=='fullmode'):
		out_1='--wib' 

	#Define outtypes for fmt_dpr
	elif (fmt=='dpr'):
		out_1='--wib'

	#Define outtypes for fmt_unpacker
	elif (fmt=='unpacker'):
		out_1='--wib'

	#Define outtypes for fmt_philfile
	elif (fmt=='phil'):
		out_1='--wib'

	out_2 = str('--'+fmt)


	#Generate random hits, convert and write to file. Save stdout to file for verification.
	with open('datagen.txt', "w+") as f:
		subprocess.call(['python', '../bin/hfDataflow.py', '--pattern', pattern, '--n-packets', n_packets, '--outtype', fmt, '--outfile', 'formatted.txt', out_1, out_2], stdout=f)

	#Read in formatted .txt file and convert back. Save stdout to file to compare.
	with open('dataconv.txt', "w+") as g:
		subprocess.call(['python', '../bin/hfDataflow.py', '--intype', fmt, '--infile', 'formatted.txt', out_1, out_2], stdout=g)


	#Compare generated (datagen.txt) and converted (dataconv.txt) data.
	#Show diff(erences) in stdout
	#Exit=0 -> passed, Exit=1 -> failed 
	
	#print subprocess.check_output(['diff', 'datagen.txt', 'dataconv.txt'])

	exitstatus = subprocess.call(['diff', 'datagen.txt', 'dataconv.txt'])	
	if exitstatus == 0:
		print('Exit Status:')
		print(exitstatus)
		print('Passed - good job!')

	else:
		print subprocess.call(['diff', 'datagen.txt', 'dataconv.txt'])
		print('Exit Status:')
		print(exitstatus)
		print('Failed - try again!')

if __name__ == '__main__':
	test()
	
