#!/bin/env python2

print("test_fmt_axi4sfileTPG")

import sys
sys.path.insert(1, '../dataformats')
import hit
import hitpattgen
import fmt_axi4sHit
import fmt_axi4sfile

#Create some random hits
hits = hitpattgen.hitList('onehit', 100)
# convert to axi4s object
formatter = fmt_axi4sHit.Axi4sHitFormatter()
o = formatter._formatHitToAxi4s(hits)
# convert to axi4stpg format and back to axi4s object
formatter2 = fmt_axi4sfile.Axi4sTPGFormatter()
t = formatter2._formatAxi4sToTPGOutStream(o)
o2 = formatter2._formatStreamsToAxi4s(t)
# convert new axi4s object back to hits
hits2 = formatter._formatAxi4sToHit(o2)

#Check for empty hits list - had occations where test 'passed' due to empty lists
if not hits or not hits2:
    print("Error: Empty hits(2) list")
    exit(1)
    
#Compare with original
if hits == hits2:
    print("Success!")
    exit(0)
else:
    for h, h2 in zip(hits, hits2):
        if not h == h2:
            print(h)
            print(h2)
            print("Difference!")
    exit(1)