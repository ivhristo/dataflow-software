#!/bin/bash

python2 test_fmt_wibfile.py
python2 test_fmt_philfile.py
python2 test_fmt_fullmode.py
python2 test_fmt_dpr.py
python2 test_fmt_unpacker.py
python2 test_fmt_axi4sfileUp.py
python2 test_fmt_axi4sHit.py
python2 test_fmt_axi4sfileTPG.py
python2 test_fmt_hitfullmode.py
python2 test_fmt_hitfile.py
