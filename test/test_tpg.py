import fmt_tpg
import axi4s
import hit

tpg = fmt_tpg.TriggerPrimitiveGenerator(thresh=30, ped=500)

ip = axi4s.Axi4sPacket()
ip.header.tstamp = 1234
ip.payload.data = [
    500,
    500,
    500,
    500,
    500,
    500,
    500,
    600,
    700,
    500,
    500,
    500,
    500,
    500,
    500,
    500,
    500,
    500,
    500,
    501,
    502,
    504,
    506,
    509,
    513,
    518,
    524,
    530,
    536,
    541,
    546,
    549,
    550,
    549,
    546,
    541,
    536,
    530,
    524,
    518,
    513,
    509,
    506,
    504,
    502,
    501,
    500,
    500,
    500,
    500,
    500,
    500,
    500,
    500,
    500,
    530,
    536,
    541,
    546,
    549,
    550,
    500,
    500,
    500,
]

op = tpg.runTPG(ip)

print(op)

op_true = hit.HitPacket()
op_true.hitHeader.tstamp = 1234
starts = [7, 28, 56]
ends = [9, 37, 61]
sums = [300, 394, 222]
for i in [0, 1, 2]:
    h = hit.HitFrame()
    h.hitStartTime = starts[i]
    h.hitEndTime = ends[i]
    h.hitSumADC = sums[i]
    op_true.hitPayload.hits.append(h)


if op == op_true:
    print("Success!")
else:
    print("Failed")
