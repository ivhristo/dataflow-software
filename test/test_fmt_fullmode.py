#!/bin/env python2
##### UNFINISHED PLACEHOLDER #
# Requires fullmode -> wib object #####

print("test_fmt_fullmode")

import sys
sys.path.insert(1, '../dataformats')
import wib2g
import wib_pattgen
import fmt_fullmode

# create some random packets
packets = wib_pattgen.wibPattern('rnd', 1000)
# convert to file string and back again
formatter = fmt_fullmode.FullModeWibFormatter()
s = formatter.dumpList(packets)
#packets2 = formatter.(s)
packets2 = []

#Check for empty packets list - had occations where test 'passed' due to empty lists
if not packets or not packets2:
    print("Error: Empty packets(2) list")
    exit(1)

# compare with original
if packets == packets2:
    print("Success!")
    exit(0)
else:
    for p, p2 in zip(packets, packets2):
        if not p == p2:
            print(p)
            print(p2)
            print("Difference!")
    exit(1)