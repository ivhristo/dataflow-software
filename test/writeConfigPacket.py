"""

Author: David Cussans

Used to generate a text file that can be used with the 
single procecessing-block test-bench to exercise configuration

See https://wiki.dunescience.org/wiki/Recipes_for_June_2019_DAQ_sprint#Simulation_Test-bench_.28to_test_individual_modules_with_stream_I.2FO.29 on how to build test-bench tp_dtpc_***

See https://docs.google.com/document/d/1Z7fnTA3x5RCPmcrfhEYIXSWOpSaPwef_DzbXECrxsY8/edit# for definition of configuration packet format

Format of output text file:

tdata(hex,16-bits) tvalid(1-bit) tuser(1-bit) tlast(1-bit) tkeep(1-bit)

"""

import click

"""Build a dictionary containing tdata,tuser,tlast,tvalid,tkeep from a dictionary containg configuration data.
configItems['channelNumber'] : 16-bit

configItems['blockAddress'] : 16-bits
configItems['ramAddress']   31-bits
configItems['writeNRead'] = 0x1 # 1 = write
configItems['writeData'] = [ 0x1111 , 0x2222 , 0x3333 , 0x4444 ]
""" 
def buildConfigPacket(configItems):

    configItems['flags'] = 0x0001  # demand flags = 1 ( i.e. a config packet )

    # Construct header frame
    HdrChanNumberLocation = 0
    HdrFlagsLocation = 1
    HdrBlockAddressLocation = 2

    pktHeaderLength = 6
    pktHeader = [0x0] * pktHeaderLength 
    pktHeader[HdrChanNumberLocation] = configItems['channelNumber']
    pktHeader[HdrFlagsLocation] = configItems['flags']
    pktHeader[HdrBlockAddressLocation] = configItems['blockAddress']
    pktTuser = ([0x0] * (pktHeaderLength-1)) + [0x1]
    pktTlast = [0x0] * pktHeaderLength

    # Construct payload frame
    PayloadRamAddressLSWLocation = 0
    PayloadRamAddressMSWLocation = 1
    PayloadLengthLocation = 2
    PayloadDataStartLocation = 3

    pktPayloadLength = 64
    pktPayload = [0x0] * pktPayloadLength
    pktPayload[PayloadRamAddressLSWLocation] = configItems['ramAddress'] & 0xFFFF
    pktPayload[PayloadRamAddressMSWLocation] = ((configItems['ramAddress'] >>16) & 0x7FFF) + (( (configItems['writeNRead']&1) << 15))
    pktPayload[PayloadLengthLocation] = len(configItems['writeData'])
    pktDataLength = len(configItems['writeData'])
    assert pktDataLength < pktPayloadLength - 3
    PayloadDataEndLocation = PayloadDataStartLocation+ pktDataLength 
    pktPayload[PayloadDataStartLocation:PayloadDataEndLocation] =  configItems['writeData']

    # Create arrays for TUSER, TLAST, TVALID , TKEEP
    pktTuser = pktTuser + ([0x0] * (pktPayloadLength-1)) + [0x1]
    pktTlast = pktTlast +  ([0x0] * (pktPayloadLength-1)) + [0x1]
    # Set TVALID, TKEEP permanently high
    pktTvalid = ([0x1] * ( pktHeaderLength + pktPayloadLength ))
    pktTkeep = pktTvalid

    # Assemble packet (from header and payload frames )
    pktTdata = pktHeader + pktPayload
    pktLength = len(pktTdata)
    assert len(pktTdata) == len(pktTuser) == len(pktTlast) == len(pktTvalid) == pktLength == (pktPayloadLength+pktHeaderLength)

    packet = {}
    packet['tdata'] = pktTdata
    packet['tvalid'] = pktTvalid
    packet['tuser'] = pktTuser
    packet['tlast'] = pktTlast
    packet['tkeep'] = pktTkeep
    return packet

"""
Prints out configuration packet
"""
def printConfigPacket(packet,outfile):
    pktLength = len(packet['tdata'])
    assert len(packet['tdata']) == len(packet['tvalid']) == len(packet['tuser']) == len(packet['tlast']) == len(packet['tkeep']) == pktLength
    for idx in range(0,pktLength):
        outfile.write( "%04x %1x %1x %1x %1x\n"%(packet['tdata'][idx],packet['tvalid'][idx], packet['tuser'][idx], packet['tlast'][idx], packet['tkeep'][idx]))


###############################################################################
# Main 
###############################################################################

@click.command()
@click.option('--outfile', '-o', help='Output file name', type=click.File('w'), default='out.txt')
@click.option('--packets', '-n', default=1 )


def run(outfile, packets):

    print "Generating %d configuration packet(s)" % packets
    print "Writing to " , outfile

    for pktNumber in range(1,packets+1):

        # Initialize empty dictionary to hold configuration items
        configItems = {}
        configItems['channelNumber'] = 0xFA00 + pktNumber
        configItems['blockAddress'] = 0x1234
        configItems['ramAddress'] = 0x5EADBEEF # 31-bits
        configItems['writeNRead'] = 0x1 # 1 = write
        data = [None] * 60
        for dataIdx in range(0,60):
            data[dataIdx] = (dataIdx+1) + (pktNumber<<8)
        configItems['writeData'] = data

        packet = buildConfigPacket(configItems)

        # Finally... print out contents of config packet.
        printConfigPacket(packet,outfile)

    outfile.close()



if __name__ == '__main__':
    run()




