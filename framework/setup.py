# eveyrthing to do with ipbus connection to the device goes here...

import os
import uhal


def connectionManager(conn_file=os.environ['CONNECTION_FILE']):
    uhal.setLogLevelTo(uhal.LogLevel.NOTICE)
    conn_file = "file://"+conn_file

    from importlib import import_module
    try:
        ipboflx = import_module('ipboflx')
        manager = ipboflx.ConnectionManager(conn_file)
    except:
        manager = uhal.ConnectionManager(conn_file)

    return manager


def init(conn_file, device):

    manager = connectionManager(conn_file)

    hw = manager.getDevice(str(device))
    return hw


def softReset(ctrlNode):

    print "Soft reset"
    ctrlNode.getNode("csr.ctrl.soft_rst").write(1)
    ctrlNode.getClient().dispatch()
    csrVal = ctrlNode.getNode("csr.ctrl.soft_rst").read()
    ctrlNode.getClient().dispatch()
    print "CSR value after reset = ", csrVal

    if csrVal != 0:
        print "Warning. CSR not magically reset! Clearing"
        ctrlNode.getNode("csr.ctrl.soft_rst").write(0)
        ctrlNode.getClient().dispatch()
        csrVal = ctrlNode.getNode("csr.ctrl.soft_rst").read()
        ctrlNode.getClient().dispatch()
        print "CSR value after reset (should be zero) = ", csrVal

    ctrlNode.getNode("csr.ctrl.mst_rst").write(1)
    ctrlNode.getClient().dispatch()
    # ctrlNode.getNode("csr.ctrl.mst_rst").write(0)
    # ctrlNode.getClient().dispatch()

