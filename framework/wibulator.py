from os.path import exists
import time


kch_idle = 0x1000000BC


# -----------------------------------------------------------------------------
def loadWIBPatternFromFile(path):

    data = []
    with open(path, 'r') as f:
        for l in f:
            tokens = l.split()
            val, ctrl = int(tokens[0], 0), int(tokens[1])
            data.append(val + (ctrl << 32))
    return data

# -----------------------------------------------------------------------------
def format36bto32b(data):
    pdata = []
    for v in data:
        pdata += [ (v & 0x3ffff) , ((v>>18) & 0x3ffff) ]
    return pdata


# -----------------------------------------------------------------------------
def format32bto36b(pdata):

    data = []
    for i in xrange(0, len(pdata), 2):
        data += [ ((pdata[i+1] & 0x3ffff) << 18) + (pdata[i] & 0x3ffff) ]

    return data


# -----------------------------------------------------------------------------
def writePattern(wibNode, pattern):

    bufSize = wibNode.getNode('buf.data').getSize()/2
    patt = pattern[:bufSize]

    if len(patt) < bufSize:
        patt += [kch_idle]*(bufSize-len(patt))

    print 'pattern size', bufSize
    
    portedPattern = format36bto32b(patt)
    wibNode.getNode('buf.addr').write(0x0)
    wibNode.getNode('buf.data').writeBlock(portedPattern)
    wibNode.getClient().dispatch()

# -----------------------------------------------------------------------------
def readPattern(wibNode):
    l = wibNode.getNode('buf.data').getSize()
    wibNode.getNode('buf.addr').write(0x0)
    p = wibNode.getNode('buf.data').readBlock(l)
    wibNode.getClient().dispatch()

    return format32bto36b(p)

# -----------------------------------------------------------------------------
def fire(wibNode, n=1, interval=None):

    for i in xrange(n):

        if i != 0:
            time.sleep(interval)

        wibNode.getNode('csr.ctrl.fire').write(0x1)
        wibNode.getNode('csr.ctrl.fire').write(0x0)
        wibNode.getClient().dispatch()

# -----------------------------------------------------------------------------
def loop(wibNode, loop):
    wibNode.getNode('csr.ctrl.chain').write(loop)
    wibNode.getClient().dispatch()