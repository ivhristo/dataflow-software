import collections
from texttable import Texttable
import termui
import click


# -----------------------------------------------------------------------------
def dumpSubRegs(node):
    regs = {}
    for i in sorted(node.getNodes()):
        regs[i] = node.getNode(i).read()
    node.getClient().dispatch()

    return {k: v.value() for k, v in regs.iteritems()}


# -----------------------------------------------------------------------------
def dumpReg(node):
    v = node.read()
    node.getClient().dispatch()
    return {node.getId(): v.value()}

# -----------------------------------------------------------------------------
def readTpgStatus(node):

    row_names = [
        'upck >> hsc',
        'hsc  >> psub',
        'psub >> fir ',
        'fir  >> hf',
        'hf   >> hsc',
        'hsc  >> cr_if'
    ]

    flag_map = collections.OrderedDict([
        ('v', 'valid'),
        ('u', 'user'),
        ('l', 'last')
    ])

    probes = {}
    for i in xrange(4):
        node.getNode('sel.chain').write(i)
        node.getClient().dispatch()
        probes[i] = dumpSubRegs(node.getNode('csr.mon'))

    hdr = ['probe']+['{}'.format(k) for k in xrange(4)]
    tpg_table = Texttable(max_width=0)
    tpg_table.header(hdr)
    tpg_table.set_deco(Texttable.HEADER | Texttable.BORDER | Texttable.VLINES)
    tpg_table.set_chars(['-', '|', '+', '-'])
    for k in xrange(6):
        lbl = 'p'+str(k)

        flags = ''.join([f for f, l in flag_map.iteritems() if probes[i][lbl+'.'+l]])

        row = [lbl+': '+row_names[k]]+['{} [{}] ({}) {}'.format(probes[i][lbl+'.pkt_ctr'], 'rdy' if probes[i][lbl+'.ready'] else 'bsy', flags, probes[i][lbl+'.last_err']) for i in xrange(4)]
        tpg_table.add_row(row)
    tbl = tpg_table.draw()
    tbl = tbl.replace('[rdy]', '['+termui.kGreen+'rdy'+termui.kReset+']')
    tbl = tbl.replace('[bsy]', '['+termui.kRed+'bsy'+termui.kReset+']')
    return tbl


# -----------------------------------------------------------------------------
def readSinkStatus(node):

    reg_map = collections.OrderedDict([
        ('en', 'ctrl.en'),
        ('fifo_mode', 'ctrl.fifo_mode'),
        ('rdy_mode', 'ctrl.rdy_mode'),
        ('empty', 'stat.empty'),
        ('err', 'stat.err'),
        ('full', 'stat.full'),
        ('count', 'count')
    ])

    sinks = {}
    for i in xrange(4):
        sinks[i] = dumpSubRegs(node.getNode('sink'+str(i)+'.csr'))
        sinks[i].update(dumpReg(node.getNode('sink'+str(i)+'.buf.count')))

    hdr = ['reg']+['{}'.format(k) for k in xrange(4)]
    sink_table = Texttable(max_width=0)
    sink_table.header(hdr)
    sink_table.set_deco(Texttable.HEADER | Texttable.BORDER | Texttable.VLINES)
    sink_table.set_chars(['-', '|', '+', '-'])
    for k, l in reg_map.iteritems():
        row = [k]+[sinks[i][l] for i in xrange(4)]
        sink_table.add_row(row)

    return sink_table.draw()


    # ------------------------------------------------------------------------------
def printRegTable(aRegs, aHeader=True, aSort=True):
    print ( formatRegTable(aRegs, aHeader, aSort) )
# ------------------------------------------------------------------------------


# ------------------------------------------------------------------------------
def formatRegTable(aRegs, aHeader=True, aSort=True):
    lRegTable = Texttable(max_width=0)
    lRegTable.set_deco(Texttable.VLINES | Texttable.BORDER | Texttable.HEADER)
    lRegTable.set_chars(['-', '|', '+', '-'])
    if aHeader:
        lRegTable.header( ['name', 'value'] )

    lRegs = sorted(aRegs) if aSort else aRegs
    for k in lRegs:
        lRegTable.add_row( [str(k), hex(aRegs[k])] )

    return lRegTable.draw()
# ------------------------------------------------------------------------------


# ------------------------------------------------------------------------------
def printDictTable(aDict, aHeader=True, aSort=True, aFmtr=None):
    print ( formatDictTable(aDict, aHeader, aSort, aFmtr) )
# ------------------------------------------------------------------------------


# ------------------------------------------------------------------------------
def formatDictTable(aDict, aHeader=True, aSort=True, aFmtr=str):
    lDictTable = Texttable(max_width=0)
    lDictTable.set_deco(Texttable.VLINES | Texttable.BORDER | Texttable.HEADER)
    lDictTable.set_chars(['-', '|', '+', '-'])
    if aHeader:
        lDictTable.header( ['name', 'value'] )

    for k in (sorted(aDict) if aSort else aDict):
        v = aDict[k]
        lDictTable.add_row( [str(k), aFmtr(v) if aFmtr else v])

    return lDictTable.draw()
# ------------------------------------------------------------------------------


# ------------------------------------------------------------------------------
def gen_chan_validator(start, end):
    """
    Utility function to generate validators for integer number lists with range check

    """
    def validate_channels(ctx, param, value):
        if value is None:
            return None

        if value == 'all':
            return range(start, end)
        elif value == 'none':
            return []

        if not value[0].isdigit():
            raise click.ClickException('Malformed option (comma separated list expected): %s' % value)

        _sep  = ','
        _dash = '-'

        numbers = []
        items = value.split(_sep)
        for item in items:
            nums = item.split(_dash)
            if len(nums) == 1:
                # single number
                numbers.append(int(item))
            elif len(nums) == 2:
                i = int(nums[0])
                j = int(nums[1])
                if i > j:
                    raise click.ClickException('Invalid interval '+item)
                numbers.extend(range(i, j+1))
            else:
                raise click.ClickException('Malformed option (comma separated list expected): %s' % value)

        out_of_range = [n for n in numbers if (n < start or n >= end)]
        if any(out_of_range):
            raise click.ClickException('Values out of range %s-%s: %s' % (start, end, out_of_range))

        return numbers


    return validate_channels
# ------------------------------------------------------------------------------

