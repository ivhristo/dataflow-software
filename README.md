# dataflow-software

Contents :
  1. [Installation](#installation)
  2. [Hardware Control](#hardware-control)
  3. [Pattern Generation](#pattern-generation)

## Installation

```
git clone ssh://git@gitlab.cern.ch:7999/DUNE-SP-TDR-DAQ/dataflow-software.git
source dataflow-software/env.sh
```

These tools depend on python 2.7, uhal, click, future, numpy, scipy (amongst others...)


## Hardware Control

### ZCU102

data operations through **zcu102.**

**hfWatcher** is the utility to observe the data-flow.

terminal A:

```
source env.sh

watch --color python bin/hfWatcher.py <device-name> -BS

```
terminal B:

**hfDriver** is the utility to control the flow and set up the parameters.

Initialize **zcu102**
```
python bin/hfDriver.py <device-name> --init

```

setup the parameters:

example: following command turns Data-Roter ON, Central Router ON and DPR MUX in sink mode.

```
python bin/hfDriver.py <device-name> --dr-on --cr-on --dpr-mux sink
```
**hfWibulator** is the utility to load and capture the data

command 1 loads the pattern in the memory and command 2 loops the data through next blocks.

```
python bin/hfWibulator.py <device-name> --pattern <pattern-file>

python bin/hfWibulator.py <device-name> --loop --fire
```
At this point you will see the Terminal A continuously updating with the inflow of the data.

you can capture the output from sink, using following command:

```
python bin/hfWibulator.py <device-name> --capture > output.txt

```

Options:

```
**hfWatcher**

Usage: hfWatcher.py [OPTIONS] DEVICE

Options:
  -c, --connection PATH
  -r, --show-dr / -R, --hide-dr
  -b, --show-dpr / -B, --hide-dpr
  -t, --show-tpg / -T, --hide-tpg
  -s, --show-spies / -S, --hide-spies
  -c, --show-cr / -C, --hide-cr
  -h, --help   
  
**hfDriver**

Usage: hfDriver.py [OPTIONS] DEVICE

Options:
  -c, --connection PATH
  --init / --no-init              Setup
  --dr-on / --dr-off              Enable data-reception block
  --dpr-mux [reset|passthrough|playback|sink]
                                  Configure DPR mux
  --cr-on / --cr-off              Enable central-router interface block
  --drop-empty / --keep-empty     Drop empty hit packets
  --sk-on / --sk-off              Enable sinks
  -h, --help   
  
**hfWibulator.py**

Usage: hfWibulator.py [OPTIONS] DEVICE

Options:
  -c, --connection PATH
  -p, --pattern PATH
  -f, --fire
  -l, --loop / -s, --single
  -c, --capture
  --drop-idles
  -h, --help    
```


## Pattern Generation

### pattGenWib

Use this to generate WIB (ie. ADC) data patterns.  This will generate all necessary formats with usage :

```python bin/pattGenWib.py --type rnd -n 64 -o <pattern_name> --wib --33b --dpr --up```

Available pattern types include :


### pattGenHit

Use this to generate Hit patterns.  (This is only necessary for special tests where hit data is injected directly into the CR interface).

Typical usage :

```python bin/pattGenHit --type rnd -n 64 -o <outfile> --hitfile --hitfullmode```


### tpgSim

Use this to simulate the TPG chain.  Usage to follow....  (Ask Jim)


### hfDataflow (legacy)

This tool will perform a wide range of conversions between data formats.

Needs initial data patterns to be created in Phil format or WIB format. To create one:

```
python bin/pattGenPhil.py --type rnd --outfile <name> --phil
```

The patterns can be manipulated in different format as per user's requirement

The details on differen format can be found here:
[Insert a wiki detailing different formats](https://www.google.com "abc")

Example usage :

```
python bin/hfDataflow.py <name> --intype phil --fm-ascii --outfile <new_name> --timestamp <64b number>
```
Will convert *Phil* format into a *33b WIB input* with a 64b timestamp.
```
python hfDataflow.py ACTUAL_TPGOutput_pattern2.txt --intype axi4stpg --hit --hitfile --hitfullmode --outfile hitTest
```
Will convert the output of a TPG BLOCK (*AXI4*) into a *hitfile*, *full mode hit*, and a *hit* format.

```
python bin/hfDataflow.py --help
```
To explore more options.

The more detailed documentation can be found here,
[Insert a wiki detailing Df software classes and working](https://www.google.com "abc")


## Package contents

**/bin** - command line tools.

**/data** - data for channel mapping

**/dataformats** - classes to handle input and output data, and patterns.

**/etc** - address maps etc.

**/formatters** - obsolete. Scheduled to be removed.

**/framework** - modules for control of the firmware.

**/test** - test code for CI.  (But also currently contains some legacy FW test code)...

